//
//  CreatePostVC.m
//  FitNetApp
//
//  Created by anish on 06/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "CreatePostVC.h"
#import "PostFeedVC.h"
#import "MapLocationSetVC.h"
@interface CreatePostVC (){
    
@private
    
    NSDictionary *catDic;
    NSDictionary *finalDic;

    
    NSString *catSelectedname;
    NSString *catSelectedId;
    
    NSString *timeSelectedname;
    NSString *timeSelectedId;
    NSString *tapStringCheck;
    
    NSInteger selectedIndex;
    NSInteger selectedIndexCat;
}

@end

@implementation CreatePostVC
@synthesize CategoryBtn,loctionBtn,timeBtn;
- (void)viewDidLoad {
    [super viewDidLoad];
 
    UIImageView *_topView =   [self createUIImageView:CGRectMake(0.0, 0, self.view.frame.size.width, 64) backgroundColor:[UIColor colorWithRed:57.0/255.0 green:160.0/255.0 blue:217.0/255.0 alpha:1.0] image:nil isLogo:YES];
    
    
    _topView.userInteractionEnabled=YES;
    
    UILabel *_lblIAM = [self createLable:CGRectMake(0.0, 25, self.view.frame.size.width, 30.0) bckgroundColor:[UIColor clearColor] title:@"Add Post" font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    _lblIAM.textAlignment = NSTextAlignmentCenter;
    _lblIAM.textColor = [UIColor whiteColor];
    [_topView addSubview:_lblIAM];
    
    
    UIButton *_btnBackButton = [self createButton:CGRectMake(0, 20, 40, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"nav_back1"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    [_btnBackButton setImage:[UIImage imageNamed:@"nav_back1"] forState:UIControlStateNormal];
    [_btnBackButton addTarget:self action:@selector(func__Backbtn) forControlEvents:UIControlEventTouchUpInside];
    [_btnBackButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_topView addSubview:_btnBackButton];
    
    
    UIButton *_btnDoneButton = [self createButton:CGRectMake(self.view.frame.size.width-50, 20, 40, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"BackArrow.png"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:16.0] titleColor:[UIColor blackColor]];
    [_btnDoneButton addTarget:self action:@selector(func__btnDone) forControlEvents:UIControlEventTouchUpInside];
    [_btnDoneButton setTitle:@"Done" forState:UIControlStateNormal];
    [_btnDoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_topView addSubview:_btnDoneButton];
    
    [_btnSessionTime setTitle:@"Specific Time" forState:UIControlStateNormal];
    
    [self.view addSubview:_topView];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
    
    [[CategoryBtn layer] setBorderWidth:0.8f];
    CategoryBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [[CategoryBtn layer] setBorderColor:[UIColor blackColor].CGColor];
    //[CategoryBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 10.0, 0.0, 0.0)];
    
    // CategoryBtn.titleLabel.numberOfLines = 0;
    //CategoryBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    [[_descriptionTextView layer] setBorderWidth:0.8f];
    [[_descriptionTextView layer] setBorderColor:[UIColor blackColor].CGColor];
    
    _tfTitle.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);

    [[_tfTitle layer] setBorderWidth:0.8f];
    [[_tfTitle layer] setBorderColor:[UIColor blackColor].CGColor];
    [[loctionBtn layer] setBorderWidth:0.8f];
    [[loctionBtn layer] setBorderColor:[UIColor blackColor].CGColor];
    [loctionBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 10.0, 0.0, 0.0)];
    
    [[timeBtn layer] setBorderWidth:0.8f];
    [[timeBtn layer] setBorderColor:[UIColor blackColor].CGColor];
    [timeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 10.0, 0.0, 0.0)];
    
    
    [[_btnSessionTime layer] setBorderWidth:0.8f];
    [[_btnSessionTime layer] setBorderColor:[UIColor blackColor].CGColor];
    [_btnSessionTime setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 10.0, 0.0, 0.0)];
    
    _backgroundImagePopup.hidden = YES;
    _homeTableview.hidden = YES;
    _crossButton.hidden = YES;
    _timeTableView.hidden = YES;
    
    catDic = [[NSDictionary alloc]init];
    
    catSelectedname=@"";
    timeSelectedname=@"";
    catSelectedId = @"";
    
    
    tapStringCheck = @"";
    selectedIndex = @"";
    selectedIndexCat = @"";
    timeSelectedId=@"";
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Session Location" forKey:kLocation];
    [[NSUserDefaults standardUserDefaults] setObject:@""  forKey:kLocationLat];
    [[NSUserDefaults standardUserDefaults] setObject:@""  forKey:kLocationLong];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"categoryListServerBoolYes"];
    
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)func__Backbtn{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getCustomCategory:) name:@"custom_category" object:nil];

      [loctionBtn setTitle:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:kLocation]] forState:UIControlStateNormal];

    
}
-(void)func__btnDone{
    
    NSLog(@"catSelectedId=%lu",(unsigned long)catSelectedId.length);
    NSString *strTitle = [_tfTitle.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (strTitle.length == 0|| catSelectedname.length==0 || [[[NSUserDefaults standardUserDefaults] valueForKey:kLocation] isEqualToString:@"Location"] || timeSelectedId.length ==0 || [_btnSessionTime.titleLabel.text isEqualToString:@"Specific Time"])
    {
        [appDelegate() errorMessages:@"Please enter complete details."];
    }

//        if (catSelectedId.length==0)
//        {
//              [appDelegate() errorMessages:kAlertCategory];
//        }
        else
        {
        [appDelegate() CheckInternetConnection];
        if([appDelegate() internetWorking] ==0)
        {
            
            
          NSString *textDesc=  [_descriptionTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

            NSMutableDictionary *initdataInfo;
            
            if ([catSelectedname isEqualToString:@"Others"]){
                initdataInfo = [@{
                                               @"description":textDesc,
                                               @"title":_tfTitle.text,
                                               @"postDate":_postDateAndTime,
                                               @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"],
                                               @"categoryId":catSelectedId,
                                               @"categoryName":_strCustomCat,
                                               @"lat":[[NSUserDefaults standardUserDefaults] valueForKey:kLocationLat],
                                               @"long":[[NSUserDefaults standardUserDefaults] valueForKey:kLocationLong],
                                               @"address":[[NSUserDefaults standardUserDefaults] valueForKey:kLocation],
                                               @"timingId":timeSelectedId
                                               } mutableCopy];
            }
            else{
                initdataInfo = [@{
                                  @"description":textDesc,
                                  @"title":_tfTitle.text,
                                  @"postDate":_postDateAndTime,
                                  @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"],
                                  @"categoryId":catSelectedId,
                                  @"lat":[[NSUserDefaults standardUserDefaults] valueForKey:kLocationLat],
                                  @"long":[[NSUserDefaults standardUserDefaults] valueForKey:kLocationLong],
                                  @"address":[[NSUserDefaults standardUserDefaults] valueForKey:kLocation],
                                  @"timingId":timeSelectedId
                                  
                                  } mutableCopy];
            }
            
            NSLog(@"Category%@",initdataInfo);
            [API PostToAddFeedWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
             {
                 NSLog(@"json=%@",json);
                 
                 UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:kAppName message:[NSString stringWithFormat:@"%@",[json valueForKey:@"message"]] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
                 [alertview show];
                 
                 [self.navigationController popViewControllerAnimated:YES];
                 
             }];
        }
        else
        {
            [appDelegate() dismissActivityAlert];
            [appDelegate() errorMessages:kAlertInternetConnection];
        }
    }
}
#pragma -mark <TableView Delegate>
#pragma -mark
/************************* TableViewMethods **********************************/
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//-------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return catDic.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    if (tableView.tag == 2200)
    {
        static NSString *cellIdentifier = @"MyHomeIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        if(indexPath.row == selectedIndexCat)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }

        
        UILabel *label=(UILabel *)[cell.contentView viewWithTag:100];
        label.text = [[[catDic valueForKey:@"name"] objectAtIndex:indexPath.row] capitalizedString];
    }
    else if (tableView.tag == 2300)
    {
        static NSString *cellIdentifier = @"MyTimeIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        if(indexPath.row == selectedIndex)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        
        UILabel *label=(UILabel *)[cell.contentView viewWithTag:100];
        label.text = [[[catDic valueForKey:@"Timing"] objectAtIndex:indexPath.row] capitalizedString];
        
        UILabel *labelTime=(UILabel *)[cell.contentView viewWithTag:101];
        labelTime.text = [[[catDic valueForKey:@"categoryName"] objectAtIndex:indexPath.row] capitalizedString];
        
    }
    
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 2200){
        NSLog(@"cat is %@",[[catDic valueForKey:@"_id"] objectAtIndex:indexPath.row]);
        if (![[[catDic valueForKey:@"name"] objectAtIndex:indexPath.row] isEqualToString:@"Others"]){
            catSelectedname = [NSString stringWithFormat:@"%@",[[catDic valueForKey:@"name"] objectAtIndex:indexPath.row]];
            
            [CategoryBtn setTitle:[NSString stringWithFormat:@"%@",catSelectedname] forState:UIControlStateNormal];
            
            [tableView reloadData];
        }
        else{
            [super showCustomcategory];
        }
        catSelectedId = [NSString stringWithFormat:@"%@",[[catDic valueForKey:@"_id"] objectAtIndex:indexPath.row]];
        catSelectedname = [NSString stringWithFormat:@"%@",[[catDic valueForKey:@"name"] objectAtIndex:indexPath.row]];
        
        selectedIndexCat = indexPath.row;
        
    }
    else if (tableView.tag == 2300){
        
        timeSelectedname = [NSString stringWithFormat:@"%@",[[catDic valueForKey:@"Timing"] objectAtIndex:indexPath.row]];
        
        timeSelectedId = [NSString stringWithFormat:@"%@",[[catDic valueForKey:@"_id"] objectAtIndex:indexPath.row]];
        
        [timeBtn setTitle:[NSString stringWithFormat:@"%@",timeSelectedname] forState:UIControlStateNormal];
        
        selectedIndex = indexPath.row;
        [tableView reloadData];
        
    }
    
    _fitnnessLabel.hidden = NO;
    _backgroundImagePopup.hidden = YES;
    _homeTableview.hidden = YES;
    _timeTableView .hidden = YES;
    _crossButton.hidden = YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIImaheView UI

-(UIImageView*)createUIImageView:(CGRect)frame backgroundColor:(UIColor*)backgroundColor image:(UIImage*)image isLogo:(BOOL)isLogo{
    
    UIImageView *_imageView = [[UIImageView alloc]initWithFrame:frame];
    _imageView.image = image;
    _imageView.backgroundColor = backgroundColor;
    return _imageView;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UILable UI

-(UILabel*)createLable:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UILabel *_lable = [[UILabel alloc] init];
    _lable.backgroundColor = backgroundColor;
    _lable.frame = frame;
    _lable.text = title;
    _lable.textColor = titleColor;
    _lable.font = font;
    
    return _lable;
}

-(void)getCustomCategory:(NSNotification *)noti{
    NSLog(@"%@",noti.object );
    [CategoryBtn setTitle:[noti.object valueForKey:@"cat"] forState:UIControlStateNormal];
//    catSelectedname = [noti.object valueForKey:@"cat"];
    _strCustomCat = [noti.object valueForKey:@"cat"];
}

- (IBAction)categoryAction:(id)sender
{
    [self.view endEditing:YES];
    _backgroundImagePopup.hidden = NO;
    _timeTableView.hidden = YES;
    _homeTableview.hidden = NO;
    _crossButton.hidden = NO;
    tapStringCheck = @"category";
    catDic = [[[[NSUserDefaults standardUserDefaults] valueForKey:kAllCategoryList] valueForKey:@"data"] valueForKey:@"categories"];
    
    NSLog(@"catDic=%@",catDic);
    _homeTableview.tag =2200;
    [_homeTableview setDelegate:self];
    _homeTableview.dataSource=self;
    [_homeTableview reloadData];
    
}
- (IBAction)locationAction:(id)sender {
    
    // CLLocationCoordinate2D currentLocation = [self getCurrentLocation];
    
    MapLocationSetVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"MapLocationSetVC"];
    [self.navigationController pushViewController:newView animated:YES];

}
- (IBAction)timeAction:(id)sender {
    
    [self.view endEditing:YES];
    tapStringCheck = @"time";
    _backgroundImagePopup.hidden = NO;
    _homeTableview.hidden = YES;
    _timeTableView.hidden = NO;
    _crossButton.hidden = NO;
  
    catDic = [[[[NSUserDefaults standardUserDefaults] valueForKey:kAllCategoryList] valueForKey:@"data"] valueForKey:@"timing"];
    
    _timeTableView.tag =2300;
    [_timeTableView setDelegate:self];
    _timeTableView.dataSource=self;
    [_timeTableView reloadData];
}
- (IBAction)cancelPopup:(id)sender {
    _fitnnessLabel.hidden = NO;
    _backgroundImagePopup.hidden = YES;
    _homeTableview.hidden = YES;
    _crossButton.hidden = YES;
    _timeTableView .hidden = YES;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIButton UI
-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
    _button.backgroundColor = backgroundColor;
    _button.frame = frame;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitleColor:titleColor forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5] forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5Hov] forState:UIControlStateHighlighted];
    
    _button.titleLabel.font = font;
    
    return _button;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    return YES;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)getSessionTime:(NSString *)strTime :(NSDate *)strFormat{
    [_btnSessionTime setTitle:strTime forState:UIControlStateNormal];
  
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        [dateFormatter setTimeZone:timeZone];
        [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm a"];
        NSString *dateString = [dateFormatter stringFromDate:strFormat];
        _postDateAndTime = dateString;
}

- (IBAction)actionBtnSessionTime:(id)sender {
    DatePickerView *pickerView = (DatePickerView *)[[[NSBundle mainBundle] loadNibNamed:@"DatePickerView" owner:self options:nil] objectAtIndex:0];
    
    pickerView.frame = kWindow.frame;
    
    pickerView.layer.cornerRadius = 3.f;
    pickerView.layer.borderColor = [UIColor clearColor].CGColor;
    pickerView.layer.borderWidth = 3.f;
    [kWindow addSubview:pickerView];
    pickerView.delegate = self;
//    [pickerView setBoolValue:NO :_startDate :nil];

}

@end
