//
//  TimeVC.m
//  FitNetApp
//
//  Created by anish on 07/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "TimeVC.h"

@interface TimeVC (){
    @private
    
    NSDictionary *catDic;
    NSString *timeSelectedname;
    NSString *timeSelectedId;
    NSString *tapStringCheck;
    
    NSInteger selectedIndex;
}

@end

@implementation TimeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *_topView =   [self createUIImageView:CGRectMake(0.0, 0, self.view.frame.size.width, 64) backgroundColor:[UIColor colorWithRed:57.0/255.0 green:160.0/255.0 blue:217.0/255.0 alpha:1.0] image:nil isLogo:YES];
    
    _topView.userInteractionEnabled=YES;
    
    UILabel *_lblIAM = [self createLable:CGRectMake(0.0, 25, self.view.frame.size.width, 30.0) bckgroundColor:[UIColor clearColor] title:@"Select Time" font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    _lblIAM.textAlignment = NSTextAlignmentCenter;
    _lblIAM.textColor = [UIColor whiteColor];
    [_topView addSubview:_lblIAM];
    
    
    UIButton *_btnBackButton = [self createButton:CGRectMake(self.view.frame.size.width-56, 16, 48, 48) bckgroundColor:[UIColor clearColor] image:nil title:@"Done" font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
//    [_btnBackButton setImage:[UIImage imageNamed:@"nav_back1"] forState:UIControlStateNormal];
    [_btnBackButton addTarget:self action:@selector(func__btnBack) forControlEvents:UIControlEventTouchUpInside];
    [_btnBackButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_topView addSubview:_btnBackButton];
    
    
    [self.view addSubview:_topView];
    
    selectedIndex = @"";
    
    catDic = [[NSDictionary alloc]init];
    
    catDic = [[[[NSUserDefaults standardUserDefaults] valueForKey:kAllCategoryList] valueForKey:@"data"] valueForKey:@"timing"];
    
    [_timeTable setDelegate:self];
    _timeTable.dataSource=self;
    [_timeTable reloadData];
}

-(void)func__btnBack{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark TableView Delegate

/************************* TableViewMethods **********************************/

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return catDic.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell * cell;
    static NSString *cellIdentifier = @"MyTimeIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        if(indexPath.row == selectedIndex)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        
        UILabel *label=(UILabel *)[cell.contentView viewWithTag:100];
        label.text = [[[catDic valueForKey:@"Timing"] objectAtIndex:indexPath.row] capitalizedString];
        
        UILabel *labelTime=(UILabel *)[cell.contentView viewWithTag:101];
        labelTime.text = [[[catDic valueForKey:@"categoryName"] objectAtIndex:indexPath.row] capitalizedString];
        labelTime.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

        timeSelectedname = [NSString stringWithFormat:@"%@",[[catDic valueForKey:@"Timing"] objectAtIndex:indexPath.row]];
        
        timeSelectedId = [NSString stringWithFormat:@"%@",[[catDic valueForKey:@"_id"] objectAtIndex:indexPath.row]];

    [[NSUserDefaults standardUserDefaults] setObject:timeSelectedname  forKey:kTime];
    [[NSUserDefaults standardUserDefaults] setObject:timeSelectedId  forKey:kTimeIdData];
    [[NSUserDefaults standardUserDefaults] synchronize];

        selectedIndex = indexPath.row;
        [tableView reloadData];
}

#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIImaheView UI

-(UIImageView*)createUIImageView:(CGRect)frame backgroundColor:(UIColor*)backgroundColor image:(UIImage*)image isLogo:(BOOL)isLogo{
    
    UIImageView *_imageView = [[UIImageView alloc]initWithFrame:frame];
    _imageView.image = image;
    _imageView.backgroundColor = backgroundColor;
    return _imageView;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UILable UI

-(UILabel*)createLable:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UILabel *_lable = [[UILabel alloc] init];
    _lable.backgroundColor = backgroundColor;
    _lable.frame = frame;
    _lable.text = title;
    _lable.textColor = titleColor;
    _lable.font = font;
    
    return _lable;
}
//------------------------------------- bckgroundColor ----------------------------------------------------------------------------------------------
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIButton UI
-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
    _button.backgroundColor = backgroundColor;
    _button.frame = frame;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitleColor:titleColor forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5] forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5Hov] forState:UIControlStateHighlighted];
    
    _button.titleLabel.font = font;
    
    return _button;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
