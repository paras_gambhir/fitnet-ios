//
//  ProfileVC.h
//  FitNetApp
//
//  Created by anish on 18/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPFloatRatingView.h"
#import "APIManager.h"
#import "BaseControllerVC.h"
#import <UIImageView+WebCache.h>

@interface ProfileVC : BaseControllerVC<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITableViewDataSource,UITextFieldDelegate,TPFloatRatingViewDelegate>
@property (strong, nonatomic) IBOutlet TPFloatRatingView *ratingView;
@property (strong, nonatomic) IBOutlet UILabel *ratingLabel;
@property (strong, nonatomic) IBOutlet UILabel *liveLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalReviewLbl;

@property (weak, nonatomic) IBOutlet UIImageView *userProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *userLocationLbl;
@property (weak, nonatomic) IBOutlet UIView *imageTopBar;
@property (weak, nonatomic) IBOutlet UIButton *uploadImageBtn;
@property (weak, nonatomic) IBOutlet UIButton *crossBtn;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIView *editView;

@property (weak, nonatomic) IBOutlet UITextField *aboutTextField;
@property (weak, nonatomic) IBOutlet UIButton *fitnessBtn;
@property (weak, nonatomic) IBOutlet UIButton *timeBtnShow;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn;

@property (weak, nonatomic) IBOutlet UITextField *sessionTextField;
@property (weak, nonatomic) IBOutlet UITextField *goodForField;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;

@property (weak, nonatomic) IBOutlet UILabel *aboutMeLbl;
@property (weak, nonatomic) IBOutlet UILabel *fitnessLbl;
@property (weak, nonatomic) IBOutlet UILabel *sessionLbl;
@property (weak, nonatomic) IBOutlet UILabel *advancedLbl;
@property (weak, nonatomic) IBOutlet UILabel *motivationLbl;
@property (weak, nonatomic) IBOutlet UIImageView *imageUploarder;


@end
