
/*
 ********************BusinessDir Server URLs********************
 */
#define kBaseUrl @"http://54.187.56.3:8001/"


#define kDeviceHeight [[UIScreen mainScreen] bounds].size.height
#define kWindow [[[UIApplication sharedApplication] delegate] window]
#define kframe [[UIScreen mainScreen] bounds].size

#define kHeaderHeight 64
#define kUserCurrentLongitude @"Current Longitude"
#define kUserCurrentLatitude @"Current Latitude"
#define kRed 36.0/255.0
#define kGreen 41.0/255.0
#define kBlue 48.0/255.0
#define API   [ApiMaster singleton]
#define JSONObjectFromData(d) [NSJSONSerialization JSONObjectWithData:data options:0 error:nil]
#define NSStringFromNSData(d) [[NSString alloc] initWithBytes:[d bytes] length:[d length] encoding:NSUTF8StringEncoding]

#define AdMob_ID  @"ca-app-pub-9977201646182272/2535679344"
#define rNSUserDefault  [NSUserDefaults standardUserDefaults]
#define QuicksandBold @"AvenirLTStd-Book"
#define QuicksandRegular @"AvenirLTStd-Book"
#define AvenirRegular @"AvenirLTStd-Book"
#define FontQuick @"Quicksand-Bold"
#define FacebookKey @"865469603499728"

/*------------------------NSUserdefault---------------------------------*/

#define kDeviceToken @"Device Token"
#define kLoginData @"LoginData"
#define kAllCategoryList @"AllCategoryList"
#define kAllProfileData @"AllProfileData"

#define kAllNewCategoryList @"AllNewCategoryList"
#define kAllSearchCategoryList @"AllSearchCategoryList"
#define kAllSubCategoryList @"AllSubCategoryList"
#define kReviewViewData @"ReviewViewData"
#define kGetMapUserData @"GetMapUserData"

#define kFitness @"Fitness"
#define kSelectIdData @"SelectIdData"

#define kTime @"Time"
#define kTimeIdData @"TimeIdData"
#define kLevel @"Level"
#define kLocation @"Location"
#define kLocationLat @"LocationLat"
#define kLocationLong @"LocationLong"

#define kHomeLocation @"Home Location"
#define kWorkLocation @"Work Location"
#define kHomeSelection @"HomeSelection"

#define kCurrentActivity @"CurrentActivity"
#define kStoreInitDataResponse @"StoreInitDataResponse"
#define kLAT @"Longitude"
#define kLNG @"Latitude"
#define kUserDeviceToken @"Device Token"

#define kUserDestinationLongitude @"Destination Longitude"
#define kUserDestinationLatitude @"Destination Latitude"

#define AppDelegateClass ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define Storyboard ApplicationDelegate.navController.storyboard

#define VCWithIdentifier(i) [Storyboard instantiateViewControllerWithIdentifier:i]
/*
 **************************************************************************************************************************
 **************************************************************************************************************************
 **************************************************************************************************************************
 
 Alerts
 **************************************************************************************************************************
 **************************************************************************************************************************
 **************************************************************************************************************************
 
 */
#define kAlertNoReview @"No, Review Found."
#define kAlertDis @"Please enter description."
#define kAlertNoFeed @"No, Feed Found."
#define kAppName @"FitNet"
#define kAlertTerms @"Please select the terms and condition."
#define kAlertCategory @"Please select the at least one filter."
#define kAlertInternetConnection @"Please connect to internet."
#define kAlerTwitterPhonePermisionError @"No Permission granted. Please provide permissions access your twitter account."

//No Permission granted", @"Please provide permissions access your twitter account.
