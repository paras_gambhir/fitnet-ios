//
//  OtherProfileVC.m
//  FitNetApp
//
//  Created by anish on 23/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "OtherProfileVC.h"

@interface OtherProfileVC ()

@end

@implementation OtherProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *_topView =   [self createUIImageView:CGRectMake(0.0, 0, self.view.frame.size.width, 64) backgroundColor:[UIColor colorWithRed:57.0/255.0 green:160.0/255.0 blue:217.0/255.0 alpha:1.0] image:nil isLogo:YES];
    
    
    _topView.userInteractionEnabled=YES;
    
    UIButton *_btnBackButton = [self createButton:CGRectMake(0, 20, 40, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"nav_back1"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    [_btnBackButton setImage:[UIImage imageNamed:@"nav_back1"] forState:UIControlStateNormal];
    [_btnBackButton addTarget:self action:@selector(func__btnBack) forControlEvents:UIControlEventTouchUpInside];
    [_btnBackButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_topView addSubview:_btnBackButton];

    
    
    UILabel *_lblIAM = [self createLable:CGRectMake(0.0, 25, self.view.frame.size.width, 30.0) bckgroundColor:[UIColor clearColor] title:@"OTHER PROFILE" font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    _lblIAM.textAlignment = NSTextAlignmentCenter;
    _lblIAM.textColor = [UIColor whiteColor];
    [_topView addSubview:_lblIAM];
    
    
    [self.view addSubview:_topView];

    [self setUI];
}
-(void)setUI{
    
    _aboutMeLbl.text = _model.strAboutMe;
    _labelTimeValue.text = _model.strGoodFor;
    _sessionLbl.text = [NSString stringWithFormat:@"%@",_model.strSession];
    _userNameLbl.text = _model.strName;
    _advancedLbl.text = _model.strLevel;
    _userLocationLbl.text = @"";
    
//    [_userProfileImage yy_setImageWithURL:[NSURL URLWithString:_model.strProfilePicURL] options:YYWebImageOptionProgressiveBlur];
    
    [_userProfileImage sd_setImageWithURL:[NSURL URLWithString:_model.strProfilePicURL] placeholderImage:[UIImage imageNamed:@"profile_icon-1"]];
    self.ratingView.delegate = self;
    self.ratingView.rating = [_model.strRating floatValue];
    self.ratingView.emptySelectedImage = [UIImage imageNamed:@"star_inactive"];
    self.ratingView.fullSelectedImage = [UIImage imageNamed:@"star_active"];
    self.ratingView.contentMode = UIViewContentModeScaleAspectFill;
    self.ratingView.maxRating = 5;
    self.ratingView.minRating = 0;
    self.ratingView.editable = NO;
    self.ratingView.halfRatings = NO;
    self.ratingView.floatRatings = NO;
    _labelTotalReview.text = [NSString stringWithFormat:@"%@ Review",_model.strReviewCount];
    _labelFitnessValue.text = [_model.arrFitness componentsJoinedByString:@","];
    _labelTimeValue.text = _model.strTime;
    
}
-(void)func__btnBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIImaheView UI

-(UIImageView*)createUIImageView:(CGRect)frame backgroundColor:(UIColor*)backgroundColor image:(UIImage*)image isLogo:(BOOL)isLogo{
    
    UIImageView *_imageView = [[UIImageView alloc]initWithFrame:frame];
    _imageView.image = image;
    _imageView.backgroundColor = backgroundColor;
    return _imageView;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UILable UI

-(UILabel*)createLable:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UILabel *_lable = [[UILabel alloc] init];
    _lable.backgroundColor = backgroundColor;
    _lable.frame = frame;
    _lable.text = title;
    _lable.textColor = titleColor;
    _lable.font = font;
    
    return _lable;
}
//------------------------------------- bckgroundColor ----------------------------------------------------------------------------------------------
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIButton UI

-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
    _button.backgroundColor = backgroundColor;
    _button.frame = frame;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitleColor:titleColor forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5] forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5Hov] forState:UIControlStateHighlighted];
    
    _button.titleLabel.font = font;
    
    return _button;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionBtnViewReview:(id)sender {
    
    [self.view endEditing:true];
    
    ViewReviewVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewReviewVC"];
    [newView setStrOtherUserID:_model.strID];
    [self.navigationController pushViewController:newView animated:YES];
}
@end
