//
//  GoogleMapVC.m
//  FitNetApp
//
//  Created by anish on 18/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "GoogleMapVC.h"

@implementation GoogleMapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    

    NSString *strLat = [[NSUserDefaults standardUserDefaults] valueForKey:kLAT];
    NSString *strLng = [[NSUserDefaults standardUserDefaults] valueForKey:kLNG];
    
    
    GMSCameraPosition* camera = [GMSCameraPosition cameraWithLatitude:[strLat doubleValue]
                                                            longitude:[strLng doubleValue]
                                                                 zoom:15];
    _mapView.myLocationEnabled = YES;
    _mapView.settings.compassButton = YES;
    [_mapView setCamera:camera];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getWorkAndHomeLocation];

}

-(void)getWorkAndHomeLocation{
    [super showLoader];
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"] forKey:@"accessToken"];
    
    
    
    [API getHomeWorkLAtLong:[dict mutableCopy] completionHandler:^(NSDictionary *result,NSError *error){
        NSLog(@"%@",result);
        _strLatWork = [[result valueForKeyPath:@"data.workLatitude"] stringValue];
        _strLngWork = [[result valueForKeyPath:@"data.workLongitude"] stringValue];
        _strLatHome = [[result valueForKeyPath:@"data.homeLatitude"] stringValue];
        _strLngHome = [[result valueForKeyPath:@"data.homeLongitude"] stringValue];
        
        if(_strLatWork.length != 0 && _strLatHome.length !=  0)
            [self checkSegmentControlandhitAPI];

    }];
}

-(void)setCamera:(NSString *)strLat :(NSString *)strLng{
    
    GMSCameraPosition* camera = [GMSCameraPosition cameraWithLatitude:[strLat doubleValue]
                                                            longitude:[strLng doubleValue]
                                                                 zoom:15];
    [_mapView setCamera:camera];
}

-(void)getMapdata:(NSString*)strLat : (NSString *)strLng :(NSString *)type{
    
    [_mapView clear];
    if(strLat.length >0){
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.icon = [UIImage imageNamed:@"ic_checkin_small"];
        marker.title = type;
        marker.position = CLLocationCoordinate2DMake([strLat doubleValue], [strLng doubleValue]);
        marker.map = _mapView;
    }
    
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"] forKey:@"accessToken"];
    [dict setObject:strLat forKey:@"lat"];
    [dict setObject:strLng forKey:@"long"];
    
    [API getMapData:[dict mutableCopy] completionHandler:^(NSDictionary *result,NSError *error){
        NSLog(@"%@",result);
        if([type isEqualToString:@"Work"]){
            _arrWorkData = [NSMutableArray new];
            _arrWorkData = [[MapModel parseDataToArray:[result valueForKeyPath:@"data"]] mutableCopy];
            [self showMapMarkers:_arrWorkData];
        }
        else{
            _arrHomeData = [NSMutableArray new];
            _arrHomeData = [[MapModel parseDataToArray:[result valueForKeyPath:@"data"]] mutableCopy];
            [self showMapMarkers:_arrHomeData];
        }
        
    }];
}
-(void)checkSegmentControlandhitAPI{
    
    if(_segmentControlHomeWork.selectedSegmentIndex == 0){
        [self getMapdata:_strLatHome :_strLngHome : @"Home"];
        [self setCamera:_strLatHome :_strLngHome];
        
    }
    else{
        [self getMapdata:_strLatWork :_strLngWork : @"Work"];
        [self setCamera:_strLatWork :_strLngWork];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showMapMarkers:(NSMutableArray *)arr{

    for (MapModel *model in arr){
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.icon = [UIImage imageNamed:@"default_marker"];
        marker.title = model.strName;
        marker.position = CLLocationCoordinate2DMake([model.strLat doubleValue], [model.strLong doubleValue]);
        marker.map = _mapView;
    }
}

- (IBAction)actionSegmentControl:(id)sender {
    [self checkSegmentControlandhitAPI];
}

- (IBAction)actionBtnSettings:(id)sender {
    SettingVC *SettingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
    [self.navigationController pushViewController:SettingVC animated:YES];
}
@end
