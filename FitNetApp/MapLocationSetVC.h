//
//  MapLocationSetVC.h
//  FitNetApp
//
//  Created by anish on 09/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "BaseControllerVC.h"

@interface MapLocationSetVC : BaseControllerVC <UITableViewDataSource,UITableViewDelegate>


@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UITextField *textFieldMap;
@property (weak, nonatomic) IBOutlet MKMapView *mapGet;
@property (weak, nonatomic) IBOutlet UIImageView *backImage;
@property (weak, nonatomic) IBOutlet UITableView *searchTableView;

@property NSString *strHomeWorkLat;
@property NSString *strHomeWorkLng;
@property NSString *strHomeWorkAddress;


@end
