//
//  HomeVC.m
//  FitNetApp
//
//  Created by anish on 18/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "HomeVC.h"
#import "PostFeedVC.h"
#import "MapLocationSetVC.h"
#import "TabBarVC.h"
#import "SettingVC.h"
#import "NSString+Score.h"

@interface HomeVC ()
{
    @private
    
    NSDictionary *catDic;
    NSDictionary *finalDic;
    NSMutableArray *catSelectedArray;
    NSMutableArray *catnameSelectedArray;
    NSString *catSelectedname;
    
    NSString *timeSelectedname;
    NSString *timeSelectedId;
    NSString *tapStringCheck;
    
    NSInteger selectedIndex;
    
}

@end

@implementation HomeVC
@synthesize CategoryBtn,loctionBtn,timeBtn;
- (void)viewDidLoad
{
    [super viewDidLoad];
    [loctionBtn setTitle:@"Search from" forState:UIControlStateNormal];
    [_homeTableview setDelegate:self];
    _homeTableview.dataSource=self;
    UIImageView *_topView =   [self createUIImageView:CGRectMake(0.0, 0, self.view.frame.size.width, 64) backgroundColor:[UIColor colorWithRed:57.0/255.0 green:160.0/255.0 blue:217.0/255.0 alpha:1.0] image:nil isLogo:YES];
    
    _dictAPI = [NSMutableDictionary new];
    _topView.userInteractionEnabled=YES;
    
    UILabel *_lblIAM = [self createLable:CGRectMake(0.0, 25, self.view.frame.size.width, 30.0) bckgroundColor:[UIColor clearColor] title:@"HOME" font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    _lblIAM.textAlignment = NSTextAlignmentCenter;
    _lblIAM.textColor = [UIColor whiteColor];
    [_topView addSubview:_lblIAM];
        NSLog(@"Perentage matchig %f",[@"Bed" scoreAgainst:@"bed" fuzziness:[NSNumber numberWithFloat:0.5]]);
    
//    UIButton *_btnDoneButton = [self createButton:CGRectMake(self.view.frame.size.width-60, 20, 50, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"BackArrow.png"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
//    [_btnDoneButton setImage:[UIImage imageNamed:@"BackArrow.png"] forState:UIControlStateNormal];
//    [_btnDoneButton addTarget:self action:@selector(func__btnDone) forControlEvents:UIControlEventTouchUpInside];
//    [_btnDoneButton setTitle:@"Done" forState:UIControlStateNormal];
//    [_btnDoneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [_topView addSubview:_btnDoneButton];
    
    UIButton *_btnSetting = [self createButton:CGRectMake(self.view.frame.size.width-45, 20, 40, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"nav_settings1"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    [_btnSetting setImage:[UIImage imageNamed:@"nav_settings1"] forState:UIControlStateNormal];
    [_btnSetting addTarget:self action:@selector(func__btnSetting) forControlEvents:UIControlEventTouchUpInside];
    //[_btnPlus setTitle:@"Plus" forState:UIControlStateNormal];
    _btnSetting.backgroundColor = [UIColor clearColor];
    [_topView addSubview:_btnSetting];
    
    
    [self.view addSubview:_topView];
    if(![[[[[NSUserDefaults standardUserDefaults] valueForKeyPath:kLoginData] valueForKeyPath:@"location"] stringValue] isEqualToString:@"1"]){
//        [super showAlert:@"Please select location from Settings" :nil];
    }
    
    
    [self.view addSubview:_topView];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
   
    [[CategoryBtn layer] setBorderWidth:0.8f];
    CategoryBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [[CategoryBtn layer] setBorderColor:[UIColor blackColor].CGColor];
    //[CategoryBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 10.0, 0.0, 0.0)];
    
   // CategoryBtn.titleLabel.numberOfLines = 0;
    //CategoryBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    [[loctionBtn layer] setBorderWidth:0.8f];
    [[loctionBtn layer] setBorderColor:[UIColor blackColor].CGColor];
    [loctionBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 10.0, 0.0, 40.0)];
    [loctionBtn.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
    
    [[timeBtn layer] setBorderWidth:0.8f];
    [[timeBtn layer] setBorderColor:[UIColor blackColor].CGColor];
    [timeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 10.0, 0.0, 0.0)];
    
   // [[_searchBtn layer] setBorderWidth:0.5f];
    //[[_searchBtn layer] setBorderColor:[UIColor blackColor].CGColor];
    _searchBtn.backgroundColor =[UIColor colorWithRed:57.0/255.0 green:160.0/255.0 blue:217.0/255.0 alpha:1.0];
    
    _backgroundImagePopup.hidden = YES;
    _homeTableview.hidden = YES;
    _crossButton.hidden = YES;
    _timeTableView.hidden = YES;
    
    catDic = [[NSDictionary alloc]init];
    catSelectedArray= [[NSMutableArray alloc]init];
    catnameSelectedArray = [[NSMutableArray alloc]init];
    
    catSelectedname=@"";
    timeSelectedname=@"";
    
    tapStringCheck = @"";
    selectedIndex = @"";
    timeSelectedId=@"";
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"categoryListServerBoolYes"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Distance willing to travel" forKey:kLocation];
    [[NSUserDefaults standardUserDefaults] setObject:@""  forKey:kLocationLat];
    [[NSUserDefaults standardUserDefaults] setObject:@""  forKey:kLocationLong];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SelectCategoryFile"];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tokenExpired)
                                                 name:@"tokenExpired"
                                               object:nil];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"categoryListServerBoolYes"]==YES){
        
        [appDelegate() CheckInternetConnection];
        if([appDelegate() internetWorking] ==0)
        {
            NSDictionary *initdataInfo = @{
                                           @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"]
                                           };
            NSLog(@"Category%@",initdataInfo);
            [API PostAccessTokenWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
             {
                 NSLog(@"json=%@",json);
                 [[NSUserDefaults standardUserDefaults] setObject:[json valueForKey:@"data"]  forKey:kLoginData];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 
                 if ([[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"location"] integerValue]==0)
                 {
                     UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Home and Work Location Preferences" message:[NSString stringWithFormat:@"%@",@"Please set your location preferences"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
                     [alertview show];

                     
                 }
                 else
                 {
                      NSLog(@"log");
                 }
            
                 //[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"categoryListServerBoolYes"];
                 [self performSelector:@selector(categoryActionCall) withObject:nil afterDelay:0.0];
                 
             }];
        }
        else
        {
            [appDelegate() dismissActivityAlert];
            [appDelegate() errorMessages:kAlertInternetConnection];
        }
    
    }
    
//    [loctionBtn setTitle:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:kLocation]] forState:UIControlStateNormal];
    
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)categoryActionCall
{
    [appDelegate() CheckInternetConnection];
    if([appDelegate() internetWorking] ==0)
    {
        NSDictionary *initdataInfo = @{
                                       @"":@"",
                                       };
        NSLog(@"Category%@",initdataInfo);
        [API PostCategoryListWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
         {
             
             [[NSUserDefaults standardUserDefaults] setObject:json  forKey:kAllCategoryList];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
        
             finalDic = json;
             catDic = [[finalDic valueForKey:@"data"] valueForKey:@"categories"];
             
             NSLog(@"catDic=%@",catDic);
             
             [appDelegate() dismissActivityAlert];
         }];
    }
    else
    {
        [appDelegate() dismissActivityAlert];
        [appDelegate() errorMessages:kAlertInternetConnection];
    }
}
-(void)func__btnDone
{
    
    
    
}
#pragma -mark <TableView Delegate>
#pragma -mark
/************************* TableViewMethods **********************************/
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//-------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return catDic.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    if (tableView.tag == 2200)
    {
        static NSString *cellIdentifier = @"MyHomeIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        UILabel *label=(UILabel *)[cell.contentView viewWithTag:100];

        if((cell.accessoryType == UITableViewCellAccessoryCheckmark)){
            label.text = [NSString stringWithFormat:@"         %@",[[[catDic valueForKey:@"name"] objectAtIndex:indexPath.row] capitalizedString]];

        }
        else{
            label.text = [[[catDic valueForKey:@"name"] objectAtIndex:indexPath.row] capitalizedString];

        }
        
    }
    else if (tableView.tag == 2300)
    {
        static NSString *cellIdentifier = @"MyTimeIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        if(indexPath.row == selectedIndex)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//            cell.layoutMargins = UIEdgeInsetsMake(0, 40, 0, 10);

        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        
        UILabel *label=(UILabel *)[cell.contentView viewWithTag:100];
        label.text = [[[catDic valueForKey:@"Timing"] objectAtIndex:indexPath.row] capitalizedString];
        
        UILabel *labelTime=(UILabel *)[cell.contentView viewWithTag:101];
        labelTime.text = [[[catDic valueForKey:@"categoryName"] objectAtIndex:indexPath.row] capitalizedString];
        labelTime.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];

        
    }
   
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (tableView.tag == 2200)
    {
    
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
//            cell.layoutMargins = UIEdgeInsetsMake(0, 10, 0, 10);
            UILabel *label=(UILabel *)[cell.contentView viewWithTag:100];

            label.text = [[[catDic valueForKey:@"name"] objectAtIndex:indexPath.row] capitalizedString];

            [catSelectedArray removeObject:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[catDic valueForKey:@"_id"] objectAtIndex:indexPath.row]]]];
            
            [catnameSelectedArray removeObject:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[catDic valueForKey:@"name"] objectAtIndex:indexPath.row]]]];
            
            catSelectedname = @"";
            for (int i =0 ; i <catnameSelectedArray.count;i++){
                catSelectedname = [catSelectedname stringByAppendingFormat:@" , %@",[catnameSelectedArray objectAtIndex:i]];
                
            }
            
            catSelectedname = [catSelectedname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if ([catSelectedname hasPrefix:@","] && [catSelectedname length] > 1)
            {
                catSelectedname = [catSelectedname substringFromIndex:1];
            }
            if (catnameSelectedArray.count == 0)
            {
                [CategoryBtn setTitle:[NSString stringWithFormat:@"%@",@"Category"] forState:UIControlStateNormal];
            }
            else
            {
                [CategoryBtn setTitle:[NSString stringWithFormat:@"%@",catSelectedname] forState:UIControlStateNormal];
            }
            
            
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;

            UILabel *label=(UILabel *)[cell.contentView viewWithTag:100];


            label.text = [NSString stringWithFormat:@"         %@",label.text];
            [catSelectedArray addObject:[NSString stringWithFormat:@"%@",[[catDic valueForKey:@"_id"] objectAtIndex:indexPath.row]]];
            
            [catnameSelectedArray addObject:[NSString stringWithFormat:@"%@",[[catDic valueForKey:@"name"] objectAtIndex:indexPath.row]]];
            
            catSelectedname =@"";
            for (int i =0 ; i <catnameSelectedArray.count;i++)
            {
                catSelectedname = [catSelectedname stringByAppendingFormat:@" , %@",[catnameSelectedArray objectAtIndex:i]];
                
            }
            
            catSelectedname = [catSelectedname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if ([catSelectedname hasPrefix:@","] && [catSelectedname length] > 1) {
                catSelectedname = [catSelectedname substringFromIndex:1];
            }
            [CategoryBtn setTitle:[NSString stringWithFormat:@"%@",catSelectedname] forState:UIControlStateNormal];
        }
        
    }
    else if (tableView.tag == 2300)
    {
      
//        UILabel *label=(UILabel *)[cell.contentView viewWithTag:100];
//        label.text = [[[catDic valueForKey:@"name"] objectAtIndex:indexPath.row] capitalizedString];
        
        timeSelectedname = [NSString stringWithFormat:@"%@",[[catDic valueForKey:@"Timing"] objectAtIndex:indexPath.row]];

        timeSelectedId = [NSString stringWithFormat:@"%@",[[catDic valueForKey:@"_id"] objectAtIndex:indexPath.row]];
        
        [timeBtn setTitle:[NSString stringWithFormat:@"%@",timeSelectedname] forState:UIControlStateNormal];
           [_dictAPI setObject:timeSelectedId forKey:@"timingId"];
        selectedIndex = indexPath.row;
        [tableView reloadData];
        _fitnnessLabel.hidden = NO;
        _backgroundImagePopup.hidden = YES;
        _homeTableview.hidden = YES;
        _timeTableView .hidden = YES;
        _crossButton.hidden = YES;
    }
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIImaheView UI

-(UIImageView*)createUIImageView:(CGRect)frame backgroundColor:(UIColor*)backgroundColor image:(UIImage*)image isLogo:(BOOL)isLogo{
    
    UIImageView *_imageView = [[UIImageView alloc]initWithFrame:frame];
    _imageView.image = image;
    _imageView.backgroundColor = backgroundColor;
    return _imageView;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UILable UI

-(UILabel*)createLable:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UILabel *_lable = [[UILabel alloc] init];
    _lable.backgroundColor = backgroundColor;
    _lable.frame = frame;
    _lable.text = title;
    _lable.textColor = titleColor;
    _lable.font = font;
    
    return _lable;
}
-(void)func__btnSetting
{
    SettingVC *SettingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
    [self.navigationController pushViewController:SettingVC animated:YES];
}
- (IBAction)categoryAction:(id)sender
{
    _backgroundImagePopup.hidden = NO;
    _timeTableView.hidden = YES;
    _homeTableview.hidden = NO;
    _crossButton.hidden = NO;
      tapStringCheck = @"category";
    catDic = [[finalDic valueForKey:@"data"] valueForKey:@"categories"];
    _homeTableview.tag =2200;

    [_homeTableview reloadData];
    
}
- (IBAction)locationAction:(id)sender {
    
   // CLLocationCoordinate2D currentLocation = [self getCurrentLocation];
    
//    MapLocationSetVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"MapLocationSetVC"];
//    [self.navigationController pushViewController:newView animated:YES];
    
    
//    NSString *addressOnMap = @"cupertino";  //place name
//    NSString* addr = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@",addressOnMap];
//    NSURL *url = [[NSURL alloc] initWithString:[addr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//    [[UIApplication sharedApplication] openURL:url];
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKeyPath:kLoginData]);
    if( [[[[[NSUserDefaults standardUserDefaults] valueForKeyPath:kLoginData] valueForKeyPath:@"location"] stringValue] isEqualToString:@"1"]){
        UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Select location" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Work",@"Home",@"Custom location",nil];
        [action showInView:self.view];
    }
    else{
//        [super showAlert:@"Please select location from Settings" :nil];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    if (buttonIndex == 0){
        _strLat = [[[NSUserDefaults standardUserDefaults] valueForKeyPath:kLoginData] valueForKeyPath:@"userDetails.workLatitude"];
        _strLng = [[[NSUserDefaults standardUserDefaults] valueForKeyPath:kLoginData] valueForKeyPath:@"userDetails.workLongitude"];
        
        [_dictAPI setObject:_strLat forKey:@"lat"];
        [_dictAPI setObject:_strLng forKey:@"long"];
        
        [loctionBtn setTitle:@"Work" forState:UIControlStateNormal];

    }
    else if (buttonIndex == 1){
        _strLat = [[[NSUserDefaults standardUserDefaults] valueForKeyPath:kLoginData] valueForKeyPath:@"userDetails.homeLatitude"];
        _strLng = [[[NSUserDefaults standardUserDefaults] valueForKeyPath:kLoginData] valueForKeyPath:@"userDetails.homeLongitude"];
        [_dictAPI setObject:_strLat forKey:@"lat"];
        [_dictAPI setObject:_strLng forKey:@"long"];
        [loctionBtn setTitle:@"Home" forState:UIControlStateNormal];
    }
    else if (buttonIndex == 2){
        CustomLocationVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"CustomLocationVC"];
        VC.delegate = self;
        [self.navigationController pushViewController:VC animated:YES];
    }
    
    
}

-(void)chooseLocation:(NSString *)lat :(NSString *)lng :(NSString *)strAddress{
    [_dictAPI setObject:lat forKey:@"lat"];
    [_dictAPI setObject:lng forKey:@"long"];
    
    [loctionBtn setTitle:strAddress forState:UIControlStateNormal];
    
}


- (IBAction)timeAction:(id)sender {
    
      tapStringCheck = @"time";
    _backgroundImagePopup.hidden = NO;
    _homeTableview.hidden = YES;
     _timeTableView.hidden = NO;
    _crossButton.hidden = NO;
    catDic = [[finalDic valueForKey:@"data"] valueForKey:@"timing"];
    
    _timeTableView.tag =2300;
    [_timeTableView setDelegate:self];
    _timeTableView.dataSource=self;
    [_timeTableView reloadData];
}
- (IBAction)cancelPopup:(id)sender {
     _fitnnessLabel.hidden = NO;
    _backgroundImagePopup.hidden = YES;
    _homeTableview.hidden = YES;
    _crossButton.hidden = YES;
    _timeTableView .hidden = YES;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIButton UI
-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
    _button.backgroundColor = backgroundColor;
    _button.frame = frame;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitleColor:titleColor forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5] forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5Hov] forState:UIControlStateHighlighted];
    
    _button.titleLabel.font = font;
    
    return _button;
}
- (IBAction)searchBtnAction:(id)sender {

    if (catSelectedArray.count==0 && [[[NSUserDefaults standardUserDefaults] valueForKey:kLocation] isEqualToString:@"Location"] && timeSelectedId.length ==0){
        [appDelegate() errorMessages:kAlertCategory];
        
    }
    else{
        [appDelegate() CheckInternetConnection];
        if([appDelegate() internetWorking] ==0)
        {
            
            
            NSError *error=nil;
            if([catSelectedArray count] >0){
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:catSelectedArray options:NSJSONWritingPrettyPrinted error:&error];
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                [_dictAPI setObject:jsonString forKey:@"categoryId"];
            }

            [_dictAPI setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"] forKey:@"accessToken"];
            
//            NSDictionary *initdataInfo = @{
//                                           @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"],
//                                           @"categoryId":jsonString,
//                                           @"lat":[[NSUserDefaults standardUserDefaults] valueForKey:kLocationLat],
//                                           @"long":[[NSUserDefaults standardUserDefaults] valueForKey:kLocationLong],
//                                           @"timingId":timeSelectedId
//                                           };

            [API PostToFeedWithInfo:_dictAPI completionHandler:^(NSDictionary *json,NSError *error){
                 NSLog(@"json=%@",json);
                 
                 if ([[json valueForKey:@"data"] isKindOfClass:[NSNull class]]){
                     [appDelegate() errorMessages:kAlertNoFeed];
                     
                 }
                 else{
                     [[UserInfo sharedUserInfo] setArrSearchResult:[[json valueForKey:@"data"] mutableCopy]];
                     [[UserInfo sharedUserInfo] setIsSearched:YES];
                     [self.tabBarController setSelectedIndex:1];
                 }
             }];
        }
        else
        {
            [appDelegate() dismissActivityAlert];
            [appDelegate() errorMessages:kAlertInternetConnection];
        }
    }

}
//-----------------------------------------------------------------------------------------------------------------------------
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        SettingVC *SettingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
        [self.navigationController pushViewController:SettingVC animated:YES];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)tokenExpired{
    [self performSegueWithIdentifier:@"HomeVC" sender:self];
}

@end
