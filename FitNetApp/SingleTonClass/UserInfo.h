//
//  UserInfo.h
//  Thredz
//
//  Created by Kunal Gupta on 17/02/16.
//  Copyright © 2016 Kunal Gupta. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UserInfo : NSObject

@property NSMutableArray *arrSearchResult;

+(id)sharedUserInfo;
@property BOOL isSearched;
@property NSString *strFilterValue;

@end
