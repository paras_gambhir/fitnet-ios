//
//  UserInfo.m
//  Thredz
//
//  Created by Kunal Gupta on 17/02/16.
//  Copyright © 2016 Kunal Gupta. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo

static UserInfo * sharedUserInfo = nil;
static dispatch_once_t once;

-(id)init{
    
    if (self == [super init]) {
        _arrSearchResult = [NSMutableArray new];
        _isSearched = NO;
    }
    return self;
}

+(id)sharedUserInfo{
    
    dispatch_once(&once, ^{
        
        sharedUserInfo = [[self alloc] init];
    });
    
    return sharedUserInfo;
}

@end
