//
//  ViewController.h
//  FitNetApp
//
//  Created by anish on 14/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UIButton *facebookBtn;
@property (weak, nonatomic) IBOutlet UIButton *termsandConditionBtn;

@property (weak, nonatomic) IBOutlet UIButton *clickTermsBtn;
-(IBAction)unwindSegue:(UIStoryboardSegue *)segue;
@end

