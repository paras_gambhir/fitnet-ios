//
//  AppDelegate.h
//  FitNetApp
//
//  Created by anish on 14/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@class Reachability;

//------------------------------------------------------------------------------------------------------------------------------------------------
@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,UIApplicationDelegate>
//------------------------------------------------------------------------------------------------------------------------------------------------

@property (strong,nonatomic) UINavigationController                     *navController;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic)NSString                                   *strDevicePlatform;
@property (strong,nonatomic) UIAlertController                          *alertIndicator;
@property (strong, nonatomic)UIAlertView                                *errorAlert;
@property(assign,nonatomic)float                                        minimumAge;
@property(assign,nonatomic)float                                        maximumAge;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property(strong,nonatomic) UINavigationController* mainNavigationController;

-(void)openActiveSessionWithPermissions:(NSArray *)permissions allowLoginUI:(BOOL)allowLoginUI;
-(void)ShowActivityIndicatorWithTitle:(NSString *)Title;

//**********Internet Check Methods
- (void) updateInterfaceWithReachability: (Reachability*) curReach;
- (void)CheckInternetConnection;



//**********Create Activity indicator with alert
-(void)ShowActivityIndicatorWithTitle:(NSString*)loadingText;
-(void)dismissActivityAlert;
-(void)errorMessages:(NSString*)message;


//******Internet Reachibility
@property (nonatomic, retain) Reachability                              *internetReach;
@property (nonatomic, retain) Reachability                              *wifiReach;
@property int internetWorking;
@end
AppDelegate *appDelegate(void);

