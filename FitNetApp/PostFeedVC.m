	//
//  PostFeedVC.m
//  FitNetApp
//
//  Created by anish on 18/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "PostFeedVC.h"
#import "SinglePostVC.h"
#import "TabBarVC.h"
#import "SettingVC.h"
#import "CreatePostVC.h"
#import "AsyncImageView.h"
#import "OtherProfileVC.h"
#import "ProfileModel.h"

@interface PostFeedVC ()
{
@private
    
}

@end

@implementation PostFeedVC
@synthesize postDic;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _btnRefine.layer.cornerRadius = 8;
    [_btnRefine setClipsToBounds:YES];
    _tableViewNetwork.delegate = self;
    _tableViewNetwork.dataSource = self;
    UIImageView *_topView =   [self createUIImageView:CGRectMake(0.0, 0, self.view.frame.size.width, 64) backgroundColor:[UIColor colorWithRed:57.0/255.0 green:160.0/255.0 blue:217.0/255.0 alpha:1.0] image:nil isLogo:YES];
    
    
    _topView.userInteractionEnabled=YES;
    
    UILabel *_lblIAM = [self createLable:CGRectMake(0.0, 25, self.view.frame.size.width, 30.0) bckgroundColor:[UIColor clearColor] title:@"NETWORK FEED" font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    _lblIAM.textAlignment = NSTextAlignmentCenter;
    _lblIAM.textColor = [UIColor whiteColor];
    [_topView addSubview:_lblIAM];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"SelectCategoryFile"]==YES){
        
        UIButton *_btnBackButton = [self createButton:CGRectMake(0, 20, 40, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"nav_back1"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
        [_btnBackButton setImage:[UIImage imageNamed:@"nav_back1"] forState:UIControlStateNormal];
        [_btnBackButton addTarget:self action:@selector(func__btnBack) forControlEvents:UIControlEventTouchUpInside];
        [_btnBackButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_topView addSubview:_btnBackButton];
    }
    else{
        UIButton *_btnPlus = [self createButton:CGRectMake(10, 20, 40, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"nav_plus1"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
        [_btnPlus setImage:[UIImage imageNamed:@"nav_plus1"] forState:UIControlStateNormal];
        [_btnPlus addTarget:self action:@selector(func__btnPlus) forControlEvents:UIControlEventTouchUpInside];
        //[_btnPlus setTitle:@"Plus" forState:UIControlStateNormal];
        _btnPlus.backgroundColor = [UIColor clearColor];
        [_topView addSubview:_btnPlus];
    }
    
    UIButton *_btnRefine = [self createButton:CGRectMake(self.view.frame.size.width-90, 20, 40, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"nav_settings1"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:13.0] titleColor:[UIColor whiteColor]];
    //[_btnRefine setImage:[UIImage imageNamed:@"nav_settings1"] forState:UIControlStateNormal];
    [_btnRefine addTarget:self action:@selector(func__btnRefine) forControlEvents:UIControlEventTouchUpInside];
    [_btnRefine setTitle:@"Refine" forState:UIControlStateNormal];
    _btnRefine.backgroundColor = [UIColor clearColor];
    //    [_topView addSubview:_btnRefine];
    
    
    UIButton *_btnSetting = [self createButton:CGRectMake(self.view.frame.size.width-45, 20, 40, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"nav_settings1"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    [_btnSetting setImage:[UIImage imageNamed:@"nav_settings1"] forState:UIControlStateNormal];
    [_btnSetting addTarget:self action:@selector(func__btnSetting) forControlEvents:UIControlEventTouchUpInside];
    //[_btnPlus setTitle:@"Plus" forState:UIControlStateNormal];
    _btnSetting.backgroundColor = [UIColor clearColor];
    [_topView addSubview:_btnSetting];
    
    [self.view addSubview:_topView];
    postDic = [NSDictionary alloc];
    
    //_postToFeedTableView.backgroundColor = [UIColor yellowColor];
    
    // Do any additional setup after loading the view.
}

-(void)func__btnSetting
{
    SettingVC *SettingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
    [self.navigationController pushViewController:SettingVC animated:YES];
}
-(void)func__btnBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)func__btnPlus
{
    [self.view endEditing:true];
    CreatePostVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePostVC"];
    [self.navigationController pushViewController:newView animated:YES];
}

-(void)func__btnRefine
{
    
}
-(void)viewWillAppear:(BOOL)animated{
    NSMutableArray *arrSearchArray = [NSMutableArray new];
    arrSearchArray = [[UserInfo sharedUserInfo] arrSearchResult];

    if([arrSearchArray count] == 0 && [[UserInfo sharedUserInfo] isSearched] == NO){
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"SelectCategoryFile"]==YES)
        {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SelectCategoryFile"];
            //        [_postToFeedTableView reloadData];
            //        [_postToFeedTableView setDelegate:self];
            //        _postToFeedTableView.dataSource=self;
        }
        else{
            [appDelegate() CheckInternetConnection];
            if([appDelegate() internetWorking] ==0)
            {
                NSDictionary *initdataInfo = @{
                                               @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"]
                                               };
                NSLog(@"Category%@",initdataInfo);
                [API PostGetAllPostWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
                 {
                     NSLog(@"json=%@",json);
                     _arrTableData = [NSMutableArray new];
                     _arrTableData = [json valueForKey:@"data"];
                     _arrTabledata = [[PostFeedModal parseDataToArray:[json valueForKey:@"data"]] mutableCopy];
                     [_tableViewNetwork reloadData];
                     postDic = [json valueForKey:@"data"];
                     _postToFeedTableView.layer.borderWidth = 1.0;
                     _postToFeedTableView.layer.borderColor = [UIColor colorWithRed:126.0/255.0 green:161.0/255.0 blue:177.0/255.0 alpha:1.0].CGColor;
                     
                     //             if ([postDic isKindOfClass:[NSNull class]])
                     //             {
                     //
                     //             }
                     if(   _arrTabledata.count == 0)
                     {
                         _postToFeedTableView.hidden = YES;
//                         [appDelegate() errorMessages:kAlertNoFeed];
                     }
                     
                     else
                     {
                         NSLog(@"postDic.count%lu",(unsigned long)postDic.count);
                         _postToFeedTableView.hidden = NO;
                         [_postToFeedTableView reloadData];
                         //                 [_postToFeedTableView setDelegate:self];
                         //                 _postToFeedTableView.dataSource=self;
                         
                     }
                     [self checkArrayAndShowTable];
                     
                 }];
            }
            else
            {
                [appDelegate() dismissActivityAlert];
                [appDelegate() errorMessages:kAlertInternetConnection];
            }
            
        }
    }
    else{
        _arrTableData = [NSMutableArray new];
        _arrTableData = arrSearchArray;
        _arrTabledata = [[PostFeedModal parseDataToArray:arrSearchArray] mutableCopy];
    }
    
    [self checkArrayAndShowTable];
    _tableViewNetwork.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}
-(void)checkArrayAndShowTable{
    if ([_arrTableData count] > 0){
        [_tableViewNetwork setHidden:NO];
        [_labelAlert setHidden:YES];
        _labelAlert.text = @"";
        [_tableViewNetwork reloadData];
    }
    else{
        [_tableViewNetwork setHidden:YES];
        [_labelAlert setHidden:NO];
        _labelAlert.text = @"No results for your search, try broadening it slightly.";
    }
}

#pragma -mark
#pragma -mark <TableView Delegate>
#pragma -mark
/************************* TableViewMethods **********************************/





//-------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrTableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NetworkFeedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NetworkFeedCell"];
    [cell configureCell:[_arrTabledata objectAtIndex:indexPath.row]];
    cell.delegate = self;
    cell.indexPath = indexPath;
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *postId = [[postDic valueForKey:@"_id"] objectAtIndex:indexPath.row];
    
    [appDelegate() CheckInternetConnection];
    if([appDelegate() internetWorking] ==0)
    {
        NSDictionary *initdataInfo = @{
                                       @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"],
                                       @"postId":postId
                                       };
        NSLog(@"Category%@",initdataInfo);
        [API GetSingleDetailsWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
         {
             NSString *strAcceptCount = [[[_arrTableData objectAtIndex:indexPath.row] valueForKey:@"accpectUsers"] stringValue];
             
             NSLog(@"json=%@",json);
             SinglePostVC *SinglePost = [self.storyboard instantiateViewControllerWithIdentifier:@"SinglePostVC"];
             SinglePost.singlePostDic = json;
             SinglePost.strAcceptCount = strAcceptCount;
             
             [self.navigationController pushViewController:SinglePost animated:YES];
             
             
         }];
    }
    else
    {
        [appDelegate() dismissActivityAlert];
        [appDelegate() errorMessages:kAlertInternetConnection];
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 186;
}

//-------------------------------------------------------------------------------------------------------------------------------------
#pragma mark - CUSTOM DELEGATE

-(void)profileButtonPressed:(NSIndexPath *)index{
    _profile = [ProfileModel initWithAttribute:[[_arrTableData objectAtIndex:index.row] valueForKey:@"customer"]];
    OtherProfileVC *SettingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OtherProfileVC"];
    SettingVC.model = _profile;
    [self.navigationController pushViewController:SettingVC animated:YES];
}

#pragma mark IMAGE PICKER DELEGATES
-(void)changeImage:(UITapGestureRecognizer*)sender
//-------------------------------------------------------------------------------------------------------------------------------------
{
    
    CGPoint touchPoint = [sender locationInView:self.postToFeedTableView];
    
    NSIndexPath *indexPath = [self.postToFeedTableView indexPathForRowAtPoint:touchPoint];
    //    NSLog(@"%@",[postDic objectAtIndex:0]);
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIImaheView UI

-(UIImageView*)createUIImageView:(CGRect)frame backgroundColor:(UIColor*)backgroundColor image:(UIImage*)image isLogo:(BOOL)isLogo{
    
    UIImageView *_imageView = [[UIImageView alloc]initWithFrame:frame];
    _imageView.image = image;
    _imageView.backgroundColor = backgroundColor;
    return _imageView;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UILable UI

-(UILabel*)createLable:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UILabel *_lable = [[UILabel alloc] init];
    _lable.backgroundColor = backgroundColor;
    _lable.frame = frame;
    _lable.text = title;
    _lable.textColor = titleColor;
    _lable.font = font;
    
    return _lable;
}
//------------------------------------- bckgroundColor ----------------------------------------------------------------------------------------------
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIButton UI

-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
    _button.backgroundColor = backgroundColor;
    _button.frame = frame;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitleColor:titleColor forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5] forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5Hov] forState:UIControlStateHighlighted];
    
    _button.titleLabel.font = font;
    
    return _button;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)actionBtnRefine:(id)sender {
    TabBarVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarVCPush"];
    [self.navigationController pushViewController:newView animated:YES];
}
@end
