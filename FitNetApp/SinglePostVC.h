//
//  SinglePostVC.h
//  FitNetApp
//
//  Created by anish on 25/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SinglePostVC : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) NSDictionary  *singlePostDic;
@property (weak, nonatomic) IBOutlet UITableView *singlePostTableView;

@property (weak, nonatomic) IBOutlet UILabel *labelAccesptCpunt;
@property NSString *strAcceptCount;

@end
