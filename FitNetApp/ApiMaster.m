//
//  ApiMaster.m
//  Go Boost Me
//
//  Created by Eshan cheema on 2/6/15.
//  Copyright (c) 2015 Eshan cheema. All rights reserved.
//
#import "ApiMaster.h"
#import "Constant.h"
#import "SVProgressHUD.h"

typedef void (^APICompletionHandler)(NSDictionary*json,NSError *error);


@implementation ApiMaster
static ApiMaster* singleton = nil;

+(ApiMaster*)singleton
{
    if(singleton == nil)
        singleton = [[self alloc] init];
    return singleton;
}
-(id)init
{
    if(self = [super init])
    {
        
    }
    return self;
}

#pragma mark- PostInitdataUserWithInfo
/*************************************************** PostInitdataUserWithInfo  ***********
 ******************************************************/
-(void)PostInitdataUserWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"name=%@&imageUrl=%@&facebookId=%@&email=%@&latitude=%@&longitude=%@&deviceType=%@&deviceToken=%@&appVersion=%@",userInfo[@"name"],userInfo[@"imageUrl"],userInfo[@"facebookId"],userInfo[@"email"],userInfo[@"latitude"],userInfo[@"longitude"],userInfo[@"deviceType"],userInfo[@"deviceToken"],userInfo[@"appVersion"]];

    NSString *url=[[NSString stringWithFormat:@"%@api/customer/loginViaFacebook",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
#pragma mark- PostCategoryListWithInfo
/*************************************************** PostInitdataUserWithInfo  ***********
 ******************************************************/
-(void)PostCategoryListWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@""];
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getAllCategory",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
   [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
#pragma mark- PostAccessTokenWithInfo
/*************************************************** PostAccessTokenWithInfo  ***********
 ******************************************************/
-(void)PostAccessTokenWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@",userInfo[@"accessToken"]];
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/accessTokenLogin",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}

-(void)PostViewReview:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@",userInfo[@"accessToken"]];
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getAllReviews",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
-(void)getHomeWorkLAtLong:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@",userInfo[@"accessToken"]];
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getWorkHomeLocations",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
-(void)getUsersList:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@",userInfo[@"accessToken"]];
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getUserList",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}///
-(void)viewReviewOfOtherUser:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@&userId=%@",userInfo[@"accessToken"],userInfo[@"userId"]];
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getAllReviewsOfOtherUser",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
-(void)blockUnblockUser:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@&customerId=%@&status=%@",userInfo[@"accessToken"],userInfo[@"customerId"],userInfo[@"status"]];
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/blockOrUnBlockCustomer",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}

-(void)getMapData:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@&lat=%@&long=%@",userInfo[@"accessToken"],userInfo[@"lat"],userInfo[@"long"]];
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getMapUsers",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}

#pragma mark- PostgetWorkHomeLocationsWithInfo
/*************************************************** PostgetWorkHomeLocationsWithInfo  ***********
 ******************************************************/
-(void)PostgetWorkHomeLocationsWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@",userInfo[@"accessToken"]];
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getWorkHomeLocations",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}

#pragma mark- getBlockUnBlockWithInfo
/*************************************************** getBlockUnBlockWithInfo  ***********
 ******************************************************/
-(void)getBlockUnBlockWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@&customerId=%@&status=%@",userInfo[@"accessToken"],userInfo[@"customerId"],userInfo[@"status"]];
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/blockOrUnBlockCustomer",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
#pragma mark- PostToFeedWithInfo
/*************************************************** PostToFeedWithInfo  ***********
 ******************************************************/
-(void)PostToFeedWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@&categoryId=%@&lat=%@&long=%@&timingId=%@",userInfo[@"accessToken"],userInfo[@"categoryId"],userInfo[@"lat"],userInfo[@"long"],userInfo[@"timingId"]];
    
    NSMutableString *strURL = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"accessToken=%@",userInfo[@"accessToken"]]];
    
    if([userInfo[@"categoryId"] length] != 0){
        NSString *newSTr = [NSString stringWithFormat:@"&categoryId=%@",userInfo[@"categoryId"]];
        [strURL appendString:newSTr];
    }
    if (userInfo[@"lat"] != 0){
        [strURL appendString:[NSString stringWithFormat:@"&lat=%@&long=%@",userInfo[@"lat"],userInfo[@"long"]]];
    }
    if (userInfo[@"timingId"] != 0){
        [strURL appendString:[NSString stringWithFormat:@"&timingId=%@",userInfo[@"timingId"]]];
    }

    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getAllPost",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[strURL dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
#pragma mark- getUpdateDistanceDataWithInfo
/*************************************************** getUpdateDistanceDataWithInfo  ***********
 ******************************************************/
-(void)getUpdateDistanceDataWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@&distance=%@",userInfo[@"accessToken"],userInfo[@"distance"]];
    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/updateDistance",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
#pragma mark- PostToAddFeedWithInfo
/*************************************************** PostToAddFeedWithInfo  ***********
 ******************************************************/
-(void)PostToAddFeedWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"title=%@&description=%@&accessToken=%@&categoryId=%@&lat=%@&long=%@&address=%@&timingId=%@&postDate=%@&categoryName=%@",userInfo[@"title"],userInfo[@"description"],userInfo[@"accessToken"],userInfo[@"categoryId"],userInfo[@"lat"],userInfo[@"long"],userInfo[@"address"],userInfo[@"timingId"],userInfo[@"postDate"],userInfo[@"categoryName"]];
    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/createNewPost",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
#pragma mark- PostupdateWorkHomeLocationWithInfo
/*************************************************** PostupdateWorkHomeLocationWithInfo  ***********
 ******************************************************/
-(void)PostupdateWorkHomeLocationWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@&latitude=%@&longitude=%@&address=%@&status=%@",userInfo[@"accessToken"],userInfo[@"latitude"],userInfo[@"longitude"],userInfo[@"address"],userInfo[@"status"]];
    
    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/updateWorkHomeLocation",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
#pragma mark- PostEditProfileFeedWithInfo
/*************************************************** PostEditProfileFeedWithInfo  ***********
 ******************************************************/
-(void)PostEditProfileFeedWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@&aboutMe=%@&fitness=%@&timingId=%@&level=%@", userInfo[@"accessToken"],userInfo[@"aboutMe"],userInfo[@"fitness"],userInfo[@"timingId"],userInfo[@"level"]];
    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/updateProfile",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
#pragma mark- LogoutWithInfo
/*************************************************** LogoutWithInfo  ***********
 ******************************************************/
-(void)LogoutWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@",userInfo[@"accessToken"]];
    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/logout",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
#pragma mark- getProfileDataWithInfo
/*************************************************** getProfileDataWithInfo  ***********
 ******************************************************/
-(void)getProfileDataWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@",userInfo[@"accessToken"]];
    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getProfileData",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}

#pragma mark- PostInitdataUserWithInfo
/*************************************************** PostInitdataUserWithInfo  ***********
 ******************************************************/
-(void)PostGetAllPostWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@",userInfo[@"accessToken"]];
    
    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getAllPost",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
#pragma mark- PostEndJobWithInfo
/*************************************************** PostEndJobWithInfo  ***********
 ******************************************************/


-(void)PostEndJobWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@&jobId=%@&rating=%@&comments=%@",userInfo[@"accessToken"],userInfo[@"id"],userInfo[@"rating"],userInfo[@"comments"]];
    
    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/endJob",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}



#pragma mark- PostCurrentActivityFeedWithInfo
/*************************************************** PostCurrentActivityFeedWithInfo  ***********
 ******************************************************/
-(void)PostCurrentActivityFeedWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@",userInfo[@"accessToken"]];
    
    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getCurrentActivities",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}

#pragma mark- GetSingleDetailsWithInfo
/*************************************************** GetSingleDetailsWithInfo  ***********
 ******************************************************/
-(void)GetSingleDetailsWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@&postId=%@",userInfo[@"accessToken"],userInfo[@"postId"]];
    
    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getSinglePostDetails",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);

    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
   [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
#pragma mark- getupdateImageWithInfo
/*************************************************** getupdateImageWithInfo  ***********
 ******************************************************/
-(void)getupdateImageWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@&profilePic=%@",userInfo[@"accessToken"],userInfo[@"profilePic"]];
    
    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/updateImage",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}


#pragma mark- acceptJobWithInfo
/*************************************************** acceptJobWithInfo  ***********
 ******************************************************/
-(void)acceptJobWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@&jobId=%@",userInfo[@"accessToken"],userInfo[@"jobId"]];
    
    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/accpectJob",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
#pragma mark- PostReviewViewWithInfo
/*************************************************** PostReviewViewWithInfo  ***********
 ******************************************************/
-(void)PostReviewViewWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@",userInfo[@"accessToken"]];
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getAllReviews",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}
#pragma mark- PostGetMapViewWithInfo
/*************************************************** PostGetMapViewWithInfo  ***********
 ******************************************************/
-(void)PostGetMapViewWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler
{
    
    NSString* infoStr = [NSString stringWithFormat:@"accessToken=%@&lat=%@&long=%@",userInfo[@"accessToken"],userInfo[@"lat"],userInfo[@"long"]];
    NSLog(@"infoStr=%@",infoStr);
    
    NSString *url=[[NSString stringWithFormat:@"%@api/customer/getMapUsers",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSLog(@"url=%@",url);
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    [request setHTTPBody:[infoStr dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:300];
    [self PostforwardRequest:request showActivity:YES completionHandler:handler];
}

#pragma mark- Post Request To Server
/******************************************************* Post Request To Server  ******************
 ******************************************************/
-(void)PostforwardRequest:(NSMutableURLRequest*)request showActivity:(BOOL)showActivity completionHandler:(APICompletionHandler)handler
{
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    if(showActivity)
    [self ShowActivityIndicatorWithTitle:@"Loading..."];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:
     ^(NSURLResponse* response, NSData* data, NSError* connectionError)
     {
         if(connectionError != nil)
         {
             [self HideActivityIndicator];

             NSDictionary *dict = JSONObjectFromData(data);
             NSLog(@"%@",[dict valueForKey:@"responseType"]);
             if([[dict valueForKey:@"responseType"] isEqualToString:@"INVALID_TOKEN"]){
                 [[[UIAlertView alloc] initWithTitle:@"Token Expired" message:@"Please login again!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"tokenExpired" object:nil];
             }
             else{
                 [[[UIAlertView alloc] initWithTitle:@"Connection Error !" message:@"No Internet Connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
             }
             
             
             return;
         }
      [self HideActivityIndicator];
         if(handler != nil)
             handler(JSONObjectFromData(data),connectionError);
     }];
}
# pragma mark
#pragma mark GET request to Server
# pragma mark
-(void)GetforwardRequest:(NSMutableURLRequest*)request showActivity:(BOOL)showActivity completionHandler:(APICompletionHandler)handler
{
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"GET"];
    if(showActivity)
         [self ShowActivityIndicatorWithTitle:@"Loading..."];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:
         ^(NSURLResponse* response, NSData* data, NSError* connectionError)
         {
             if(connectionError != nil)
             {
                 [[[UIAlertView alloc] initWithTitle:@"Connection Error !" message:@"No Internet Connection" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
                     [self HideActivityIndicator];
                 return;
             }
             NSLog(@"Response String %@", NSStringFromNSData(data));
             [self HideActivityIndicator];
             if(handler != nil)
                 handler(JSONObjectFromData(data),connectionError);
         }];
}
#pragma mark- Google Direction API
/**************************************** Google Direction API
 ******************************************************/
-(void)GetDirections:(APICompletionHandler)handler
{
    
    NSString *url=[[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f",[[[NSUserDefaults standardUserDefaults] valueForKey:kUserCurrentLatitude] doubleValue],[[[NSUserDefaults standardUserDefaults] valueForKey:kUserCurrentLongitude] doubleValue],[[[NSUserDefaults standardUserDefaults] valueForKey:@"lat"] doubleValue],[[[NSUserDefaults standardUserDefaults] valueForKey:@"long"] doubleValue]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest* request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] ];
    
    [self GetforwardRequest:request showActivity:YES completionHandler:handler];
    [request setTimeoutInterval:300];
}
-(void)ShowActivityIndicatorWithTitle:(NSString *)Title
{
    [SVProgressHUD showWithStatus:Title maskType:SVProgressHUDMaskTypeGradient];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];
}
-(void)HideActivityIndicator
{
    [SVProgressHUD dismiss];
}
@end
