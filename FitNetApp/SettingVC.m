//
//  SettingVC.m
//  FitNetApp
//
//  Created by anish on 20/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "SettingVC.h"
#import "ViewController.h"
#import "MapLocationSetVC.h"
@interface SettingVC (){
    
    @private
    
    float value;
    NSString *selectBtnFlag;
}

@end

@implementation SettingVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *_topView =   [self createUIImageView:CGRectMake(0.0, 0, self.view.frame.size.width, 64) backgroundColor:[UIColor colorWithRed:57.0/255.0 green:160.0/255.0 blue:217.0/255.0 alpha:1.0] image:nil isLogo:YES];
    _topView.userInteractionEnabled=YES;
    [_sliderBar  addTarget:self action:@selector(blurSliderChanged) forControlEvents:UIControlEventValueChanged];
    [_sliderBar  addTarget:self action:@selector(blurSliderEnded) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *_btnBackButton = [self createButton:CGRectMake(0, 20, 40, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"nav_back1"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    [_btnBackButton setImage:[UIImage imageNamed:@"nav_back1"] forState:UIControlStateNormal];
    [_btnBackButton addTarget:self action:@selector(func__btnBack) forControlEvents:UIControlEventTouchUpInside];
    [_btnBackButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_topView addSubview:_btnBackButton];
    
    UILabel *_lblIAM = [self createLable:CGRectMake(0.0, 25, self.view.frame.size.width, 30.0) bckgroundColor:[UIColor clearColor] title:@"SETTINGS" font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    _lblIAM.textAlignment = NSTextAlignmentCenter;
    _lblIAM.textColor = [UIColor whiteColor];
    [_topView addSubview:_lblIAM];
    _sliderBar.continuous = YES;
    
    [self.view addSubview:_topView];
    
    _termsandconditionBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _termsandconditionBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    
    _reportBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _reportBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    
    
    _blockBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _blockBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    
    _logoutBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _logoutBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    
    self.sliderTxtLabel.text = [NSString stringWithFormat:@"%2f MILES",_sliderBar.value];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"sliderBarBoolYes"];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Home Location" forKey:kHomeLocation];
    [[NSUserDefaults standardUserDefaults] setObject:@"Work Location"  forKey:kWorkLocation];
     [[NSUserDefaults standardUserDefaults] setObject:@""  forKey:kHomeSelection];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [_homeLoctionBtn setTitle:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:kHomeLocation]] forState:UIControlStateNormal];
    
     [_workLocationBtn setTitle:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:kWorkLocation]] forState:UIControlStateNormal];
    if([[[UserInfo sharedUserInfo] strFilterValue] length] >0){
        self.sliderTxtLabel.text = [[UserInfo sharedUserInfo] strFilterValue];
        _sliderBar.value = [_sliderTxtLabel.text floatValue];
    }
    
    [appDelegate() CheckInternetConnection];
    if([appDelegate() internetWorking] ==0)
    {
        NSDictionary *initdataInfo = @{
                                       @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"]
                                       };
        NSLog(@"PostgetWorkHomeLocationsWithInfo%@",initdataInfo);
        [API PostgetWorkHomeLocationsWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
         {
             NSLog(@"json=%@",json);

             CGFloat valuef = [[json valueForKeyPath:@"data.distanceMeter"] floatValue];
             NSString *strValue = [NSString stringWithFormat:@"%f",0.000621371 * valuef];
             _sliderBar.value = [strValue floatValue];
           
             self.sliderTxtLabel.text =  [NSString stringWithFormat:@"%00.02f MILES", _sliderBar.value];
             [[UserInfo sharedUserInfo] setStrFilterValue:self.sliderTxtLabel.text];

             NSLog(@"json=%@",[json valueForKey:@"data"]);
             
             if ([[[json valueForKey:@"data"] valueForKey:@"homeAddress"] length]>0)
             {
                 [_homeLoctionBtn setTitle:[NSString stringWithFormat:@"%@",[[json valueForKey:@"data"] valueForKey:@"homeAddress"]] forState:UIControlStateNormal];
                 _strHomeLat = [json valueForKeyPath:@"data.homeLatitude"];
                 _strHomeLng = [json valueForKeyPath:@"data.homeLongitude"];
                 _strHomeAddress = [json valueForKeyPath:@"data.homeAddress"];
             }
             else
             {
                   [_homeLoctionBtn setTitle:[NSString stringWithFormat:@"%@",@"Enter Home Location"] forState:UIControlStateNormal];
                 [_homeLoctionBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                 
             }
             if ([[[json valueForKey:@"data"] valueForKey:@"workAddress"] length]>0)
             {
                   [_workLocationBtn setTitle:[NSString stringWithFormat:@"%@",[[json valueForKey:@"data"] valueForKey:@"workAddress"]] forState:UIControlStateNormal];
                 _strWorkLat = [json valueForKeyPath:@"data.workLatitude"];
                 _strWorkLng = [json valueForKeyPath:@"data.workLongitude"];
                 _strWorkAddress = [json valueForKeyPath:@"data.workAddress"];
             }
             else
             {
                  [_workLocationBtn setTitle:[NSString stringWithFormat:@"%@",@"Enter Work Location"] forState:UIControlStateNormal];
                 
                  [_workLocationBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                 
             }
            
         }];
    }
    else
    {
        [appDelegate() dismissActivityAlert];
        [appDelegate() errorMessages:kAlertInternetConnection];
    }

    
}
-(void)viewWillAppear:(BOOL)animated
{
     [_workLocationBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_homeLoctionBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    if (![[[NSUserDefaults standardUserDefaults] valueForKey:kHomeLocation] isEqualToString:@"Home Location"])
    {
       
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:kHomeLocation] isEqualToString:@"Work Location"])
    {
       
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:kHomeSelection] isEqualToString:@"HomeBtnSelect"])
    {
         [_homeLoctionBtn setTitle:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:kHomeLocation]] forState:UIControlStateNormal];
        
        
        [appDelegate() CheckInternetConnection];
        if([appDelegate() internetWorking] ==0)
        {
            NSDictionary *initdataInfo = @{
                                           @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"],
                                           @"latitude":[[NSUserDefaults standardUserDefaults] valueForKey:kLocationLat],
                                           @"longitude":[[NSUserDefaults standardUserDefaults] valueForKey:kLocationLong],
                                           @"address":[[NSUserDefaults standardUserDefaults] valueForKey:kHomeLocation],
                                           @"status":@"0"
                                           
                                           };
            NSLog(@"PostgetWorkHomeLocationsWithInfo%@",initdataInfo);
            [API PostupdateWorkHomeLocationWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
             {
                 NSLog(@"json=%@",json);
                 
             }];
        }
        else
        {
            [appDelegate() dismissActivityAlert];
            [appDelegate() errorMessages:kAlertInternetConnection];
        }
 
    }
    else if ([[[NSUserDefaults standardUserDefaults] valueForKey:kHomeSelection] isEqualToString:@"WorkBtnSelect"])
    {
         [_workLocationBtn setTitle:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:kWorkLocation]] forState:UIControlStateNormal];
        [appDelegate() CheckInternetConnection];
        if([appDelegate() internetWorking] ==0)
        {
            NSDictionary *initdataInfo = @{
                                           @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"],
                                           @"latitude":[[NSUserDefaults standardUserDefaults] valueForKey:kLocationLat],
                                           @"longitude":[[NSUserDefaults standardUserDefaults] valueForKey:kLocationLong],
                                           @"address":[[NSUserDefaults standardUserDefaults] valueForKey:kWorkLocation],
                                           @"status":@"1"
                                           
                                           };
            NSLog(@"PostgetWorkHomeLocationsWithInfo%@",initdataInfo);
            [API PostupdateWorkHomeLocationWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
             {
                 NSLog(@"json=%@",json);
                 
             }];
        }
        else
        {
            [appDelegate() dismissActivityAlert];
            [appDelegate() errorMessages:kAlertInternetConnection];
        }
        
    }

    
    
    
    
}
//-----------------------------------------------------------------------------------------------------------------------------
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
    }
    else
        if(buttonIndex ==1)
        {
            [appDelegate() CheckInternetConnection];
            if([appDelegate() internetWorking] ==0)
            {
                NSDictionary *initdataInfo = @{
                                               @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"]
                                               };
                NSLog(@"Category%@",initdataInfo);
                [API PostGetAllPostWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
                 {
                     NSLog(@"json=%@",json);
                     
                     NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
                     NSString *token = [[NSString alloc] init];
                     token = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"device_token"]];
                     [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
                     [[NSUserDefaults standardUserDefaults] setObject:kLoginData forKey:@""];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"device_token"];
                     [[NSUserDefaults standardUserDefaults]synchronize];
                     
                     
                     [[FBSession activeSession] closeAndClearTokenInformation];
                     NSArray *viewContrlls=[[self navigationController] viewControllers];
                     
                     NSLog(@"viewContrlls=%@",viewContrlls);
                     if(! [viewContrlls containsObject:[ViewController class]])
                     {
                         ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
                         [self.navigationController pushViewController:newView animated:YES];
                     }
                 }];
            }
            else
            {
                [appDelegate() dismissActivityAlert];
                [appDelegate() errorMessages:kAlertInternetConnection];
            }
            
        }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)func__btnBack
{
        [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIImaheView UI
    
    -(UIImageView*)createUIImageView:(CGRect)frame backgroundColor:(UIColor*)backgroundColor image:(UIImage*)image isLogo:(BOOL)isLogo{
        
        UIImageView *_imageView = [[UIImageView alloc]initWithFrame:frame];
        _imageView.image = image;
        _imageView.backgroundColor = backgroundColor;
        return _imageView;
    }
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UILable UI
    
    -(UILabel*)createLable:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
        
        UILabel *_lable = [[UILabel alloc] init];
        _lable.backgroundColor = backgroundColor;
        _lable.frame = frame;
        _lable.text = title;
        _lable.textColor = titleColor;
        _lable.font = font;
        
        return _lable;
    }
    //------------------------------------- bckgroundColor ----------------------------------------------------------------------------------------------
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIButton UI

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (![[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Cancel"]){
    
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
            mail.mailComposeDelegate = self;
            [mail setSubject:[actionSheet buttonTitleAtIndex:buttonIndex]];
            [mail setMessageBody:@"" isHTML:NO];
            [mail setToRecipients:@[@"info.fitnetapp@gmail.com"]];
    
            [self presentViewController:mail animated:YES completion:NULL];
        }
        else
        {
            NSLog(@"This device cannot send email");
        }
    }
}

-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
        
        UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
        _button.backgroundColor = backgroundColor;
        _button.frame = frame;
        [_button setTitle:title forState:UIControlStateNormal];
        [_button setTitleColor:titleColor forState:UIControlStateNormal];
        // [_botton setImage:[UIImage imageNamed:kimageFBFriends5] forState:UIControlStateNormal];
        // [_botton setImage:[UIImage imageNamed:kimageFBFriends5Hov] forState:UIControlStateHighlighted];
        
        _button.titleLabel.font = font;
        
        return _button;
    }
- (IBAction)sliderValueChanged:(id)sender{
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"sliderBarBoolYes"]==YES)
    {
        self.sliderTxtLabel.text =  [NSString stringWithFormat:@"%00.02f MILES", _sliderBar.value];
        
        //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //        [self dataRefreshApi];
        //    });
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"sliderBarBoolYes"];
        
    }
}

- (IBAction)sliderAction:(UISlider *)sender {
  
  
    //[self performSelector:@selector(dataRefreshApi) withObject:nil afterDelay:0.0];
    
    
}
-(void)dataRefreshApi
{
    [appDelegate() CheckInternetConnection];
    if([appDelegate() internetWorking] ==0)
    {
        _sliderBar.userInteractionEnabled = false;
        int result = (int)roundf( _sliderBar.value);
//        NSString* myNewString = [NSString stringWithFormat:@"%i", result];
        NSDictionary *initdataInfo = @{
                                       @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"],
                                       @"distance":[NSString stringWithFormat:@"%0.2f",_sliderBar.value *1609.34]
                                       };
        NSLog(@"Category%@",initdataInfo);
        [API getUpdateDistanceDataWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
         {
             NSLog(@"json=%@",json);
                 _sliderBar.userInteractionEnabled = true;
             [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"sliderBarBoolYes"];
             
         }];
    }
    else
    {
        [appDelegate() dismissActivityAlert];
        [appDelegate() errorMessages:kAlertInternetConnection];
    }
    
}
- (IBAction)termsBtn:(id)sender {
    TermsAndConditionVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditionVC"];
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)reportBtn:(id)sender {

    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"Choose an action" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Report a bug",@"Report an abuse",@"Suggestions for improvement", nil];
    [action showInView:self.view];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (IBAction)blockBtn:(id)sender {
    
//    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Block", @"Unblock", nil];
//    
//    actionSheet.tag = 999;
//    
//    [actionSheet showInView:self.view];
    
    BlockUnBlockUserVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"BlockUnBlockUserVC"];
    [self.navigationController pushViewController:VC animated:YES];

}
//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if(actionSheet.tag==999)
//    {
//        switch (buttonIndex)
//        {
//            case 0:
//            {
//                [self setBlockAPIcall];
//                
//                
//                break;
//            }
//                
//            case 1:
//            {
//                [self setUnblockAPICall];
//                break;
//            }
//                
//            case 2:
//            {
//                [self dismissViewControllerAnimated:YES completion:^
//                 {
//                     [[NSNotificationCenter defaultCenter]
//                      postNotificationName:@"UpdateProfileGallaryArray"
//                      object:self];
//                 }];
//                
//                break;
//                
//            }
//                
//            default:
//                break;
//        }
//    }
//}
-(void)setBlockAPIcall{
    [appDelegate() CheckInternetConnection];
    if([appDelegate() internetWorking] ==0)
    {
        NSLog(@"log=%@",[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"_id"]);
        NSDictionary *initdataInfo = @{
                                       @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"],
                                       @"customerId":[[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"userDetails"] valueForKey:@"_id"],
                                       @"status":@"True"
                                       };
        NSLog(@"Category%@",initdataInfo);
        [API getBlockUnBlockWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
         {
             NSLog(@"json=%@",json);
             
             UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:kAppName message:[NSString stringWithFormat:@"%@",[json valueForKey:@"message"]] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
             [alertview show];
             
         }];
    }
    else
    {
        [appDelegate() dismissActivityAlert];
        [appDelegate() errorMessages:kAlertInternetConnection];
    }
    
}
-(void)setUnblockAPICall{
    
    [appDelegate() CheckInternetConnection];
    if([appDelegate() internetWorking] ==0)
    {
        NSDictionary *initdataInfo = @{
                                       @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"],
                                       @"customerId":[[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"userDetails"] valueForKey:@"_id"],
                                       @"status":@"False"
                                       };
        NSLog(@"Category%@",initdataInfo);
        [API getBlockUnBlockWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
         {
                NSLog(@"json=%@",json);
             
             UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:kAppName message:[NSString stringWithFormat:@"%@",[json valueForKey:@"message"]] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
                [alertview show];
         }];
    }
    else
    {
        [appDelegate() dismissActivityAlert];
        [appDelegate() errorMessages:kAlertInternetConnection];
    }

    
}
- (IBAction)logoutBtn:(id)sender {
    
    UIAlertView * alertviewLogout = [[UIAlertView alloc] initWithTitle:@"Log Out" message:@"Are you sure you want to log out?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confirm",nil];
    alertviewLogout.tag=3;
    
    [alertviewLogout show];
}
- (IBAction)homeLocationBtn:(id)sender {
    
    selectBtnFlag =@"HomeBtnSelect";
    [[NSUserDefaults standardUserDefaults] setObject:selectBtnFlag forKey:kHomeSelection];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    MapLocationSetVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"MapLocationSetVC"];
    
    if(_strHomeAddress.length >0){
        newView.strHomeWorkAddress = _strHomeAddress;
        newView.strHomeWorkLng = _strHomeLng;
        newView.strHomeWorkLat = _strHomeLat;
    }
    
    [self.navigationController pushViewController:newView animated:YES];
}
- (IBAction)workLocationBtn:(id)sender {
    selectBtnFlag =@"WorkBtnSelect";
    [[NSUserDefaults standardUserDefaults] setObject:selectBtnFlag forKey:kHomeSelection];
    [[NSUserDefaults standardUserDefaults] synchronize];
    MapLocationSetVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"MapLocationSetVC"];
    
    if(_strWorkAddress.length >0){
        newView.strHomeWorkAddress = _strWorkAddress;
        newView.strHomeWorkLng = _strWorkLng;
        newView.strHomeWorkLat = _strWorkLat;
    }
    
    [self.navigationController pushViewController:newView animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)sliderValueDidChange:(id)sender {
    self.sliderTxtLabel.text =  [NSString stringWithFormat:@"%00.02f MILES", _sliderBar.value];
}

- (IBAction)sliderDidEnd:(id)sender {
}
-(void)blurSliderChanged{
    self.sliderTxtLabel.text =  [NSString stringWithFormat:@"%00.02f MILES", _sliderBar.value];

}
-(void)blurSliderEnded{
    [self dataRefreshApi];
    [[UserInfo sharedUserInfo] setStrFilterValue:self.sliderTxtLabel.text];

}
@end
