//
//  SinglePostVC.m
//  FitNetApp
//
//  Created by anish on 25/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "SinglePostVC.h"
#import "AsyncImageView.h"
@interface SinglePostVC ()

@end

@implementation SinglePostVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImageView *_topView =   [self createUIImageView:CGRectMake(0.0, 0, self.view.frame.size.width, 64) backgroundColor:[UIColor colorWithRed:57.0/255.0 green:160.0/255.0 blue:217.0/255.0 alpha:1.0] image:nil isLogo:YES];
    
    
    _topView.userInteractionEnabled=YES;
    
    UILabel *_lblIAM = [self createLable:CGRectMake(0.0, 25, self.view.frame.size.width, 30.0) bckgroundColor:[UIColor clearColor] title:@"SINGLE POST" font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    _lblIAM.textAlignment = NSTextAlignmentCenter;
    _lblIAM.textColor = [UIColor whiteColor];
    [_topView addSubview:_lblIAM];
    
    
    UIButton *_btnBackButton = [self createButton:CGRectMake(0, 20, 40, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"nav_back1"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    [_btnBackButton setImage:[UIImage imageNamed:@"nav_back1"] forState:UIControlStateNormal];
    [_btnBackButton addTarget:self action:@selector(func__btnBack) forControlEvents:UIControlEventTouchUpInside];
    [_btnBackButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_topView addSubview:_btnBackButton];
    
    
    
    [self.view addSubview:_topView];
    
    _labelAccesptCpunt.text = [NSString stringWithFormat:@" Accept Count: %@",_strAcceptCount];
    _singlePostTableView.layer.borderWidth = 1.0;
    _singlePostTableView.layer.borderColor = [UIColor colorWithRed:126.0/255.0 green:161.0/255.0 blue:177.0/255.0 alpha:1.0].CGColor;
    
    [_singlePostTableView setDelegate:self];
    _singlePostTableView.dataSource=self;
    [_singlePostTableView reloadData];

}
//---------------------------------
-(void)func__btnBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma -mark
#pragma -mark <TableView Delegate>
#pragma -mark
/************************* TableViewMethods **********************************/
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//-------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SinglePostCell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    AsyncImageView *_thumbImageView=(AsyncImageView *)[cell.contentView viewWithTag:1300];
    _thumbImageView.backgroundColor = [UIColor clearColor];
    _thumbImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _thumbImageView.layer.borderWidth=1.f;
    _thumbImageView.imageURL = [NSURL URLWithString:[[[[[_singlePostDic valueForKey:@"data"] valueForKey:@"customer"] valueForKey:@"profilePicURL"] valueForKey:@"thumbnail"] objectAtIndex:indexPath.row]];
    
    UILabel *label=(UILabel *)[cell.contentView viewWithTag:1301];
    label.text = [[[[[_singlePostDic valueForKey:@"data"] valueForKey:@"customer"] valueForKey:@"name"] objectAtIndex:indexPath.row] capitalizedString];
    
    UILabel *labelLoction=(UILabel *)[cell.contentView viewWithTag:1304];
    labelLoction.text = [NSString stringWithFormat:@"Location: %@",[[[[_singlePostDic valueForKey:@"data"]valueForKey:@"locationAddress"] objectAtIndex:indexPath.row]  capitalizedString]];
   
    UILabel *labelCount=(UILabel *)[cell.contentView viewWithTag:5000];
    labelCount.text = [NSString stringWithFormat:@"Attendees:%@",_strAcceptCount];
    
    
    UITextView *declabel=(UITextView *)[cell.contentView viewWithTag:1305];
    declabel.text = [[[_singlePostDic valueForKey:@"data"] valueForKey:@"description"] objectAtIndex:indexPath.row];
    declabel.textContainerInset = UIEdgeInsetsZero;
    declabel.textContainer.lineFragmentPadding = 0;
    //declabel.backgroundColor = [UIColor yellowColor];
    declabel.textAlignment = NSTextAlignmentLeft;
    
   UIButton *buttonAccept=(UIButton *)[cell.contentView viewWithTag:1307];
    [buttonAccept addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
}


#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIButton UI
-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
    _button.backgroundColor = backgroundColor;
    _button.frame = frame;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitleColor:titleColor forState:UIControlStateNormal];
    _button.titleLabel.font = font;
    
    return _button;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UILable UI

-(UILabel*)createLable:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UILabel *_lable = [[UILabel alloc] init];
    _lable.backgroundColor = backgroundColor;
    _lable.frame = frame;
    _lable.text = title;
    _lable.textColor = titleColor;
    _lable.font = font;
    
    return _lable;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIImaheView UI

-(UIImageView*)createUIImageView:(CGRect)frame backgroundColor:(UIColor*)backgroundColor image:(UIImage*)image isLogo:(BOOL)isLogo{
    
    UIImageView *_imageView = [[UIImageView alloc]initWithFrame:frame];
    _imageView.image = image;
    _imageView.backgroundColor = backgroundColor;
    return _imageView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//-------------------------
-(IBAction)buttonAction:(UIButton *)sender
{
    NSLog(@"id=%@",[[[_singlePostDic valueForKey:@"data"] valueForKey:@"_id"] objectAtIndex:0]);
    
    [appDelegate() CheckInternetConnection];
    if([appDelegate() internetWorking] ==0)
    {
        NSDictionary *initdataInfo = @{
                                       @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"],
                                       @"jobId":[[[_singlePostDic valueForKey:@"data"] valueForKey:@"_id"] objectAtIndex:0]
                                       };
        NSLog(@"Category%@",initdataInfo);
        [API acceptJobWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
         {
             NSLog(@"json=%@",json);
             
             UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:kAppName message:[NSString stringWithFormat:@"%@",[json valueForKey:@"message"]] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
             [alertview show];
             

             
             [self.navigationController popViewControllerAnimated:YES];
            
         }];
    }
    else
    {
        [appDelegate() dismissActivityAlert];
        [appDelegate() errorMessages:kAlertInternetConnection];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
