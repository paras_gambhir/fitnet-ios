//
//  AppDelegate.m
//  FitNetApp
//
//  Created by anish on 14/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "SVProgressHUD.h"
#import "Reachability.h"
#import "Constant.h"
#import "TabBarVC.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize locationManager;
@synthesize mainNavigationController;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {


    //ViewController *homeObj=VCWithIdentifier(@"TabBarVCPush");
    
//    [[IQKeyboardManager sharedManager] setEnable:YES];
    
//    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:15];
    //Enabling autoToolbar behaviour. If It is set to NO. You have to manually create IQToolbar for keyboard.
//    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    
    //Setting toolbar behavious to IQAutoToolbarBySubviews. Set it to IQAutoToolbarByTag to manage previous/next according to UITextField's tag property in increasing order.
//    [[IQKeyboardManager sharedManager] setToolbarManageBehaviour:IQAutoToolbarBySubviews];
    
    //Resign textField if touched outside of UITextField/UITextView.
//    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    //Giving permission to modify TextView's frame
//    [[IQKeyboardManager sharedManager] setCanAdjustTextView:YES];
    [GMSServices provideAPIKey:@"AIzaSyCNFiG-nK51XntCTigG1BQIOAXY0jrGAN4"];

    //Show TextField placeholder texts on autoToolbar
//    [[IQKeyboardManager sharedManager] setShouldShowTextFieldPlaceholder:YES];
    
    self.mainNavigationController = (UINavigationController*)self.window.rootViewController;
    self.mainNavigationController.navigationBarHidden = YES;
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"])
    {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TabBarVC *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"TabBarVCPush"];
     
        [AppDelegateClass.mainNavigationController pushViewController:loginViewController animated:NO];
        
    }
    else
    {
        
    }
    

#ifdef __IPHONE_8_0
    if(NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1)
    {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
#endif
    
    UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound |     UIRemoteNotificationTypeNewsstandContentAvailability;
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    
    
    application.applicationIconBadgeNumber=0;
    [UIApplication sharedApplication].statusBarHidden = NO;
    [UIApplication sharedApplication].networkActivityIndicatorVisible=FALSE;
    
    [self getCurrentlocation];
    
    
    // Override point for customization after application launch.
    return YES;
}
//------------------------------------------------------------------------------------------------------------------------------------------------------
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    
    // NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    //NSLog(@"String %@",str);
}
//------------------------------------------------------------------------------------------------------------------------------------------------------
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication];
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma mark - Public method implementation

-(void)openActiveSessionWithPermissions:(NSArray *)permissions allowLoginUI:(BOOL)allowLoginUI{
    [FBSession openActiveSessionWithReadPermissions:permissions
                                       allowLoginUI:allowLoginUI
                                  completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                      // Create a NSDictionary object and set the parameter values.
                                      NSDictionary *sessionStateInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                                                                        session, @"session",
                                                                        [NSNumber numberWithInteger:status], @"state",
                                                                        error, @"error",
                                                                        nil];
                                      
                                      // Create a new notification, add the sessionStateInfo dictionary to it and post it.
                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"SessionStateChangeNotification"
                                                                                          object:nil
                                                                                        userInfo:sessionStateInfo];
                                      
                                  }];
}
//------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark------------------------------------------------------------------------------------------
#pragma mark----------------------Method to get DeviceToken---------------------------------------------------
//- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
////------------------------------------------------------------------------------------------------------------------------------------------------------
//{
//    
//    NSString* newToken = [deviceToken description];
//    newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
//    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
//    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"device_token"])
//    {
//        
//        
//        [rNSUserDefault setObject:newToken forKey:kUserDeviceToken];
//        NSLog(@"Delegate DEVICE TOKEN IS %@",[[NSUserDefaults standardUserDefaults] valueForKey:kUserDeviceToken] );
//    }
//}
//------------------------------------------------------------------------------------------------------------------------------------------------------
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
//------------------------------------------------------------------------------------------------------------------------------------------------------
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}
//------------------------------------------------------------------------------------------------------------------------------------------------------
-(void)dismissActivityAlert
//------------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self HideActivityIndicator];
    
}
//------------------------------------------------------------------------------------------------------------------------------------------------------
#pragma mark---------------------------------------------------------------------------------------------
#pragma mark----------------------UIAlertView with Error Message---------------------------------------------
- (void)errorMessages:(NSString *)message
//------------------------------------------------------------------------------------------------------------------------------------------------------
{
    _errorAlert = [[UIAlertView alloc] initWithTitle:kAppName
                                             message:[NSString stringWithFormat:@"\n%@",message]
                                            delegate:nil
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil];
    [_errorAlert show];
    
}
//------------------------------------------------------------------------------------------------------------------------------------------------------
-(void)CheckInternetConnection
//------------------------------------------------------------------------------------------------------------------------------------------------------
{
    self.internetReach = [Reachability reachabilityForInternetConnection];
    [self.internetReach reachableOnWWAN];
    [self updateInterfaceWithReachability: self.internetReach];
}
#pragma mark-------------------------------------------------------------------
#pragma mark- Check Internet Connection using reachability methods

//------------------------------------------------------------------------------------------------------------------------------------------------------
- (void) updateInterfaceWithReachability: (Reachability*) curReach
//------------------------------------------------------------------------------------------------------------------------------------------------------
{
    if(curReach == self.internetReach)
    {
        NSLog(@"Internet");
        NetworkStatus netStatus = [curReach currentReachabilityStatus];
        switch (netStatus)
        {
            case NotReachable:
            {
                self.internetWorking = -1;
                NSLog(@"Internet NOT WORKING");
                break;
            }
            case ReachableViaWiFi:
            {
                self.internetWorking = 0;
                break;
            }
            case ReachableViaWWAN:
            {
                self.internetWorking = 0;
                break;
                
            }
        }
    }
}
//------------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------------------------------------
-(void)ShowActivityIndicatorWithTitle:(NSString *)Title
//------------------------------------------------------------------------------------------------------------------------------------------------------
{
    [SVProgressHUD showWithStatus:Title maskType:SVProgressHUDMaskTypeGradient];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];
}
//------------------------------------------------------------------------------------------------------------------------------------------------------
-(void)HideActivityIndicator
//------------------------------------------------------------------------------------------------------------------------------------------------------
{
    [SVProgressHUD dismiss];
}
#pragma mark locationManager
//  locationManager //**************
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    NSLog(@"lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
    
    NSString *currentLatitude = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString *currentLongitude = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    [rNSUserDefault removeObjectForKey:@"kUserCurrentLatitude"];
    [rNSUserDefault removeObjectForKey:@"kUserCurrentLongitude"];
    
    [rNSUserDefault setObject:currentLatitude forKey:kLAT];
    [rNSUserDefault setObject:currentLongitude forKey:kLNG];
    [rNSUserDefault synchronize];
    
    NSLog(@"lat%@ - lon%@",[[NSUserDefaults standardUserDefaults] valueForKey:kUserCurrentLatitude],[[NSUserDefaults standardUserDefaults] valueForKey:kUserCurrentLongitude]);
    
    [locationManager stopUpdatingLocation];
    
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status)
    {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            [locationManager requestAlwaysAuthorization];
        }
            break;
        default:
        {
            [locationManager startUpdatingLocation];
        }
            break;
    }
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([error code] == kCLErrorDenied){
        //you had denied
    }
    [manager stopUpdatingLocation];
}
-(void)getCurrentlocation
{
    // Current Location Code Here..
    locationManager = [CLLocationManager new];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [locationManager requestAlwaysAuthorization];
    }
    // Current Location Code Here..
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        [locationManager requestWhenInUseAuthorization];
    }
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager requestAlwaysAuthorization];
    [locationManager startUpdatingLocation];
    
    
}
//------------------------------------------------------------------------------------------------------------------------------------------------------
@end
AppDelegate *appDelegate(void)
//------------------------------------------------------------------------------------------------------------------------------------------------------
{
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
    
}
