//
//  ProfileVC.m
//  FitNetApp
//
//  Created by anish on 18/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "ProfileVC.h"
#import "SettingVC.h"
#import "IQKeyboardManager.h"
#import "AsyncImageView.h"
#import "CategoryVC.h"
#import "TimeVC.h"
#import "LevelVC.h"
#import "ViewReviewVC.h"
#import "Base64.h"
@interface ProfileVC ()
{
    @private
     UIImagePickerController *_groupPhotoPicker;
     NSString *imagePath;
     NSString *_base64Encoded;
    NSData *imageData;
}

@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *_topView =   [self createUIImageView:CGRectMake(0.0, 0, self.view.frame.size.width, 64) backgroundColor:[UIColor colorWithRed:57.0/255.0 green:160.0/255.0 blue:217.0/255.0 alpha:1.0] image:nil isLogo:YES];
    
    
    _topView.userInteractionEnabled=YES;
    
    
    UIButton *_btnSetting = [self createButton:CGRectMake(self.view.frame.size.width-50, 20, 40, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"nav_settings1"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    [_btnSetting setImage:[UIImage imageNamed:@"nav_settings1"] forState:UIControlStateNormal];
    [_btnSetting addTarget:self action:@selector(func__btnSettingButton) forControlEvents:UIControlEventTouchUpInside];
    //[_btnPlus setTitle:@"Plus" forState:UIControlStateNormal];
    _btnSetting.backgroundColor = [UIColor clearColor];
    [_topView addSubview:_btnSetting];

    
    UILabel *_lblIAM = [self createLable:CGRectMake(0.0, 25, self.view.frame.size.width, 30.0) bckgroundColor:[UIColor clearColor] title:@"PROFILE" font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    _lblIAM.textAlignment = NSTextAlignmentCenter;
    _lblIAM.textColor = [UIColor whiteColor];
    [_topView addSubview:_lblIAM];
    
    
    [self.view addSubview:_topView];
    
    [[_userProfileImage layer] setBorderWidth:0.8f];
    [[_userProfileImage layer] setBorderColor:[UIColor blackColor].CGColor];
    
    
    [[_aboutTextField layer] setBorderWidth:0.7f];
    [[_aboutTextField layer] setBorderColor:[UIColor colorWithRed:214.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1.0].CGColor];
    
    [[_fitnessBtn layer] setBorderWidth:0.7f];
    [[_fitnessBtn layer] setBorderColor:[UIColor colorWithRed:214.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1.0].CGColor];
    
    [[_levelBtn layer] setBorderWidth:0.7f];
    [[_levelBtn layer] setBorderColor:[UIColor colorWithRed:214.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1.0].CGColor];
    
    [[_sessionTextField layer] setBorderWidth:0.7f];
    [[_sessionTextField layer] setBorderColor:[UIColor colorWithRed:214.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1.0].CGColor];

    [[_timeBtnShow layer] setBorderWidth:0.7f];
    [[_timeBtnShow layer] setBorderColor:[UIColor colorWithRed:214.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1.0].CGColor];

    
    [[_goodForField layer] setBorderWidth:0.7f];
    [[_goodForField layer] setBorderColor:[UIColor colorWithRed:214.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1.0].CGColor];

    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _aboutTextField.leftView = paddingView;
    _aboutTextField.leftViewMode = UITextFieldViewModeAlways;
    

    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _sessionTextField.leftView = paddingView3;
    _sessionTextField.leftViewMode = UITextFieldViewModeAlways;
    

    
    UIView *paddingView6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _goodForField.leftView = paddingView6;
    _goodForField.leftViewMode = UITextFieldViewModeAlways;
    
    // Do any additional setup after loading the view.
    
    _saveBtn.backgroundColor = [UIColor colorWithRed:57.0/255.0 green:160.0/255.0 blue:217.0/255.0 alpha:1.0];
    
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"profileListServerBoolYes"];
    
    
    _fitnessBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_fitnessBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    _timeBtnShow.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_timeBtnShow setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    _levelBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_levelBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Select"forKey:kFitness];
    [[NSUserDefaults standardUserDefaults] setObject:@""forKey:kSelectIdData];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Select"forKey:kTime];
    [[NSUserDefaults standardUserDefaults] setObject:@""  forKey:kTimeIdData];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Select"forKey:kLevel];
    
    
    self.ratingView.delegate = self;
    self.ratingView.emptySelectedImage = [UIImage imageNamed:@"star_inactive"];
    self.ratingView.fullSelectedImage = [UIImage imageNamed:@"star_active"];
    self.ratingView.contentMode = UIViewContentModeScaleAspectFill;
    self.ratingView.maxRating = 5;
    self.ratingView.minRating = 0;
    
    self.ratingView.editable = NO;
    self.ratingView.halfRatings = NO;
    self.ratingView.floatRatings = NO;
    
    self.ratingLabel.text = [NSString stringWithFormat:@"%.2f", self.ratingView.rating];
    self.liveLabel.text = [NSString stringWithFormat:@"%.2f", self.ratingView.rating];
    
    
}
-(void)FirstFunCall
{
    [_fitnessBtn setTitle:[NSString stringWithFormat:@" %@",[[NSUserDefaults standardUserDefaults] valueForKey:kFitness]] forState:UIControlStateNormal];
    
    [_timeBtnShow setTitle:[NSString stringWithFormat:@" %@",[[NSUserDefaults standardUserDefaults] valueForKey:kTime]] forState:UIControlStateNormal];
    
    [_levelBtn setTitle:[NSString stringWithFormat:@" %@",[[NSUserDefaults standardUserDefaults] valueForKey:kLevel]] forState:UIControlStateNormal];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"profileListServerBoolYes"]==YES)
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"profileListServerBoolYes"];
        _imageTopBar.hidden = true;
        _editView.hidden = true;
        _backgroundImage.hidden = true;
        [appDelegate() CheckInternetConnection];
        if([appDelegate() internetWorking] ==0)
        {
            NSDictionary *initdataInfo = @{
                                           @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"]
                                           };
            NSLog(@"Get Profile ***%@",initdataInfo);
            [API getProfileDataWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
             {
                 NSLog(@"=%@",json);
                 [[NSUserDefaults standardUserDefaults] setObject:[json valueForKey:@"data"]  forKey:kAllProfileData];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 
                 _userProfileImage.backgroundColor = [UIColor clearColor];
                 _userProfileImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
                 _userProfileImage.layer.borderWidth=1.f;
                 
                 
                 _userProfileImage.imageURL = [NSURL URLWithString:[[[[[NSUserDefaults standardUserDefaults] valueForKey:kAllProfileData] valueForKey:@"userDetails"] valueForKey:@"profilePicURL"] valueForKey:@"thumbnail"]];
                 _userProfileImage.userInteractionEnabled = true;
                 
                 _imageUploarder.backgroundColor=[UIColor clearColor];
                 _imageUploarder.userInteractionEnabled=true;
                 _imageUploarder.imageURL = [NSURL URLWithString:[[[[[NSUserDefaults standardUserDefaults] valueForKey:kAllProfileData] valueForKey:@"userDetails"] valueForKey:@"profilePicURL"] valueForKey:@"thumbnail"]];
                 _imageUploarder.layer.borderColor = [UIColor lightGrayColor].CGColor;
                 _imageUploarder.layer.borderWidth=1.f;
                 
                 _userNameLbl.text= [[[[NSUserDefaults standardUserDefaults] valueForKey:kAllProfileData] valueForKey:@"userDetails"] valueForKey:@"name"];
                 
                 //_userLocationLbl.text= [[[[NSUserDefaults standardUserDefaults] valueForKey:kAllProfileData] valueForKey:@"userDetails"] valueForKey:@"name"];
                 
                 NSString *abc = [[[[NSUserDefaults standardUserDefaults] valueForKey:kAllProfileData]   valueForKey:@"userDetails"] valueForKey:@"aboutMe"];
               
                 _totalReviewLbl.text=[NSString stringWithFormat:@"%@ Review",[[[[NSUserDefaults standardUserDefaults] valueForKey:kAllProfileData]   valueForKey:@"userDetails"] valueForKey:@"totalReviews"]];
                 
                 
                 self.ratingView.rating = [[[[[NSUserDefaults standardUserDefaults] valueForKey:kAllProfileData]   valueForKey:@"userDetails"] valueForKey:@"Rating"] integerValue];
                 
                 
                 if (abc.length==0)
                 {
                     _aboutMeLbl.text= @"";
                 }
                 else
                 {
                     _aboutMeLbl.text= abc;
                 }
                 
                 NSMutableArray *arrayCount;
                 arrayCount =[[NSMutableArray alloc]init];
                 
                 arrayCount = [[[[NSUserDefaults standardUserDefaults] valueForKey:kAllProfileData] valueForKey:@"userDetails"] valueForKey:@"categories"];
                 
                 NSString *abci =@"";
                 for (int i =0 ; i <arrayCount.count;i++)
                 {
                     abci = [abci stringByAppendingFormat:@" , %@",[[arrayCount valueForKey:@"name"] objectAtIndex:i]];
                     
                 }
                 abci = [abci stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                 
                 if ([abci hasPrefix:@","] && [abci length] > 1) {
                     abci = [abci substringFromIndex:1];
                 }
                 NSLog(@"abci=%@",abci);
                 if (abci.length==0)
                 {
                     _fitnessLbl.text= @"";
                 }
                 else
                 {
                     _fitnessLbl.text= abci;
                 }
                 NSLog(@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:kAllProfileData] valueForKey:@"userDetails"]);
                 NSString *dd=[[[[NSUserDefaults standardUserDefaults] valueForKey:kAllProfileData] valueForKey:@"userDetails"] valueForKey:@"sessions"];
                 NSLog(@"dd=%@",dd);
                 _sessionTextField.text = [NSString stringWithFormat:@"%@",dd];
        
                     _sessionLbl.text= [NSString stringWithFormat:@"%@",dd];
             

                 NSString *time=[[[[[NSUserDefaults standardUserDefaults] valueForKey:kAllProfileData] valueForKey:@"userDetails"] valueForKey:@"timings"] valueForKey:@"Timing"];
                 
                 NSLog(@"time=%@",time);
                 
                 if(time.length==0)
                 {
                     _timeLbl.text= @"";
                 }
                 else
                 {
                     _timeLbl.text= time;
                 }

                 NSString *advanced=[[[[NSUserDefaults standardUserDefaults] valueForKey:kAllProfileData] valueForKey:@"userDetails"] valueForKey:@"level"];
                 
                 if(advanced.length==0)
                 {
                     
                     _advancedLbl.text= @"";
                 }
                 else
                 {
                     _advancedLbl.text= advanced;
                 }
                 
                 NSString *goodfor=[[[[NSUserDefaults standardUserDefaults] valueForKey:kAllProfileData] valueForKey:@"userDetails"] valueForKey:@"goodFor"];
                 if(goodfor.length==0)
                 {
                     
                     _motivationLbl.text= @"";
                 }
                 else
                 {
                     _motivationLbl.text= goodfor;
                 }
                 
             }];
        }
        else
        {
            [appDelegate() dismissActivityAlert];
            [appDelegate() errorMessages:kAlertInternetConnection];
        }
        
        
    }
  
}
-(void)viewWillAppear:(BOOL)animated
{
    [self FirstFunCall];
}
-(void)func__btnSettingButton
{
    SettingVC *SettingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
    [self.navigationController pushViewController:SettingVC animated:YES];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIImaheView UI

-(UIImageView*)createUIImageView:(CGRect)frame backgroundColor:(UIColor*)backgroundColor image:(UIImage*)image isLogo:(BOOL)isLogo{
    
    UIImageView *_imageView = [[UIImageView alloc]initWithFrame:frame];
    _imageView.image = image;
    _imageView.backgroundColor = backgroundColor;
    return _imageView;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UILable UI

-(UILabel*)createLable:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UILabel *_lable = [[UILabel alloc] init];
    _lable.backgroundColor = backgroundColor;
    _lable.frame = frame;
    _lable.text = title;
    _lable.textColor = titleColor;
    _lable.font = font;
    
    return _lable;
}
//------------------------------------- bckgroundColor ----------------------------------------------------------------------------------------------
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIButton UI

-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
    _button.backgroundColor = backgroundColor;
    _button.frame = frame;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitleColor:titleColor forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5] forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5Hov] forState:UIControlStateHighlighted];
    
    _button.titleLabel.font = font;
    
    return _button;
}
- (IBAction)picEditBtn:(UIButton*)sender
{
    if (sender.selected == true)
    {
        sender.selected =true;
        _imageTopBar.hidden = true;
        _backgroundImage.hidden = true;
    }
    else
    {
        _backgroundImage.hidden = false;
        _imageTopBar.hidden = false;
        sender.selected = false;
    }
}
- (IBAction)uploadImageAction:(id)sender {
}
- (IBAction)crossBtnAction:(id)sender
{
    [self.view endEditing:YES];
    _imageTopBar.hidden = true;
    _backgroundImage.hidden = true;
}
- (IBAction)profileEditAction:(UIButton*)sender {
   
    if (sender.selected == true)
    {
        sender.selected =true;
        _editView.hidden = true;
        _backgroundImage.hidden = true;
    }
    else
    {
        _backgroundImage.hidden = false;
        _editView.hidden = false;
        sender.selected = false;
    }
    
}

- (IBAction)editCrossProfileBtnAction:(id)sender {
    [self.view endEditing:YES];
    [[NSUserDefaults standardUserDefaults] setObject:@"Select"forKey:kFitness];
    [[NSUserDefaults standardUserDefaults] setObject:@"Select"forKey:kTime];
    [[NSUserDefaults standardUserDefaults] setObject:@""forKey:kSelectIdData];
    [[NSUserDefaults standardUserDefaults] setObject:@""  forKey:kTimeIdData];
    _editView.hidden = true;
    _backgroundImage.hidden = true;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)saveBtn:(id)sender{
    [appDelegate() CheckInternetConnection];
    if([appDelegate() internetWorking] ==0){
        
        if([[_aboutTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0){
            [super showAlert:nil :@"Please fill about me"];
        }
        else if ([_fitnessBtn.titleLabel.text isEqualToString:@" Select"]){
            [super showAlert:nil :@"Please select fitness"];
        }
        else if ([_timeBtnShow.titleLabel.text isEqualToString:@" Select"]){
            [super showAlert:nil :@"Please select Time"];
        }
        else if ([_levelBtn.titleLabel.text isEqualToString:@" Select"]){
            [super showAlert:nil :@"Please select level"];
        }        else{
            
            NSString *about=  [_aboutTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            // NSString *fitness=  [_fitnessTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            NSString *sesstion=  [_sessionTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            //  NSString *time=  [_timeTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            
            //         NSString *goodFor=  [_goodForField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            
            NSError *error=nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[[NSUserDefaults standardUserDefaults] valueForKey:kSelectIdData] options:NSJSONWritingPrettyPrinted error:&error];
            
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            NSLog(@"jsonString=%@",jsonString);
            
            NSDictionary *initdataInfo = @{
                                           @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"],
                                           @"aboutMe":about,
                                           @"fitness":jsonString,
                                           @"timingId":[[NSUserDefaults standardUserDefaults] valueForKey:kTimeIdData],
                                           @"level":[[NSUserDefaults standardUserDefaults] valueForKey:kLevel]
                                           };
            NSLog(@"Category%@",initdataInfo);
            [API PostEditProfileFeedWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
             {
                 NSLog(@"json=%@",json);
                 [[NSUserDefaults standardUserDefaults] setObject:@"Fitness"forKey:kFitness];
                 [[NSUserDefaults standardUserDefaults] setObject:@"Time"forKey:kTime];
                 [[NSUserDefaults standardUserDefaults] setObject:@""forKey:kSelectIdData];
                 [[NSUserDefaults standardUserDefaults] setObject:@""  forKey:kTimeIdData];
                 _editView.hidden = true;
                 _backgroundImage.hidden = true;
                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"profileListServerBoolYes"];
                 [self FirstFunCall];
                 UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:kAppName message:[NSString stringWithFormat:@"%@",[json valueForKey:@"message"]] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
                 [alertview show];
                 
             }];
        }
        
        
        
   
    }
    else
    {
        [appDelegate() dismissActivityAlert];
        [appDelegate() errorMessages:kAlertInternetConnection];
    }
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
        switch (buttonIndex)
        {
            case 0:
            {
                if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                {
                    
                }
                else
                {
                    _groupPhotoPicker = [[UIImagePickerController alloc]init];
                    _groupPhotoPicker.delegate = self;
                    _groupPhotoPicker.allowsEditing = YES;
                    _groupPhotoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    [self presentViewController:_groupPhotoPicker animated:YES completion:NULL];
                }
            }
                break;
                
            case 1:
            {
                _groupPhotoPicker = [[UIImagePickerController alloc]init];
                _groupPhotoPicker.delegate = self;
                _groupPhotoPicker.allowsEditing = YES;
                _groupPhotoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:_groupPhotoPicker animated:YES completion:NULL];
                break;
            }
                
            default:
                break;
        }
}
#pragma mark - UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary* )info
{
    
    UIImage *image1 = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    imageData = UIImagePNGRepresentation(image1);
    //data = UIImagePNGRepresentation(image,0.2f);
    imageData = UIImageJPEGRepresentation(image1, 0.2f);
//    _base64Encoded = [[NSString alloc] initWithString:[Base64 encode:data]];
    
    _userProfileImage.image = image1;
    _imageUploarder.image = image1;
    [self uploadImage];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)uploadImage{
    [super showLoader];

    NSString *url=[[NSString stringWithFormat:@"%@api/customer/updateImage",kBaseUrl] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSDictionary *initdataInfo = @{@"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"]};

    [APIManager postPic:url :initdataInfo :YES :imageData :^(NSDictionary *response_success) {
        [super hideLoader];
        [self FirstFunCall];
        NSLog(@"%@",response_success);
        if([[response_success valueForKey:@"statusCode"] integerValue] == 200){
            [_userProfileImage sd_setImageWithURL:[NSURL URLWithString:[response_success valueForKeyPath:@"data.original"]]];
        }
    } :^(NSError *response_error) {
        NSLog(@"%@",response_error);
        [super hideLoader];
    }];
}

-(void)imageUploadFun
{
    [appDelegate() CheckInternetConnection];
    if([appDelegate() internetWorking] ==0)
    {
        NSDictionary *initdataInfo = @{
                                       @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"],
                                       @"profilePic":_base64Encoded,
                                       };
        NSLog(@"image%@",initdataInfo);
        [API getupdateImageWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
         {
             NSLog(@"json=%@",json);
             
             UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:kAppName message:[NSString stringWithFormat:@"%@",[json valueForKey:@"message"]] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
             [alertview show];
             
         }];
    }
    else
    {
        [appDelegate() dismissActivityAlert];
        [appDelegate() errorMessages:kAlertInternetConnection];
    }
}


#pragma Mark -TextFieldDidBeginEditing
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == 5600)
    {
      
        //[_fitnessTextField resignFirstResponder];
      //  [self.view endEditing:true];
    }
    if (textField.tag == 5700)
    {
        
    }
}
- (IBAction)uploadPhoto:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Your Photo"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Camera", @"Select from Library", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];

}
-(void)keyBoard
{
    [self.view endEditing:true]; 
}
- (IBAction)fitnessBtnAction:(id)sender
{
      [self keyBoard];
        [[NSUserDefaults standardUserDefaults] setObject:@"Select"forKey:kFitness];
        [[NSUserDefaults standardUserDefaults] setObject:@""forKey:kSelectIdData];

        CategoryVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoryVC"];
        [self.navigationController pushViewController:newView animated:YES];

}
- (IBAction)timeBtnAction:(id)sender
{
    [self keyBoard];
     [[NSUserDefaults standardUserDefaults] setObject:@"Select"forKey:kTime];
        [[NSUserDefaults standardUserDefaults] setObject:@""  forKey:kTimeIdData];
    TimeVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"TimeVC"];
    [self.navigationController pushViewController:newView animated:YES];
}
- (IBAction)levelBtn:(id)sender {
    
    [self keyBoard];
    [[NSUserDefaults standardUserDefaults] setObject:@"Select"forKey:kLevel];
    LevelVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LevelVC"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)getReviewAction:(id)sender {
    
    [self.view endEditing:true];
    ViewReviewVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewReviewVC"];
    [self.navigationController pushViewController:newView animated:YES];
}


/*
 
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
