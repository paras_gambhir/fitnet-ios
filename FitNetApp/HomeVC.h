//
//  HomeVC.h
//  FitNetApp
//
//  Created by anish on 18/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseControllerVC.h"
#import "CustomLocationVC.h"
#import "UserInfo.h"

@interface HomeVC : BaseControllerVC<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,CustomLocationDelegate>

@property (weak, nonatomic) IBOutlet UIButton *CategoryBtn;
@property (weak, nonatomic) IBOutlet UIButton *loctionBtn;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImagePopup;
@property (weak, nonatomic) IBOutlet UITableView *homeTableview;
@property (weak, nonatomic) IBOutlet UIButton *crossButton;
@property (weak, nonatomic) IBOutlet UILabel *fitnnessLabel;
@property (weak, nonatomic) IBOutlet UITableView *timeTableView;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;

@property NSMutableDictionary *dictAPI;

@property NSString *strLat;
@property NSString *strLng;

@end
