//
//  ApiMaster.h
//  Go Boost Me
//
//  Created by Eshan cheema on 2/6/15.
//  Copyright (c) 2015 Eshan cheema. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^APICompletionHandler)(NSDictionary* json,NSError *error);

@interface ApiMaster : NSObject

+(ApiMaster*)singleton;

-(void)PostInitdataUserWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;
-(void)getHomeWorkLAtLong:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;
-(void)PostCategoryListWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;
-(void)getUsersList:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)blockUnblockUser:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)PostAccessTokenWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;
-(void)viewReviewOfOtherUser:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)getMapData:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)PostToFeedWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;
-(void)PostViewReview:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)PostToAddFeedWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)PostEditProfileFeedWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)PostCurrentActivityFeedWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)GetSingleDetailsWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)acceptJobWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)getProfileDataWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;


-(void)getUpdateDistanceDataWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)getBlockUnBlockWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)getupdateImageWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)PostEndJobWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)LogoutWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;


-(void)PostGetAllPostWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;


-(void)PostReviewViewWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)PostGetMapViewWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)PostgetWorkHomeLocationsWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;

-(void)PostupdateWorkHomeLocationWithInfo:(NSMutableDictionary*)userInfo completionHandler:(APICompletionHandler)handler;



-(void)GetDirections:(APICompletionHandler)handler;
@end
