//
//  CustomCategory.h
//  FitNetApp
//
//  Created by Kunal Gupta on 12/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCategory : UIView<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UITextField *tfCategory;
- (IBAction)actionBtnDone:(id)sender;
-(id)initWithFrame:(CGRect)frame;
@property (weak, nonatomic) IBOutlet UIView *viewContent;

@end
