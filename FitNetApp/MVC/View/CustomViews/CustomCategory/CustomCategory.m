//
//  CustomCategory.m
//  FitNetApp
//
//  Created by Kunal Gupta on 12/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "CustomCategory.h"

@implementation CustomCategory


-(id)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self){
        self = [[[NSBundle mainBundle] loadNibNamed:@"CustomCategory" owner:self options:nil] firstObject];
        [self setFrame:frame];
    }
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
    [self initialize];
    [self showWithAnimation];
    return self;
}

-(void)initialize{
    _viewContent.transform = CGAffineTransformMakeTranslation(0, 600);
    [_tfCategory becomeFirstResponder];
    _tfCategory.delegate = self;
}

-(void)showWithAnimation{
    
    [UIView animateWithDuration:0.6 delay:0.5 usingSpringWithDamping:0.6 initialSpringVelocity:0.8 options:UIViewAnimationOptionCurveLinear animations:^{
        _viewContent.transform = CGAffineTransformMakeTranslation(0, 0);
        [_viewBackground setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    }completion:^(BOOL finished) {
        nil;
    }];
}

-(void)removeWithAnimation{
    
    [UIView animateWithDuration:0.3 animations:^{
        _viewContent.transform = CGAffineTransformMakeTranslation(0, 600);
        [_viewBackground setBackgroundColor:[UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha:0]];
        NSMutableDictionary *dict = [NSMutableDictionary new];
        [dict setObject:_tfCategory.text forKey:@"cat"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"custom_category" object:dict];
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    if ([[_tfCategory.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] != 0){
        [self removeWithAnimation];
    }
    return  YES;
}


- (IBAction)actionBtnDone:(id)sender {

    if ([[_tfCategory.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] != 0){
        [self removeWithAnimation];
    }
}
@end
