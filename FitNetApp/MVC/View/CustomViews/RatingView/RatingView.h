//
//  RatingView.h
//  FitNetApp
//
//  Created by Kunal Gupta on 25/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPFloatRatingView.h"
#import <SZTextView/SZTextView.h>
#import "CurrentActivityModel.h"
#import <UIImageView+WebCache.h>

@interface RatingView : UIView

-(id)initWithFrame:(CGRect)frame : (CurrentActivityModel *)model;

@property CurrentActivityModel *model;
@property (weak, nonatomic) IBOutlet UIImageView *imageVIewProfilePIc;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet SZTextView *textViewFeedback;
@property (weak, nonatomic) IBOutlet UIButton *btncancel;
@property (weak, nonatomic) IBOutlet UIButton *btnOK;
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet TPFloatRatingView *viewRating;

- (IBAction)actionBtncancel:(id)sender;

- (IBAction)actionBtnOK:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewContent;


@end
