//
//  RatingView.m
//  FitNetApp
//
//  Created by Kunal Gupta on 25/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "RatingView.h"
#import "BaseControllerVC.h"

@implementation RatingView

-(id)initWithFrame:(CGRect)frame : (CurrentActivityModel *)model{
    
    self = [super initWithFrame:frame];
    
    if (self){
        self = [[[NSBundle mainBundle] loadNibNamed:@"RatingView" owner:self options:nil] firstObject];
        [self setFrame:frame];
    }
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
    [self initialize:model];
    [self showWithAnimation];
    return self;
}

#pragma mark - SELF MADE

-(void)initialize:(CurrentActivityModel *)model{
    _viewContent.transform = CGAffineTransformMakeTranslation(0, 600);
    [_viewBackground setBackgroundColor:[UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha:0]];
    _imageVIewProfilePIc.layer.cornerRadius = _imageVIewProfilePIc.frame.size.width/2;
    [_imageVIewProfilePIc setClipsToBounds:YES];
    _model = model;
    self.viewRating.emptySelectedImage = [UIImage imageNamed:@"star_inactive"];
    self.viewRating.fullSelectedImage = [UIImage imageNamed:@"star_active"];
    self.viewRating.contentMode = UIViewContentModeScaleAspectFill;
    self.viewRating.maxRating = 5;
    self.viewRating.minRating = 0;
//    self.viewRating.rating = [model.strRating floatValue];
    self.viewRating.editable = YES;
    self.viewRating.halfRatings = YES;
    self.viewRating.floatRatings = NO;
    _viewRating.backgroundColor = [UIColor clearColor];
    
    [_imageVIewProfilePIc sd_setImageWithURL:[NSURL URLWithString:model.strImage]];
    _labelName.text = model.strName;
    
    
}

-(void)showWithAnimation{
    
    [UIView animateWithDuration:0.6 delay:0.5 usingSpringWithDamping:0.6 initialSpringVelocity:0.8 options:UIViewAnimationOptionCurveLinear animations:^{
        _viewContent.transform = CGAffineTransformIdentity;
        [_viewBackground setBackgroundColor:[UIColor colorWithRed:170/255.f green:170/255.f blue:170/255.f alpha:0.7]];
    }completion:^(BOOL finished) {
        nil;
    }];
}

-(void)removeWithAnimation{
    
    [UIView animateWithDuration:0.3 animations:^{
        _viewContent.transform = CGAffineTransformMakeTranslation(0, 600);
        [_viewBackground setBackgroundColor:[UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha:0]];
    }completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (IBAction)actionBtncancel:(id)sender {
    [self removeWithAnimation];
}

- (IBAction)actionBtnOK:(id)sender {
    
    BaseControllerVC *vc = [BaseControllerVC new];
    if(_viewRating.rating == 0){
        [vc showAlert:@"Please rate the user" :@"ERROR"];
    }
    else if ([[_textViewFeedback.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0){
        [vc showAlert:@"Please give feedback" :@"ERROR"];
    }
    else{
        NSMutableDictionary *dict = [NSMutableDictionary new];
        [dict setObject:_model.strID forKey:@"id"];
        [dict setObject:[NSString stringWithFormat:@"%f",_viewRating.rating] forKey:@"rating"];
        [dict setObject:_textViewFeedback.text forKey:@"comments"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"rate" object:nil userInfo:dict];
        [self removeWithAnimation];
    }
    
}
@end
