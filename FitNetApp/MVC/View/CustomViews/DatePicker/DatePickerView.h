//
//  DatePickerView.h
//  COIL
//
//  Created by Aseem 13 on 06/05/16.
//  Copyright © 2016 Aseem 9. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SendDateBackDelegate
//-(void)cancelPressed;
//-(void)sendStartDateBack :(NSString*)lblDate :(NSString*)formattedDate;
//-(void)sendEndDateBack :(NSString*)lblDate :(NSString*)formattedDate;

-(void)getSessionTime:(NSString *)strTime :(NSDate *)strFormat;

@end

@interface DatePickerView : UIView
@property BOOL stratEvent;
-(void)setBoolValue :(BOOL)value :(NSString *)string :(NSString *)Endstring;
@property(nonatomic,strong)id<SendDateBackDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;

- (IBAction)actionBtnDone:(id)sender;

@end
