//
//  DatePickerView.m
//  COIL
//
//  Created by Aseem 13 on 06/05/16.
//  Copyright © 2016 Aseem 9. All rights reserved.
//

#import "DatePickerView.h"
@interface DatePickerView()

{
    NSDateFormatter *dateFormatter;
}
@end

@implementation DatePickerView

-(void)awakeFromNib
{
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy hh:mm a"];
    _datePicker.minimumDate = NSDate.date;
    
}


-(void)setBoolValue :(BOOL)value :(NSString*)string :(NSString *)Endstring
{
    _stratEvent=value;
    
    if (string!=nil) {
        NSDate *date =[self DateFromString:string];
        _datePicker.minimumDate=date;
    }
    
    if (Endstring!=nil) {
        NSDate *date =[self DateFromString:Endstring];
        _datePicker.maximumDate=date;
    }
   
}

-(NSDate*)DateFromString:(NSString*)str
{

    NSDateFormatter *Formatter = [[NSDateFormatter alloc] init];
   [Formatter setDateFormat:@"dd MMM yyyy HH:mm a"];
    NSDate *date = [Formatter dateFromString:str];
    return  date;
}

- (IBAction)actionBtnDone:(id)sender {
    NSLog(@"time is%@",[dateFormatter stringFromDate:self.datePicker.date]);
    [self.delegate getSessionTime:[dateFormatter stringFromDate:self.datePicker.date]:_datePicker.date];
    [self removeFromSuperview];
}

@end
