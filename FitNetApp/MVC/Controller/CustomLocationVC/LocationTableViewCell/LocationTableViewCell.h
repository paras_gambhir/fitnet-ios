//
//  LocationTableViewCell.h
//  FitNetApp
//
//  Created by Kunal Gupta on 29/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labellocation;

@end
