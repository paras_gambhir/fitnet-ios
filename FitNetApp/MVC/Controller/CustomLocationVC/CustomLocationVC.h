//
//  CustomLocationVC.h
//  FitNetApp
//
//  Created by Kunal Gupta on 29/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationTableViewCell.h"
#import "BaseControllerVC.h"
#import <GoogleMaps/GMSPlace.h>

@protocol CustomLocationDelegate
-(void)chooseLocation:(NSString *)lat :(NSString *)lng :(NSString *)strAddress;
@end

@interface CustomLocationVC : BaseControllerVC<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewTop;

@property (weak, nonatomic) IBOutlet UITextField *textFieldSearch;
@property (weak, nonatomic) IBOutlet GMSMapView *viewMap;

@property (weak, nonatomic) IBOutlet UILabel *labelAlert;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property NSMutableArray *arrTableData;
- (IBAction)textFieldDidChange:(id)sender;
@property id <CustomLocationDelegate> delegate;
- (IBAction)actionBtnBack:(id)sender;
@property GMSPlacesClient *placeClient;

@end
