//
//  CustomLocationVC.m
//  FitNetApp
//
//  Created by Kunal Gupta on 29/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "CustomLocationVC.h"
#import "APIManager.h"

@implementation CustomLocationVC


-(void)viewDidLoad{
    [super viewDidLoad];
    [_tableView setHidden:YES];
    _tableView.layer.borderColor = [[UIColor blackColor] CGColor];
    _tableView.layer.borderWidth = 1.0;
    NSString *strLat = [[NSUserDefaults standardUserDefaults] valueForKey:kLAT];
    NSString *strLng = [[NSUserDefaults standardUserDefaults] valueForKey:kLNG];
    GMSCameraPosition* camera = [GMSCameraPosition cameraWithLatitude:[strLat doubleValue]
                                                            longitude:[strLng doubleValue]
                                                                 zoom:16];

        _viewMap.myLocationEnabled = YES;
    
    NSLog(@"%f",_viewMap.myLocation.coordinate.latitude);
    NSLog(@"%f",_viewMap.myLocation.coordinate.longitude);
    _viewMap.settings.compassButton = YES;
    [_viewMap setCamera:camera];

}


#pragma mark - TABLE VIEW

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrTableData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LocationTableViewCell"];
    if(indexPath.row%2==0)
    {
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    cell.labellocation.text = [[_arrTableData objectAtIndex:indexPath.row] valueForKeyPath:@"description"];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [_tableView setHidden:YES];
    LocationTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self getLatLng:[[_arrTableData objectAtIndex:indexPath.row] valueForKeyPath:@"place_id"] :cell.labellocation.text];
    
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
}

#pragma mark - TEXT FIELD DELEGATE

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{

    textField.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f];
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField{
    textField.text = @"";
    [textField resignFirstResponder];
    _tableView.hidden = YES;
    
    return NO;
}
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    NSLog(@"textFieldShouldReturn:");
    [theTextField resignFirstResponder];
    
    // _backImage.hidden = YES;
    // _searchTableView.hidden = YES;
    return YES;
}

-(void)getLatLng:(NSString *)placeID :(NSString *)strAddress{
    //https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJb1PbNDp3AjoRors37MVpUIQ&key=AIzaSyCDi2dklT-95tEHqYoE7Tklwzn3eJP-MtM
    
    
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=AIzaSyCDi2dklT-95tEHqYoE7Tklwzn3eJP-MtM",placeID];
    
    [APIManager getJSONResponse:strUrl :^(NSDictionary *response_success) {
        NSLog(@"%@",response_success);
        NSLog(@"%@",[response_success valueForKeyPath:@"result.geometry.location"]);
        [self.delegate chooseLocation:[response_success valueForKeyPath:@"result.geometry.location.lat"] :[response_success valueForKeyPath:@"result.geometry.location.lng"] :strAddress];
        [self.navigationController popViewControllerAnimated:YES];
    
    } :^(NSError *response_error) {
        NSLog(@"%@",response_error);
    }];

}
- (IBAction)actionBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)textFieldDidChange:(id)sender {
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geoScode&language=fr&key=AIzaSyCDi2dklT-95tEHqYoE7Tklwzn3eJP-MtM",_textFieldSearch.text];
    NSString *strURiL = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&sensor=true&key=AIzaSyCDi2dklT-95tEHqYoE7Tklwzn3eJP-MtM",_textFieldSearch.text];
    [APIManager getJSONResponse:strURiL :^(NSDictionary *response_success) {
        NSLog(@"%@",response_success);
        _arrTableData = [NSMutableArray new];
        _arrTableData = [response_success valueForKey:@"predictions"];
        if([_arrTableData count] >0){
            [_tableView setHidden:NO];
        }
        else{
            [_tableView setHidden:YES];
        }
        [_tableView reloadData];
    } :^(NSError *response_error) {
        NSLog(@"%@",response_error);
    }];

}

@end
