//
//  NetworkFeedCell.h
//  FitNetApp
//
//  Created by Kunal Gupta on 17/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostFeedModal.h"
#import <UIButton+WebCache.h>
#import <UIImageView+WebCache.h>

@protocol NetworkCellDelegate
-(void)profileButtonPressed:(NSIndexPath *)index;
@end


@interface NetworkFeedCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfilePic;

@property (weak, nonatomic) IBOutlet UIButton *btnProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;

@property (weak, nonatomic) IBOutlet UILabel *labelLocation;
@property (weak, nonatomic) IBOutlet UILabel *labelAttendees;
@property (weak, nonatomic) IBOutlet UILabel *labelSpecificTime;

@property (weak, nonatomic) IBOutlet UILabel *labelPosted;
@property (weak, nonatomic) IBOutlet UILabel *labelCategory;

@property NSIndexPath *indexPath;

- (IBAction)actionBtnProfilePic:(id)sender;

-(void)configureCell:(PostFeedModal *)modal;
@property id <NetworkCellDelegate> delegate;

@end
