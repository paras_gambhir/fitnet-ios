//
//  NetworkFeedCell.m
//  FitNetApp
//
//  Created by Kunal Gupta on 17/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "NetworkFeedCell.h"

@implementation NetworkFeedCell



-(void)configureCell:(PostFeedModal *)modal{
    _btnProfilePic.layer.cornerRadius = _btnProfilePic.frame.size.height/2;
    [_btnProfilePic setClipsToBounds:YES];
    _imageViewProfilePic.layer.cornerRadius = _imageViewProfilePic.frame.size.height/2;
    [_imageViewProfilePic setClipsToBounds:YES];
    
    [_btnProfilePic sd_setImageWithURL:[NSURL URLWithString:modal.strUserProfilePic] forState:UIControlStateNormal];
    [_imageViewProfilePic sd_setImageWithURL:[NSURL URLWithString:modal.strUserProfilePic] placeholderImage:nil];
    _labelName.text = modal.strUserName;
    _labelLocation.text = modal.strLocationAddress;
    _labelCategory.text = [NSString stringWithFormat:@"Category: %@",[modal.arrCategory componentsJoinedByString:@","]];
    _labelPosted.text = [NSString stringWithFormat:@"Posted: %@",modal.strPostedAgo];
    _labelTitle.text = modal.strTitle;
    _labelDescription.text = modal.strDescription;
//    _labelDescription.text = @"Hey there it is just a test . Actually paras sir doubts that the description may work fine if the text length is quite large , so just adding a big test to prove it works fine";
    _labelAttendees.text = [NSString stringWithFormat:@"Attendees: %@",modal.strAttendees];
    _labelTime.text = modal.strDate;
    _labelSpecificTime.text = [NSString stringWithFormat:@"Specific Time: %@",modal.strSessionTime];
    }

- (IBAction)actionBtnProfilePic:(id)sender {
    [self.delegate profileButtonPressed:_indexPath];
}

@end
