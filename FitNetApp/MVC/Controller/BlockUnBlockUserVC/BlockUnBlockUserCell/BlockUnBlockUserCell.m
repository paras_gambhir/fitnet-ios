//
//  BlockUnBlockUserCell.m
//  FitNetApp
//
//  Created by Kunal Gupta on 01/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "BlockUnBlockUserCell.h"

@implementation BlockUnBlockUserCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imagevIewProfilePic.layer.cornerRadius = _imagevIewProfilePic.frame.size.width/2;
    self.imagevIewProfilePic.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(UsersModel *)model{

    _labelName.text = model.strName;
    
    if([model.strIsBlocked integerValue] == 0){
        [_btnBlock setTitle:@"Block" forState:UIControlStateNormal];
    }
    else{
        [_btnBlock setTitle:@"Unblock" forState:UIControlStateNormal];
    }
    [_imagevIewProfilePic sd_setImageWithURL:[NSURL URLWithString:model.strImage]];
}
- (IBAction)actionBtnBlockUnblock:(id)sender {
    [self.delegate blockUnblockUser:_index];
}

@end
