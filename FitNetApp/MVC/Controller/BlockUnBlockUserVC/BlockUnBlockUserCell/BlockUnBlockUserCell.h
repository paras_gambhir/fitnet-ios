//
//  BlockUnBlockUserCell.h
//  FitNetApp
//
//  Created by Kunal Gupta on 01/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UsersModel.h"
#import <UIImageView+WebCache.h>

@protocol BlockUnblockDelegate
-(void)blockUnblockUser:(NSIndexPath *)index;
@end

@interface BlockUnBlockUserCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelName;

@property (weak, nonatomic) IBOutlet UIImageView *imagevIewProfilePic;

@property (weak, nonatomic) IBOutlet UIButton *btnBlock;
- (IBAction)actionBtnBlockUnblock:(id)sender;
@property id <BlockUnblockDelegate> delegate;

@property NSIndexPath *index;
-(void)configureCell:(UsersModel *)model;


@end
