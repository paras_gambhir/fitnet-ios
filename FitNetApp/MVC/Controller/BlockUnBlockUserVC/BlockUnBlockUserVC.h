//
//  BlockUnBlockUserVC.h
//  FitNetApp
//
//  Created by Kunal Gupta on 01/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlockUnBlockUserCell.h"
#import "BaseControllerVC.h"

@interface BlockUnBlockUserVC : BaseControllerVC <BlockUnblockDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UIView *viewTop;


@property (weak, nonatomic) IBOutlet UITableView *tableViewUsers;

@property NSMutableArray *arrTableData;


- (IBAction)actionBtnBack:(id)sender;

@end
