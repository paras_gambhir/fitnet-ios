//
//  BlockUnBlockUserVC.m
//  FitNetApp
//
//  Created by Kunal Gupta on 01/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "BlockUnBlockUserVC.h"

@interface BlockUnBlockUserVC ()

@end

@implementation BlockUnBlockUserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialise];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SELF MADE

-(void)initialise{
    _tableViewUsers.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self hitAPI];
}

-(void)hitAPI{
    [super showLoader];
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"] forKey:@"accessToken"];

    [API getUsersList:[dict mutableCopy] completionHandler:^(NSDictionary *result,NSError *error){
        [super hideLoader];
        NSLog(@"%@",result);
        _arrTableData = [[UsersModel parseDataToArray:[result valueForKeyPath:@"data.users"]] mutableCopy];
        
        if([_arrTableData count] == 0){
            _tableViewUsers.hidden = YES;
        }
        else{
            _tableViewUsers.hidden = NO;
            [_tableViewUsers reloadData];
        }
    }];
}

#pragma mark - TABLE VIEW

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrTableData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BlockUnBlockUserCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BlockUnBlockUserCell"];
    cell.delegate = self;
    cell.index = indexPath;
    [cell configureCell:[_arrTableData objectAtIndex:indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

#pragma mark- CUSTOM DELEGATE

-(void)blockUnblockUser:(NSIndexPath *)index{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"] forKey:@"accessToken"];
    [dict setObject:[[_arrTableData objectAtIndex:index.row] strID] forKey:@"customerId"];
    
    if([[[_arrTableData objectAtIndex:index.row] strIsBlocked]integerValue] == 1){
        [dict setObject:@"False" forKey:@"status"];
        
    }
    else{
        [dict setObject:@"True" forKey:@"status"];
    }
    
    [API blockUnblockUser:dict completionHandler:^(NSDictionary *json, NSError *error) {
        NSLog(@"%@",json);
        [super hideLoader];
        if([[json valueForKey:@"statusCode"] integerValue] ==200){
            [self hitAPI];
        }
    
    }];
}


#pragma mark- ACTION BUTTON

- (IBAction)actionBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
