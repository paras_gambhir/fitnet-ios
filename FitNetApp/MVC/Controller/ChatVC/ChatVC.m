//
//  ChatVC.m
//  FitNetApp
//
//  Created by Kunal Gupta on 07/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "ChatVC.h"

@interface ChatVC ()

@end

@implementation ChatVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self intilaise];
    [super showLoader];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uplift:) name:@"get_keyboard_height" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reset:) name:@"reset" object:nil];
    [self preferredStatusBarStyle];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_timer invalidate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SELF MADE

-(void)intilaise{
    _textViewMessage.delegate = self;
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [self startTimer];
    [self hitAPI];
}


-(void)startTimer{
    _timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(hitAPI) userInfo:nil repeats:YES];
}

-(void)addChatModel:(ChatModel *)model{
    
    if([_arrTableData count] == 0){
        _arrTableData = [NSMutableArray new];
        [_tableViewChat setHidden:NO];
    }
    [_arrTableData addObject:model];
    
    [_tableViewChat reloadData];
    if([_arrTableData count] >1)
        [_tableViewChat scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_arrTableData count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
    
}
-(void)scrollTableToLastMessage:(BOOL)animated{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if(_arrTableData.count > 0){
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: _arrTableData.count-1 inSection: 0];
       
        [self.tableViewChat scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: animated];
        }
   });
}

#pragma mark - HIT API

-(void)hitAPI{
    NSLog(@" my info %@",[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData]);
    NSMutableDictionary *dictAPI = [NSMutableDictionary new];
    [dictAPI setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"] forKey:@"accessToken"];
    [dictAPI setObject:_strPostID forKey:@"postId"];
    
    NSString *strURL = @"http://54.187.56.3:8001/api/customer/listChatOfPost";
    
    [APIManager postData:strURL :dictAPI :^(NSDictionary *response_success) {
        NSLog(@"%@",response_success);
        [super hideLoader];
        if ([[response_success valueForKeyPath:@"data.chats"] count] > 0){
            _arrTableData = [[ChatModel parseDataToArray:[response_success valueForKeyPath:@"data.chats"]] mutableCopy];
            [_tableViewChat reloadData];
        }
    } :^(NSError *response_error) {
        [super hideLoader];
        NSLog(@"%@",response_error);
    }];
}

#pragma mark - KEYBOARD DELEGATES

-(void)uplift:(NSNotification *)noti{
    NSLog(@" height is %@",[noti.userInfo valueForKey:@"height"]);
    _bottomViewMessageConstraint.constant = [[noti.userInfo valueForKey:@"height"] floatValue];
    [UIView animateWithDuration:1 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)reset:(NSNotification *)noti{
    _bottomViewMessageConstraint.constant = 0;
    [UIView animateWithDuration:1 animations:^{
        [self.view layoutIfNeeded];
    }];
}
#pragma mark - TABLE VIEW DELEGATES AND DATA SOURCES

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_arrTableData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MyChatCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyChatCell"];
    OtherChatCell *other = [tableView dequeueReusableCellWithIdentifier:@"OtherChatCell"];
    
    NSString *userID = [[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKeyPath:@"userDetails._id"];
    
    NSString *strShowDate ;
    strShowDate = @"1";

    if (indexPath.row > 0 ){
        
        if ([[[_arrTableData objectAtIndex:indexPath.row -1] strDate] isEqualToString:[[_arrTableData objectAtIndex:indexPath.row] strDate]]){
            strShowDate = @"0";
        }
        else{
            strShowDate = @"1";
        }
    }

    if([userID isEqualToString:[[_arrTableData objectAtIndex:indexPath.row] strUserID]]){
        [cell configureCell:[_arrTableData objectAtIndex:indexPath.row]:strShowDate];
        return cell;
    }
    else{
        [other configureCell:[_arrTableData objectAtIndex:indexPath.row]:strShowDate];
        return other;
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}



#pragma mark - TEXT VIEW DELEGATES

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}
#pragma mark - ACTION BUTTONS

- (IBAction)actionBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionBtnSend:(id)sender {
    
    if ([[_textViewMessage.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] != 0){
        [_textViewMessage resignFirstResponder];
        NSMutableDictionary *dictAPI = [NSMutableDictionary new];
        [dictAPI setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"] forKey:@"accessToken"];
        [dictAPI setObject:_strPostID forKey:@"postId"];
        [dictAPI setObject:[_textViewMessage.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"message"];
        
        _textViewMessage.text = @"";

        NSString *strURL = @"http://54.187.56.3:8001/api/customer/sendChatMessage";

        [APIManager postData:strURL :dictAPI :^(NSDictionary *response_success) {
            NSLog(@"%@",response_success);
            if ([[response_success valueForKey:@"statusCode"] integerValue] == 200){
                ChatModel *cm = [ChatModel initWithAttribute:[response_success valueForKeyPath:@"data.chats"]];
                if ([_arrTableData count] == 0){
                    _arrTableData = [NSMutableArray new];
                }
                [_arrTableData addObject:cm];
                [_tableViewChat reloadData];
                [_tableViewChat setHidden:NO];
                [self scrollTableToLastMessage:NO];
            }
        } :^(NSError *response_error) {
            NSLog(@"%@",response_error);
            _textViewMessage.text = [dictAPI valueForKey:@"message"];
        }];
    }
}

@end
