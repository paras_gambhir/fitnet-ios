//
//  ChatVC.h
//  FitNetApp
//
//  Created by Kunal Gupta on 07/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQKeyboardManager.h"
#import "ChatModel.h"
#import "APIManager.h"
#import "MyChatCell.h"
#import "OtherChatCell.h"
#import <SZTextView.h>
#import "BaseControllerVC.h"

@interface ChatVC : BaseControllerVC<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIView *viewTop;
@property (weak, nonatomic) IBOutlet SZTextView *textViewMessage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewMessageConstraint;

@property (weak, nonatomic) IBOutlet UIView *viewBottom;

@property (weak, nonatomic) IBOutlet UITableView *tableViewChat;
- (IBAction)actionBtnBack:(id)sender;

- (IBAction)actionBtnSend:(id)sender;

@property NSString *strPostID;
@property NSTimer *timer;

@property NSMutableArray *arrTableData;
@property UIRefreshControl *refreshControl;

@property NSMutableDictionary *dict;
@property NSString *userID;

@end
