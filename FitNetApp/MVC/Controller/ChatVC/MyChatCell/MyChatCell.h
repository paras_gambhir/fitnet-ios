//
//  MyChatCell.h
//  FitNetApp
//
//  Created by Kunal Gupta on 07/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatModel.h"
#import <UIImageView+WebCache.h>


@interface MyChatCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *viewDate;
@property (weak, nonatomic) IBOutlet UIView *viewMessageContent;

@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfilePic;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightDateConstraint;

@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;

-(void)configureCell:(ChatModel *)model :(NSString *)strShowDate;

@end
