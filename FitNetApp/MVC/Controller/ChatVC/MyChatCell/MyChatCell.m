
//
//  MyChatCell.m
//  FitNetApp
//
//  Created by Kunal Gupta on 07/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "MyChatCell.h"

@implementation MyChatCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _imageViewProfilePic.layer.cornerRadius = _imageViewProfilePic.frame.size.height/2;
    [_imageViewProfilePic setClipsToBounds:YES];
    [_imageViewProfilePic setBackgroundColor:[UIColor grayColor]];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self.viewMessageContent.layer setCornerRadius:8.0];
    [self.viewDate.layer setCornerRadius:8.0];
    [self setClipsToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(ChatModel *)model :(NSString *)strShowDate{
    _labelMessage.text = model.strMessage;
//    _labelName.text = model.strName;
    _labelDate.text = model.strDate;
    _labelTime.text = model.strTimeAgo;

    [_imageViewProfilePic sd_setImageWithURL:[NSURL URLWithString:model.strImage]];
    
    if ([strShowDate isEqualToString:@"1"]){
        _heightDateConstraint.constant = 32;
    }
    else{
        _heightDateConstraint.constant = 0;
    }
}

@end
