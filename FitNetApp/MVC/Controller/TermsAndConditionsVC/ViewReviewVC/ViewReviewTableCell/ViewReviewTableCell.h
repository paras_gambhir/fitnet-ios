//
//  ViewReviewTableCell.h
//  FitNetApp
//
//  Created by Kunal Gupta on 25/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReviewModel.h"
#import <UIImageView+WebCache.h>
#import "TPFloatRatingView.h"

@interface ViewReviewTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UILabel *labelReview;

@property (weak, nonatomic) IBOutlet TPFloatRatingView *viewRating;
@property (weak, nonatomic) IBOutlet UILabel *labelName;

-(void)configureCell:(ReviewModel *)model;

@end
