//
//  ViewReviewTableCell.m
//  FitNetApp
//
//  Created by Kunal Gupta on 25/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "ViewReviewTableCell.h"

@implementation ViewReviewTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(ReviewModel *)model{
    _labelTime.text = model.strTime;
    _labelName.text = model.strName;
    _labelReview.text = model.strDesciption;
    [_imageViewProfilePic sd_setImageWithURL:[NSURL URLWithString:model.strimage] placeholderImage:[UIImage imageNamed:@"profile_icon-1"]];
    
    _imageViewProfilePic.layer.cornerRadius = _imageViewProfilePic.frame.size.width/2;
    [_imageViewProfilePic setClipsToBounds:YES];
    
    
    
//    self.viewRating.delegate = self;
    self.viewRating.emptySelectedImage = [UIImage imageNamed:@"star_inactive"];
    self.viewRating.fullSelectedImage = [UIImage imageNamed:@"star_active"];
    self.viewRating.contentMode = UIViewContentModeScaleAspectFill;
    self.viewRating.maxRating = 5;
    self.viewRating.minRating = 0;
    self.viewRating.rating = [model.strRating floatValue];
    self.viewRating.editable = NO;
    self.viewRating.halfRatings = NO;
    self.viewRating.floatRatings = NO;

 
}
@end
