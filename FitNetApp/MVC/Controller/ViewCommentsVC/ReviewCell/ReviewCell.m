//
//  ReviewCell.m
//  FitNetApp
//
//  Created by Kunal Gupta on 08/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "ReviewCell.h"

@implementation ReviewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _imageViewProfilePic.layer.cornerRadius = _imageViewProfilePic.frame.size.width/2;
    [_imageViewProfilePic setClipsToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}



-(void)configureCell:(ReviewModel *)model{
    _labelName.text = model.strName;
    _labelTime.text = model.strTime;
    _labelReview.text = model.strDesciption;
    [_imageViewProfilePic sd_setImageWithURL:[NSURL URLWithString:model.strimage]];

}

@end
