//
//  ReviewCell.h
//  FitNetApp
//
//  Created by Kunal Gupta on 08/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReviewModel.h"
#import <UIImageView+WebCache.h>


@interface ReviewCell : UITableViewCell

-(void)configureCell:(ReviewModel *)model;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *labelReview;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;

@property (weak, nonatomic) IBOutlet UILabel *labelName;
@end
