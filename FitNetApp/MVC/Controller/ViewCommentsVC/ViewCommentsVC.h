//
//  ViewCommentsVC.h
//  FitNetApp
//
//  Created by Kunal Gupta on 07/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SZTextView.h>
#import "CommentCell.h"
#import "ReviewCell.h"
#import "APIManager.h"
#import "BaseControllerVC.h"
#import "IQKeyboardManager.h"


@interface ViewCommentsVC : BaseControllerVC<UITextViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableViewComments;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewMessageConstraint;
@property (weak, nonatomic) IBOutlet SZTextView *textViewComment;


@property NSMutableArray *arrTableData;
@property ReviewModel *reviewModel;
@property NSString *strReviewID;
- (IBAction)actionBtnComment:(id)sender;
- (IBAction)actionBtnback:(id)sender;

@end
