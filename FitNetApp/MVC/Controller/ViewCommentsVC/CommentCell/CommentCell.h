//
//  CommentCell.h
//  FitNetApp
//
//  Created by Kunal Gupta on 08/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentModel.h"
#import <UIImageView+WebCache.h>

@interface CommentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *labelComment;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;

@property (weak, nonatomic) IBOutlet UILabel *labelName;

-(void)configureCell:(CommentModel *)model;

@end

