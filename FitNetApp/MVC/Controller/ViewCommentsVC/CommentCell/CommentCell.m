//
//  CommentCell.m
//  FitNetApp
//
//  Created by Kunal Gupta on 08/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "CommentCell.h"

@implementation CommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _imageViewProfilePic.layer.cornerRadius = _imageViewProfilePic.frame.size.width/2;
    [_imageViewProfilePic setClipsToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell:(CommentModel *)model{
    _labelName.text = model.strName;
    _labelTime.text = model.strTimeAgo;
    _labelComment.text = model.strComment;
    [_imageViewProfilePic sd_setImageWithURL:[NSURL URLWithString:model.strImage]];

}

@end
