//
//  ViewCommentsVC.m
//  FitNetApp
//
//  Created by Kunal Gupta on 07/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "ViewCommentsVC.h"

@interface ViewCommentsVC ()

@end

@implementation ViewCommentsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [_textViewComment setDelegate:self];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    _tableViewComments.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_tableViewComments reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uplift:) name:@"get_keyboard_height" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reset:) name:@"reset" object:nil];
    [self preferredStatusBarStyle];
}
-(void)scrollTableToLastMessage:(BOOL)animated{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: _arrTableData.count-1 inSection: 0];
        [self.tableViewComments scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: animated];
    });
}
#pragma mark - KEYBOARD DELEGATES

-(void)uplift:(NSNotification *)noti{
    _bottomViewMessageConstraint.constant = [[noti.userInfo valueForKey:@"height"] floatValue];
    [UIView animateWithDuration:1 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)reset:(NSNotification *)noti{
    _bottomViewMessageConstraint.constant = 0;
    [UIView animateWithDuration:1 animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - TABLEVIEW DELEGATE AND DATASOURCE

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrTableData.count+1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ReviewCell *review = [tableView dequeueReusableCellWithIdentifier:@"ReviewCell"];
    CommentCell *comment = [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];
    
    if (indexPath.row == 0){
        [review configureCell:_reviewModel];
        return review;
    }
    else{
        [comment configureCell:[_arrTableData objectAtIndex:indexPath.row-1]];
        return  comment;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 77;
}

#pragma mark - TEXT VIEW DELEGATES

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text{
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}

#pragma mark - ACTION BUTTONS


- (IBAction)actionBtnComment:(id)sender {

    if ([[_textViewComment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] != 0){
        [_textViewComment resignFirstResponder];

        [super showLoader];
        NSMutableDictionary *dictAPI = [NSMutableDictionary new];
        [dictAPI setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"] forKey:@"accessToken"];
        [dictAPI setObject:_reviewModel.strID forKey:@"reviewId"];
        [dictAPI setObject:[_textViewComment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"comments"];
        
        _textViewComment.text = @"";
        
        NSString *strURL = @"http://54.187.56.3:8001/api/customer/giveCommentsOnReview";
        
        [APIManager postData:strURL :dictAPI :^(NSDictionary *response_success) {
            NSLog(@"%@",response_success);
            [super hideLoader];
            if ([[response_success valueForKey:@"statusCode"] integerValue] == 200){
                

                _arrTableData = [[CommentModel parseDataToArray:[response_success valueForKeyPath:@"data.comments"]] mutableCopy];
                [_tableViewComments reloadData];
                [self scrollTableToLastMessage:NO];
            }
        } :^(NSError *response_error) {
            NSLog(@"%@",response_error);
            [super hideLoader];
            _textViewComment.text = [dictAPI valueForKey:@"comments"];
        }];
    }

}

- (IBAction)actionBtnback:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
