//
//  ReviewModel.h
//  FitNetApp
//
//  Created by Kunal Gupta on 25/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReviewModel : NSObject

+ (NSArray*)parseDataToArray:(NSArray*)array;

@property NSString *strID;
@property NSString *strName;
@property NSString *strimage;
@property NSString *strRating;
@property NSString *strDesciption;
@property NSString *strTime;
@property NSArray *arrComments;

@end
