//
//  ReviewModel.m
//  FitNetApp
//
//  Created by Kunal Gupta on 25/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "ReviewModel.h"

@implementation ReviewModel



+ (NSArray*)parseDataToArray:(NSArray*)array{
    
    NSMutableArray * arr = [NSMutableArray new];
    
    for (NSDictionary * dict  in array){
        
        ReviewModel * fm = [[ReviewModel alloc]initWithAttribute:dict];
        
        [arr addObject:fm];
    }
    
    return arr;
}

-(ReviewModel *)initWithAttribute:(NSDictionary*)attributes{
    
    ReviewModel * fm = [[ReviewModel alloc]init];
    NSLog(@"%@",attributes);
    fm.strID = [attributes valueForKeyPath:@"_id"];
    fm.strTime = [attributes valueForKeyPath:@"timeDifference"];
    fm.strDesciption = [attributes valueForKeyPath:@"review"];
    fm.strName = [attributes valueForKeyPath:@"fromCustomer.name"];
    fm.strimage = [attributes valueForKeyPath:@"fromCustomer.profilePicURL.original"];
    fm.strRating = [attributes valueForKeyPath:@"rating"];
    fm.arrComments = [attributes valueForKeyPath:@"comments"];
    
    return fm;
    
}
@end
