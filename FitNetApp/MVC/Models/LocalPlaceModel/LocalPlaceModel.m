//
//  LocalPlaceModel.m
//  iCollege
//
//  Created by cbl20 on 5/27/16.
//  Copyright © 2016 Kunal Gupta. All rights reserved.
//

#import "LocalPlaceModel.h"

@implementation LocalPlaceModel


+ (NSArray*)parseDataToArray:(NSArray*)array{
    
    NSMutableArray * arr = [NSMutableArray new];
    
    for (NSDictionary * dict  in array){
        
        LocalPlaceModel * fm = [[LocalPlaceModel alloc]initWithAttribute:dict];
        
        [arr addObject:fm];
    }
    
    return arr;
}

-(LocalPlaceModel *)initWithAttribute:(NSDictionary*)attributes{
    
    LocalPlaceModel * fm = [[LocalPlaceModel alloc]init];
    
//    fm.strAddress = [attributes objectForKey:@"address"];
//    fm.strDistance = [attributes objectForKey:@"distance"];
//    fm.strID = [attributes objectForKey:@"id"];
//    fm.strLat = [attributes objectForKey:@"lat"];
//    fm.strLng = [attributes objectForKey:@"lng"];
//    fm.strImage = [attributes objectForKey:@"pic"];
//    fm.strName = [attributes objectForKey:@"name"];
//    fm.strPhoneNumber = [attributes objectForKey:@"phn_no"];
//    fm.strPic = [attributes objectForKey:@"pic"];
//    fm.strRating = [attributes objectForKey:@"rating"];
//    fm.strSubCategoryID = [attributes objectForKey:@"sub_category_id"];
//    fm.strPlaceID = [attributes objectForKey:@"place_id"];
//    fm.strToday = [attributes objectForKey:@"today"];
//    fm.arrTime = [attributes objectForKey:@"time"];

    return fm;
    
}

@end
