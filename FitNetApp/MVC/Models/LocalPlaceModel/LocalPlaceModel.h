//
//  LocalPlaceModel.h
//  iCollege
//
//  Created by cbl20 on 5/27/16.
//  Copyright © 2016 Kunal Gupta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalPlaceModel : NSObject

@property NSString *strAddress;
@property NSString *strDistance;
@property NSString *strID;
@property NSString *strLat;
@property NSString *strLng;
@property NSString *strImage;
@property NSString *strName;
@property NSString *strPhoneNumber;
@property NSString *strSessions;
@property NSString *strRating;


+ (NSArray*)parseDataToArray:(NSArray*)array;


@end
