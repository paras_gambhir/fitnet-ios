//
//  CommentModel.m
//  FitNetApp
//
//  Created by Kunal Gupta on 07/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "CommentModel.h"

@implementation CommentModel

+ (NSArray*)parseDataToArray:(NSArray*)array{
    
    NSMutableArray * arr = [NSMutableArray new];
    
    for (NSDictionary * dict  in array){
        
        CommentModel * fm = [CommentModel initWithAttribute:dict];
        
        [arr addObject:fm];
    }
    
    return arr;
}

+(CommentModel *)initWithAttribute:(NSDictionary*)attributes{
    
    CommentModel * fm = [[CommentModel alloc]init];
    
    fm.strName = [attributes valueForKeyPath:@"customer.name"];
    fm.strImage = [attributes valueForKeyPath:@"customer.profilePicURL.original"];
    fm.strComment = [attributes valueForKeyPath:@"comment"];
    fm.strTimeAgo = [attributes valueForKeyPath:@"timeDifference"];
//    fm.strDate = [[CommentModel alloc] getProperDate:[attributes valueForKeyPath:@"creationDate"]];
//    fm.strUserID = [attributes valueForKeyPath:@"customer._id"];
    
    return fm;
    
}
@end
