//
//  ChatModel.h
//  FitNetApp
//
//  Created by Kunal Gupta on 07/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatModel : NSObject

@property NSString *strID;
@property NSString *strMessage;
@property NSString *strImage;
@property NSString *strName;
@property NSString *strTimeAgo;
@property NSString *strDate;
@property NSString *strUserID;

+ (NSArray*)parseDataToArray:(NSArray*)array;
+(ChatModel *)initWithAttribute:(NSDictionary*)attributes;

@end
