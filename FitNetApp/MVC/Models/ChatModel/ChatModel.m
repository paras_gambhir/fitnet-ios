//
//  ChatModel.m
//  FitNetApp
//
//  Created by Kunal Gupta on 07/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "ChatModel.h"

@implementation ChatModel

+ (NSArray*)parseDataToArray:(NSArray*)array{
    
    NSMutableArray * arr = [NSMutableArray new];
    
    for (NSDictionary * dict  in array){
        
        ChatModel * fm = [ChatModel initWithAttribute:dict];
        
        [arr addObject:fm];
    }
    
    return arr;
}

+(ChatModel *)initWithAttribute:(NSDictionary*)attributes{
    
    ChatModel * fm = [[ChatModel alloc]init];
    
    fm.strName = [attributes valueForKeyPath:@"customer.name"];
    fm.strImage = [attributes valueForKeyPath:@"customer.profilePicURL.original"];
    fm.strMessage = [attributes valueForKeyPath:@"comment"];
    fm.strTimeAgo = [attributes valueForKeyPath:@"timeDifference"];
    fm.strDate = [[ChatModel alloc] getProperDate:[attributes valueForKeyPath:@"creationDate"]];
    fm.strUserID = [attributes valueForKeyPath:@"customer._id"];
    
    return fm;
    
}

-(NSString *)getProperDate:(NSString *)strUTC{
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate* utcTime = [dateFormatter dateFromString:strUTC];
    NSLog(@"UTC time: %@", utcTime);
    
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    NSString* localTime = [dateFormatter stringFromDate:utcTime];
    return  localTime;
}

@end
