//
//  MapModel.h
//  FitNetApp
//
//  Created by Kunal Gupta on 26/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapModel : NSObject

@property NSString *strName;
@property NSString *strLat;
@property NSString *strLong;
@property NSString *strID;

+ (NSArray*)parseDataToArray:(NSArray*)array;


@end
