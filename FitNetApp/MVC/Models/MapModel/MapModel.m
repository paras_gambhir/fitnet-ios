//
//  MapModel.m
//  FitNetApp
//
//  Created by Kunal Gupta on 26/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "MapModel.h"

@implementation MapModel




+ (NSArray*)parseDataToArray:(NSArray*)array{
    
    NSMutableArray * arr = [NSMutableArray new];
    
    for (NSDictionary * dict  in array){
        
        MapModel * fm = [[MapModel alloc]initWithAttribute:dict];
        
        [arr addObject:fm];
    }
    
    return arr;
}

-(MapModel *)initWithAttribute:(NSDictionary*)attributes{
    
    MapModel * fm = [[MapModel alloc]init];
    NSLog(@"%@",attributes);
    fm.strID = [attributes valueForKeyPath:@"_id"];
    fm.strName = [attributes valueForKeyPath:@"name"];
    fm.strLat = [attributes valueForKeyPath:@"latitude"];
    fm.strLong = [attributes valueForKeyPath:@"longitude"];
    
    return fm;
    
}

@end
