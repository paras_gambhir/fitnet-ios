//
//  CurrentActivityModel.h
//  FitNetApp
//
//  Created by Kunal Gupta on 27/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrentActivityModel : NSObject
@property NSString *strID;
@property NSString *strName;
@property NSString *strImage;

+ (NSArray*)parseDataToArray:(NSArray*)array;

@end
