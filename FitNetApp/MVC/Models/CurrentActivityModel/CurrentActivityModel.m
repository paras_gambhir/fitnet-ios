//
//  CurrentActivityModel.m
//  FitNetApp
//
//  Created by Kunal Gupta on 27/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "CurrentActivityModel.h"




/*
 {
 "__v" = 0;
 "_id" = 57bdad9d666bb39615b56ec2;
 acceptUser =                 (
 );
 categories =                 (
 {
 "_id" = 57bb3ab9755e67cd56dd96b8;
 name = GYM;
 }
 );
 creationDate = "2016-08-24T14:22:21.901Z";
 customer =                 {
 "_id" = 57bd08a5666bb39615b56ec1;
 name = Kunal;
 profilePicURL =                     {
 original = "http://www.indianfunpic.com/wp-content/uploads/2016/06/Funny-Kids-5.jpg";
 thumbnail = "http://www.indianfunpic.com/wp-content/uploads/2016/06/Funny-Kids-5.jpg";
 };
 };
 description = "Hiii this is demo";
 isDeleted = 0;
 locationAddress = "Chandigarh, India";
 locationLongLat =                 (
 "30.7333148",
 "76.7794179"
 );
 timings =                 {
 Timing = "Lunchtime (12-2pm)";
 "_id" = 57bb3ab9755e67cd56dd96c9;
 };
 }
 */
@implementation CurrentActivityModel



+ (NSArray*)parseDataToArray:(NSArray*)array{
    
    NSMutableArray * arr = [NSMutableArray new];
    
    for (NSDictionary * dict  in array){
        
        CurrentActivityModel * fm = [[CurrentActivityModel alloc]initWithAttribute:dict];
        
        [arr addObject:fm];
    }
    
    return arr;
}

-(CurrentActivityModel *)initWithAttribute:(NSDictionary*)attributes{
    
    CurrentActivityModel * fm = [[CurrentActivityModel alloc]init];
    NSLog(@"%@",attributes);
    fm.strID = [attributes valueForKeyPath:@"_id"];
    fm.strName = [attributes valueForKeyPath:@"customer.name"];
    fm.strImage = [attributes valueForKeyPath:@"customer.profilePicURL.original"];
    
    return fm;
    
}

@end
