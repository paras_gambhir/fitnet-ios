//
//  UsersModel.h
//  FitNetApp
//
//  Created by Kunal Gupta on 01/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UsersModel : NSObject


@property NSString *strID;
@property NSString *strName;
@property NSString *strImage;
@property NSString *strIsBlocked;

+ (NSArray*)parseDataToArray:(NSArray*)array;

@end
