//
//  UsersModel.m
//  FitNetApp
//
//  Created by Kunal Gupta on 01/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "UsersModel.h"

@implementation UsersModel

+ (NSArray*)parseDataToArray:(NSArray*)array{
    
    NSMutableArray * arr = [NSMutableArray new];
    
    for (NSDictionary * dict  in array){
        
        UsersModel * fm = [[UsersModel alloc]initWithAttribute:dict];
        
        [arr addObject:fm];
    }
    
    return arr;
}

-(UsersModel *)initWithAttribute:(NSDictionary*)attributes{
    
    UsersModel * fm = [[UsersModel alloc]init];

    fm.strID = [attributes valueForKeyPath:@"_id"];
    fm.strName = [attributes valueForKeyPath:@"name"];
    fm.strImage = [attributes valueForKeyPath:@"profilePicURL.original"];
    fm.strIsBlocked = [attributes valueForKeyPath:@"isBlocked"];
    
    return fm;
    
}
@end
