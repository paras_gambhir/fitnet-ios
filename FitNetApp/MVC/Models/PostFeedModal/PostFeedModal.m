//
//  PostFeedModal.m
//  FitNetApp
//
//  Created by Kunal Gupta on 17/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "PostFeedModal.h"

@implementation PostFeedModal

+ (NSArray*)parseDataToArray:(NSArray*)array{
    
    NSMutableArray * arr = [NSMutableArray new];
    for (NSDictionary * dict  in array){
        PostFeedModal * fm = [[PostFeedModal alloc]initWithAttribute:dict];
        [arr addObject:fm];
    }
    return arr;
}

-(PostFeedModal *)initWithAttribute:(NSDictionary*)attributes{
    
    PostFeedModal * fm = [[PostFeedModal alloc]init];
    
    fm.strID = [attributes valueForKeyPath:@"_id"];
    fm.strTitle = [attributes valueForKeyPath:@"title"];
    fm.strSessionTime = [attributes valueForKeyPath:@"postDate"];
    fm.strDescription = [attributes valueForKeyPath:@"description"];
    fm.strUserName = [attributes valueForKeyPath:@"customer.name"];
    fm.strUserProfilePic = [attributes valueForKeyPath:@"customer.profilePicURL.original"];
    fm.strLocationAddress = [attributes valueForKeyPath:@"locationAddress"];
    fm.strUserID = [attributes valueForKeyPath:@"customer._id"];
    fm.strDate = [attributes valueForKeyPath:@"postDate"];
    fm.arrCategory = [attributes valueForKeyPath:@"categories.name"];
    fm.strPostedAgo = [attributes valueForKey:@"creationDate"];
    fm.strSessionTime = [attributes valueForKey:@"sessionTime"];
    fm.strAttendees = [attributes valueForKey:@"accpectUsers"];
    
    return fm;

}



@end
