//
//  PostFeedModal.h
//  FitNetApp
//
//  Created by Kunal Gupta on 17/09/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostFeedModal : NSObject

@property NSString *strID;
@property NSString *strTitle;
@property NSString *strSessionTime;
@property NSString *strDescription;
@property NSString *strUserName;
@property NSString *strUserProfilePic;
@property NSString *strLocationAddress;
@property NSString *strUserID;
@property NSString *strDate;
@property NSArray *arrCategory;
@property NSString *strAttendees;
@property NSString *strPostedAgo;

+ (NSArray*)parseDataToArray:(NSArray*)array;

@end
