//
//  ProfileModel.m
//  FitNetApp
//
//  Created by Kunal Gupta on 24/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "ProfileModel.h"

@implementation ProfileModel


//+ (NSArray*)parseDataToArray:(NSArray*)array{
//    
//    NSMutableArray * arr = [NSMutableArray new];
//    
//    for (NSDictionary * dict  in array){
//        
//        ProfileModel * fm = [[ProfileModel alloc]initWithAttribute:dict];
//        
//        [arr addObject:fm];
//    }
//    
//    return arr;
//}

+(ProfileModel *)initWithAttribute:(NSDictionary*)attributes{
    
    ProfileModel * fm = [[ProfileModel alloc]init];
    
    fm.strRating = [attributes objectForKey:@"Rating"];
    fm.strID = [attributes objectForKey:@"_id"];
    fm.strAboutMe = [attributes objectForKey:@"aboutMe"];
    fm.strGoodFor = [attributes objectForKey:@"goodFor"];
    fm.strLat = [attributes objectForKey:@"latitude"];
    fm.strLevel = [attributes objectForKey:@"level"];
    fm.strLng = [attributes objectForKey:@"longitude"];
    fm.strName = [attributes objectForKey:@"name"];
    fm.strProfilePicURL = [attributes valueForKeyPath:@"profilePicURL.original"];
    fm.strSession = [attributes objectForKey:@"sessions"];
    fm.strReviewCount = [attributes objectForKey:@"totalReviews"];
    fm.arrFitness = [attributes valueForKeyPath:@"categories"];
    fm.strTime = [attributes valueForKeyPath:@"timings"];

    
    return fm;
    
}
@end
