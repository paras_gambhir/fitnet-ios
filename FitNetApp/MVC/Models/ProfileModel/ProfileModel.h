//
//  ProfileModel.h
//  FitNetApp
//
//  Created by Kunal Gupta on 24/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProfileModel : NSObject


@property NSString *strRating;
@property NSString *strReviewCount;
@property NSArray *arrFitness;
@property NSString *strTime;
@property NSString *strID;
@property NSString *strAboutMe;
@property NSString *strGoodFor;
@property NSString *strLat;
@property NSString *strLevel;
@property NSString *strLng;
@property NSString *strName;
@property NSString *strProfilePicURL;
@property NSString *strSession;


+(ProfileModel *)initWithAttribute:(NSDictionary*)attributes;



@end
