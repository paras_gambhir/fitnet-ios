//
//  CurrentActitivyVC.h
//  FitNetApp
//
//  Created by anish on 18/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPFloatRatingView.h"
#import "BaseControllerVC.h"
#import "CurrentActivityModel.h"
#import "ChatVC.h"
#import "SettingVC.h"

#import <UIImageView+WebCache.h>

@interface CurrentActitivyVC : BaseControllerVC<UITableViewDataSource,UITableViewDelegate,TPFloatRatingViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *currentActivityTable;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UILabel *labelAlert;

@property (weak, nonatomic) IBOutlet UIImageView *backBg;
@property (weak, nonatomic) IBOutlet UIView *endPopup;

@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

@property (strong, nonatomic) IBOutlet UILabel *ratingLabel;
@property (strong, nonatomic) IBOutlet UILabel *liveLabel;
@property (strong, nonatomic) IBOutlet TPFloatRatingView *ratingView;
@property NSMutableArray *arrTableData;

@end
