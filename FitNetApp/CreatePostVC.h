//
//  CreatePostVC.h
//  FitNetApp
//
//  Created by anish on 06/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatePickerView.h"
#import "BaseControllerVC.h"

@interface CreatePostVC : BaseControllerVC <UITableViewDataSource,UITableViewDelegate,SendDateBackDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tfTitle;
@property (weak, nonatomic) IBOutlet UIButton *CategoryBtn;
@property (weak, nonatomic) IBOutlet UIButton *loctionBtn;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImagePopup;
@property (weak, nonatomic) IBOutlet UITableView *homeTableview;
@property (weak, nonatomic) IBOutlet UIButton *crossButton;
@property (weak, nonatomic) IBOutlet UILabel *fitnnessLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
- (IBAction)actionBtnSessionTime:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSessionTime;
@property (weak, nonatomic) IBOutlet UITableView *timeTableView;
@property NSString *strCustomCat;
@property NSString *postDateAndTime;

@end
