//
//  BaseControllerVC.h
//  Thredz
//
//  Created by Kunal Gupta on 28/01/16.
//  Copyright © 2016 Kunal Gupta. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingView.h"
#import "CurrentActivityModel.h"
#import "CustomCategory.h"


//#import "Colors.h"
//#import "Loader.h"
//
//#import "ReportNoise.h"
//#import "CustomAlert.h"
//#import "BaseControllerVC.h"
@interface BaseControllerVC : UIViewController<CLLocationManagerDelegate>;

-(BOOL)NSStringIsValidEmail:(NSString *)checkString;
-(BOOL)isPhoneNumberValid:(NSString *)phoneNumber;
-(BOOL)isUsernameValid:(NSString *)string;
-(BOOL)isNameValid:(NSString *)string;
@property RatingView *rating;
@property CustomCategory *customCategory;

@property CGFloat keyBoardheight;
-(void)showRatingScreen:(CurrentActivityModel *)model;

@property CLLocationManager *locationManager;
-(void)getLocation;
-(NSString *)getZone;
//-(BOOL)internetWorking;
-(void)hideToolBar;
-(void)showAlert:(NSString *)title :(NSString *)message;

-(void)showLoader;
-(void)hideLoader;
-(void)showCustomcategory;


@end
