//
//  BaseControllerVC.m
//  Thredz
//
//  Created by Kunal Gupta on 28/01/16.
//  Copyright © 2016 Kunal Gupta. All rights reserved.
//

#import "BaseControllerVC.h"

@interface BaseControllerVC ()

@end

@implementation BaseControllerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getLocation];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
//
//    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChangeFrames:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tokenExpired)
                                                 name:@"tokenExpired"
                                               object:nil];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//-(void)showLoader{
//    _loader = [[Loader alloc]initWithFrame:CGRectMake(0, 0, kframe.width, kframe.height) andColor:[UIColor redColor]];
//    [kWindow addSubview:_loader];
//    [_loader startLoader];
//}

//-(void)hideLoader{
//    [_loader stopLoader];
//    [_loader removeFromSuperview];
//}
-(void)showAlert:(NSString *)title :(NSString *)message{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:message message:title delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

-(BOOL)NSStringIsValidEmail:(NSString *)checkString{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    //    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    //    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    return [emailTest evaluateWithObject:checkString];
}
-(BOOL)isPhoneNumberValid:(NSString *)phoneNumber{

    
    NSString *mobileNumberPattern = @"[789][0-9]{9}";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    
    BOOL matched = [mobileNumberPred evaluateWithObject:phoneNumber];
    return matched;
}

-(BOOL)isUsernameValid:(NSString *)string{
    
    NSString *specialCharacterString = @" !~`@#$%^&*-+();:={}[],.<>?\\/\"\'";
    NSCharacterSet *specialCharacterSet = [NSCharacterSet
                                           characterSetWithCharactersInString:specialCharacterString];
    
    if ([string.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) {
        return NO;
    }
    return YES;
}
-(BOOL)isNameValid:(NSString *)string{
    
    NSString *specialCharacterString = @"!~`@#$%^&*-+();:={}[],.<>?\\/\"\'1234567890";
    NSCharacterSet *specialCharacterSet = [NSCharacterSet
                                           characterSetWithCharactersInString:specialCharacterString];
    
    if ([string.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) {
        return NO;
    }
    return YES;
}
-(void)setCornerRadiusAndClip:(UIView *) view :(CGFloat) value{
    view.layer.cornerRadius = value;
    [view setClipsToBounds:YES];
}
-(void)setTopViewShadow:(UIView *)view :(UIColor *)color{
    
    self.view.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.view.layer.shadowOffset = CGSizeMake(1.0f, 1.5f);
    self.view.layer.shadowRadius = 3.0f;
    self.view.layer.shadowOpacity = 0.25f;
}

-(void)setButtonShadow:(UIButton *)btn :(UIColor *)color{
    [btn.layer setCornerRadius:4.0f];
    
    // border
    [btn.layer setBorderColor:[color CGColor]];
    [btn.layer setBorderWidth:0.2f];
    
    // drop shadow
    [btn.layer setShadowColor:[color CGColor]];
    [btn.layer setShadowOpacity:0.6];
    [btn.layer setShadowRadius:2.0];
    [btn.layer setShadowOffset:CGSizeMake(0.0, 1.5)];
}

-(void)getLocation{
    
    self.locationManager = [[CLLocationManager alloc] init];
    if([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]){
        [self.locationManager requestWhenInUseAuthorization];
    }
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
}

-(NSString *)getZone{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    return tzName;
}
//-(void)showAlert :(NSString *) title :(NSString *)message{
//    DQAlertView * alertView = [[DQAlertView alloc] initWithTitle:title message:message cancelButtonTitle:nil otherButtonTitle:@"OK"];
//    [alertView show];
//    
//}

//-(void)showCustomAlert :(NSString *)title :(NSString *)message :(NSString *)cancel :(NSString *)other {
//    DQAlertView * alertView = [[DQAlertView alloc] initWithTitle:title message:message cancelButtonTitle:cancel otherButtonTitle:other];
//    alertView.delegate = self;
//    [alertView show];
//}
//-(void)showReportNoise{
//    _report = [[ReportNoise alloc]initWithFrame:CGRectMake(0, 0, kframe.width, kframe.height)];
//    [kWindow addSubview:_report];
//}

//-(void)hideToolBar{
  //  [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
//}

//#pragma mark - ALERT VIEW CONTROLLER
//
//- (void)cancelButtonClickedOnAlertView:(DQAlertView *)alertView{
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"cancel" object:nil];
//}
//
//- (void)otherButtonClickedOnAlertView:(DQAlertView *)alertView{
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"other" object:nil];
//}

//-(BOOL)internetWorking {
//    
//    Reachability *reachTest = [Reachability reachabilityWithHostName:@"www.google.com"];
//    NetworkStatus internetStatus = [reachTest  currentReachabilityStatus];
//    
//    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN)){
//        [self showAlert:nil :@"Please check your internet connection"];
//        return NO;
//    }
//    else{
//        return YES;
//    }
//}

#pragma mark - XIB'S

//-(void)showCustomAlert:(NSString *)title :(NSString *)message :(NSString *)firstTitle :(NSString *)secondTitle :(NSString *)thirdtitle{
//    
//    CustomAlert *alert = [[CustomAlert alloc]initWithFrame:CGRectMake(0, 0, kframe.width, kframe.height) :title :message :firstTitle :secondTitle :thirdtitle];
//    [kWindow addSubview:alert];
//}
//-(void)showNotificationFilters:(NSMutableDictionary *)dict{
//    _userNoti = [[UserNotifications alloc]initWithFrame:CGRectMake(0, 0, kframe.width, kframe.height) :dict];
//    [kWindow addSubview:_userNoti];
//}


#pragma  mark - Touches Began

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)showLoader{
    
    [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeGradient];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01]];
}
-(void)hideLoader{
    [SVProgressHUD dismiss];
}
#pragma mark - keyboard Notifications

-(void)keyboardDidShow:(NSNotification *)notification{
    
}

-(void)keyboardDidHide:(NSNotification *)notification{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reset" object:nil];
}

-(void)keyboardWillChangeFrames:(NSNotification *)notification{
    
    NSDictionary* notificationInfo = [notification userInfo];
    CGRect keyboardFrame = [[notificationInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:[[notificationInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue]
                          delay:0
                        options:[[notificationInfo valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue]
                     animations:^{
                         _keyBoardheight = keyboardFrame.size.height;
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"get_keyboard_height" object:nil userInfo:@{@"height":[NSString stringWithFormat:@"%f",_keyBoardheight]}];
                     } completion:nil];
}

#pragma mark - CLLocationManagerDelegate

//- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
//    NSLog(@"didFailWithError: %@", [error localizedDescription]);
//   
//    if([CLLocationManager locationServicesEnabled]){
//        if([CLLocationManager authorizationStatus] == 2){
//            
//            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:LATITUDE];
//            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:LONGITUDE];
//        }
//    }
//}

//-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
//    NSLog(@"STATUS %d",status);
//    if (status == kCLAuthorizationStatusDenied) {
//        [self showAlert:nil :@"It seems you have not enabled location. Go to settings > iCollege > enable location"];
//    }
//}
//
//- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
//    //  NSLog(@"didUpdateToLocation: %@", [locations lastObject]);
//    CLLocation *currentLocation = [locations lastObject];
//    
//    if (currentLocation != nil){
//        NSString * currentLat = [NSString stringWithFormat:@"%.6f", currentLocation.coordinate.latitude];
//        NSString * currentLng = [NSString stringWithFormat:@"%.6f", currentLocation.coordinate.longitude];
//       
//        [[NSUserDefaults standardUserDefaults] setObject:currentLat forKey:LATITUDE];
//        [[NSUserDefaults standardUserDefaults] setObject:currentLng forKey:LONGITUDE];
//        [_locationManager stopUpdatingLocation];
//        NSLog(@"%@",currentLat);
//
//    }
//}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *location = [locations lastObject];
    NSLog(@"lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
    
    NSString *currentLatitude = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString *currentLongitude = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    [rNSUserDefault removeObjectForKey:@"kUserCurrentLatitude"];
    [rNSUserDefault removeObjectForKey:@"kUserCurrentLongitude"];
    
    [rNSUserDefault setObject:currentLatitude forKey:kLAT];
    [rNSUserDefault setObject:currentLongitude forKey:kLNG];
    [rNSUserDefault synchronize];
    
    
    NSLog(@"lat%@ - lon%@",[[NSUserDefaults standardUserDefaults] valueForKey:kUserCurrentLatitude],[[NSUserDefaults standardUserDefaults] valueForKey:kUserCurrentLongitude]);
    
    [_locationManager stopUpdatingLocation];
    
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status)
    {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            [_locationManager requestAlwaysAuthorization];
        }
            break;
        default:
        {
            [_locationManager startUpdatingLocation];
        }
            break;
    }
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([error code] == kCLErrorDenied){
        [rNSUserDefault setObject:@"0" forKey:kLAT];
        [rNSUserDefault setObject:@"0" forKey:kLNG];
    }
    [manager stopUpdatingLocation];
}
-(void)showRatingScreen:(CurrentActivityModel *)model{
    
    _rating = [[RatingView alloc]initWithFrame:CGRectMake(0, 0, kframe.width, kframe.height) :model];
    [kWindow addSubview:_rating];
}

-(void)showCustomcategory{
    
    _customCategory = [[CustomCategory alloc]initWithFrame:CGRectMake(0, 0, kframe.width, kframe.height)];
    [kWindow addSubview:_customCategory];
}
-(void)tokenExpired{
    NSLog(@"yeppp");
}

@end
