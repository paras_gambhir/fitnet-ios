//
//  APIManager.h
//  iCollege
//
//  Created by cbl20 on 5/24/16.
//  Copyright © 2016 Kunal Gupta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

@interface APIManager : NSObject

+(void)postData : (NSString *)urlStr : (NSDictionary *)parameters : (void(^)(NSDictionary * response_success))success : (void(^)(NSError * response_error))failure;

+(void)postPic : (NSString *)urlStr : (NSDictionary *)parameters :(BOOL)isImage : (NSData *)data : (void(^)(NSDictionary * response_success))success : (void(^)(NSError * response_error))failure;
    
+(void)getJSONResponse :(NSString *)urlStr :(void(^)(NSDictionary * response_success))success : (void(^)(NSError * response_error))failure;

@end
