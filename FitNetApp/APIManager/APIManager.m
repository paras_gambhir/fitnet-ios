//
//  APIManager.m
//  iCollege
//
//  Created by cbl20 on 5/24/16.
//  Copyright © 2016 Kunal Gupta. All rights reserved.
//

#import "APIManager.h"
#import "AFHTTPRequestOperation.h"
#import "Reachability.h"

@implementation APIManager

+(void)postData : (NSString *)urlStr : (NSDictionary *)parameters : (void(^)(NSDictionary * response_success))success : (void(^)(NSError * response_error))failure {
    
    if([[APIManager alloc] internetWorking] == NO){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please check your internet connection!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer =  [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/json", nil];
    [manager POST:urlStr parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData){
        
    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              //              NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
              success([NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil]);
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              failure(error);
          }];
}

+(void)postPic : (NSString *)urlStr : (NSDictionary *)parameters :(BOOL)isImage : (NSData *)data : (void(^)(NSDictionary * response_success))success : (void(^)(NSError * response_error))failure{
    
    if([[APIManager alloc] internetWorking] == NO){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please check your internet connection!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer =  [AFHTTPResponseSerializer serializer];
    [manager POST:urlStr parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData){
        
        if(isImage == true){
            [formData appendPartWithFileData: data name:@"profilePic" fileName:@"temp.jpg" mimeType:@"image/jpg"];
        }
    }
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              //              NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
              success([NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil]);
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              failure(error);
          }];
}

+(void)getJSONResponse :(NSString *)urlStr :(void(^)(NSDictionary * response_success))success : (void(^)(NSError * response_error))failure{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    [manager GET:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(error);
    }];
}

-(BOOL)internetWorking {
    
    Reachability *reachTest = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus internetStatus = [reachTest  currentReachabilityStatus];
    
    if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN)){
        return NO;
    }
    else{
        return YES;
    }
}

@end
