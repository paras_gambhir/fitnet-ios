//
//  CategoryVC.m
//  FitNetApp
//
//  Created by anish on 07/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "CategoryVC.h"

@interface CategoryVC ()
{
@private
    
    NSDictionary *catDic;
    NSDictionary *finalDic;
    NSMutableArray *catSelectedArray;
    NSMutableArray *catnameSelectedArray;
    NSString *catSelectedname;
    
}

@end

@implementation CategoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *_topView =   [self createUIImageView:CGRectMake(0.0, 0, self.view.frame.size.width, 64) backgroundColor:[UIColor colorWithRed:57.0/255.0 green:160.0/255.0 blue:217.0/255.0 alpha:1.0] image:nil isLogo:YES];
    
    _topView.userInteractionEnabled=YES;
    
    UILabel *_lblIAM = [self createLable:CGRectMake(0.0, 25, self.view.frame.size.width, 30.0) bckgroundColor:[UIColor clearColor] title:@"Select Category" font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    _lblIAM.textAlignment = NSTextAlignmentCenter;
    _lblIAM.textColor = [UIColor whiteColor];
    [_topView addSubview:_lblIAM];
    
    
    UIButton *_btnBackButton = [self createButton:CGRectMake(self.view.frame.size.width-56, 16, 48, 48) bckgroundColor:[UIColor clearColor] image:nil title:@"Done" font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
//    [_btnBackButton setImage:[UIImage imageNamed:@"nav_back1"] forState:UIControlStateNormal];
    [_btnBackButton addTarget:self action:@selector(func__btnBack) forControlEvents:UIControlEventTouchUpInside];
    [_btnBackButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_topView addSubview:_btnBackButton];
    
    
    [self.view addSubview:_topView];


    catDic = [[NSDictionary alloc]init];
    catDic = [[[[NSUserDefaults standardUserDefaults] valueForKey:kAllCategoryList] valueForKey:@"data"] valueForKey:@"categories"];
    catSelectedname=@"";
    
    catSelectedArray= [[NSMutableArray alloc]init];
    catnameSelectedArray = [[NSMutableArray alloc]init];
    
    [_categoryTable setDelegate:self];
    _categoryTable.dataSource=self;
    [_categoryTable reloadData];
    
    [self.view endEditing:true];


}
-(void)func__btnBack
{
    NSLog(@"catSelectedname=%@",catSelectedname);
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIImaheView UI

-(UIImageView*)createUIImageView:(CGRect)frame backgroundColor:(UIColor*)backgroundColor image:(UIImage*)image isLogo:(BOOL)isLogo{
    
    UIImageView *_imageView = [[UIImageView alloc]initWithFrame:frame];
    _imageView.image = image;
    _imageView.backgroundColor = backgroundColor;
    return _imageView;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UILable UI

-(UILabel*)createLable:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UILabel *_lable = [[UILabel alloc] init];
    _lable.backgroundColor = backgroundColor;
    _lable.frame = frame;
    _lable.text = title;
    _lable.textColor = titleColor;
    _lable.font = font;
    
    return _lable;
}
//------------------------------------- bckgroundColor ----------------------------------------------------------------------------------------------
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIButton UI
-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
    _button.backgroundColor = backgroundColor;
    _button.frame = frame;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitleColor:titleColor forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5] forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5Hov] forState:UIControlStateHighlighted];
    
    _button.titleLabel.font = font;
    
    return _button;
}

#pragma -mark <TableView Delegate>
#pragma -mark
/************************* TableViewMethods **********************************/
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//-------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return catDic.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
        static NSString *cellIdentifier = @"MyHomeIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        UILabel *label=(UILabel *)[cell.contentView viewWithTag:100];
        label.text = [[[catDic valueForKey:@"name"] objectAtIndex:indexPath.row] capitalizedString];

    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            
            [catSelectedArray removeObject:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[catDic valueForKey:@"_id"] objectAtIndex:indexPath.row]]]];
            
            [catnameSelectedArray removeObject:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[[catDic valueForKey:@"name"] objectAtIndex:indexPath.row]]]];
            
            catSelectedname = @"";
            for (int i =0 ; i <catnameSelectedArray.count;i++)
            {
                catSelectedname = [catSelectedname stringByAppendingFormat:@" , %@",[catnameSelectedArray objectAtIndex:i]];
                
            }
            
            catSelectedname = [catSelectedname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if ([catSelectedname hasPrefix:@","] && [catSelectedname length] > 1)
            {
                catSelectedname = [catSelectedname substringFromIndex:1];
            }
            [[NSUserDefaults standardUserDefaults] setObject:catSelectedname  forKey:kFitness];
            
            [[NSUserDefaults standardUserDefaults] setObject:catSelectedArray  forKey:kSelectIdData];
            [[NSUserDefaults standardUserDefaults] synchronize];

            
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [catSelectedArray addObject:[NSString stringWithFormat:@"%@",[[catDic valueForKey:@"_id"] objectAtIndex:indexPath.row]]];
            
            [catnameSelectedArray addObject:[NSString stringWithFormat:@"%@",[[catDic valueForKey:@"name"] objectAtIndex:indexPath.row]]];
            
            catSelectedname =@"";
            for (int i =0 ; i <catnameSelectedArray.count;i++)
            {
                catSelectedname = [catSelectedname stringByAppendingFormat:@" , %@",[catnameSelectedArray objectAtIndex:i]];
                
            }
            
            catSelectedname = [catSelectedname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if ([catSelectedname hasPrefix:@","] && [catSelectedname length] > 1) {
                catSelectedname = [catSelectedname substringFromIndex:1];
            }
            [[NSUserDefaults standardUserDefaults] setObject:catSelectedname  forKey:kFitness];
            [[NSUserDefaults standardUserDefaults] setObject:catSelectedArray  forKey:kSelectIdData];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
