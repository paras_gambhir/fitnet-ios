//
//  OtherProfileVC.h
//  FitNetApp
//
//  Created by anish on 23/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPFloatRatingView.h"
#import "ProfileModel.h"
#import <YYWebImage.h>
#import "ViewReviewVC.h"
#import <UIImageView+WebCache.h>

@interface OtherProfileVC : UIViewController<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITableViewDataSource,UITextFieldDelegate,TPFloatRatingViewDelegate>


@property (strong, nonatomic) IBOutlet TPFloatRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *userLocationLbl;
@property (weak, nonatomic) IBOutlet UIView *imageTopBar;
@property (weak, nonatomic) IBOutlet UIButton *uploadImageBtn;
@property (weak, nonatomic) IBOutlet UIButton *crossBtn;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIView *editView;

@property (weak, nonatomic) IBOutlet UITextField *aboutTextField;
@property (weak, nonatomic) IBOutlet UIButton *fitnessBtn;
@property (weak, nonatomic) IBOutlet UIButton *timeBtnShow;
@property (weak, nonatomic) IBOutlet UIButton *levelBtn;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalReview;

@property (weak, nonatomic) IBOutlet UIButton *btnViewReview;
@property (weak, nonatomic) IBOutlet UITextField *sessionTextField;
@property (weak, nonatomic) IBOutlet UITextField *goodForField;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *labelFitnessValue;
@property (weak, nonatomic) IBOutlet UILabel *labelTimeValue;

@property (weak, nonatomic) IBOutlet UILabel *aboutMeLbl;
@property (weak, nonatomic) IBOutlet UILabel *fitnessLbl;
@property (weak, nonatomic) IBOutlet UILabel *sessionLbl;
@property (weak, nonatomic) IBOutlet UILabel *advancedLbl;
- (IBAction)actionBtnViewReview:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *motivationLbl;
@property NSString *strUserID;

@property ProfileModel *model;

@end
