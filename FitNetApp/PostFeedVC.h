//
//  PostFeedVC.h
//  FitNetApp
//
//  Created by anish on 18/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileModel.h"
#import "NetworkFeedCell.h"
#import "PostFeedModal.h"
#import "UserInfo.h"
#import "BaseControllerVC.h"


@interface PostFeedVC : BaseControllerVC <UITableViewDelegate,UITableViewDataSource,NetworkCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *postToFeedTableView;

@property (weak, nonatomic) IBOutlet UIButton *btnRefine;
@property (strong, nonatomic) NSDictionary  *postDic;
@property  ProfileModel *profile ;
@property NSMutableArray *arrTableData;
- (IBAction)actionBtnRefine:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelAttentees;
@property (weak, nonatomic) IBOutlet UILabel *labelAlert;

@property NSMutableArray *arrTabledata;

@property (weak, nonatomic) IBOutlet UITableView *tableViewNetwork;

@end
