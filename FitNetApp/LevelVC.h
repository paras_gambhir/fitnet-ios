//
//  LevelVC.h
//  FitNetApp
//
//  Created by anish on 11/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LevelVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *levelTableView;
@property (weak, nonatomic) IBOutlet UIImageView *levelBg;

@end
