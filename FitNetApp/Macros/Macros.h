//
//  Macros.h
//  Thredz
//
//  Created by Kunal Gupta on 28/01/16.
//  Copyright © 2016 Kunal Gupta. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Macros : NSObject

#define kWindow [[[UIApplication sharedApplication] delegate] window]
#define kframe [[UIScreen mainScreen] bounds].size
#define UD_USER_INFO @"iCollegeUserInfo"
#define UD_DEVICE_TOKEN @"iCollegeDeviceToken"
#define UD_NOTIFICATION_INFO @"iCollegeNotificationInfo"
#define N_NOTI_INFO @"iCollegeNotiInfo"
#define USER_ID [USER_DICT valueForKey:@"id"]
//    [[NSUserDefaults standardUserDefaults]valueForKey:@"iCollegeUserInfo"]
#define USER_DICT [[NSUserDefaults standardUserDefaults]valueForKey:UD_USER_INFO]
#define TOKEN [[[NSUserDefaults standardUserDefaults] valueForKey:UD_USER_INFO] valueForKey:@"token"]
#define LATITUDE @"iCollegeLatitude"
#define LONGITUDE @"iCollegeLongitude"
#define MY_LATITUDE [[NSUserDefaults standardUserDefaults]valueForKey:LATITUDE]
#define MY_LONGITUDE [[NSUserDefaults standardUserDefaults]valueForKey:LONGITUDE]
#define IMAGE_URL @"http://192.168.100.117:8000/resize/%@"

#define BASE_URL @"http://54.187.56.3:8001"

#define STATIC_TOKEN @"dfsgdfgsdgr32r34ff34f34f34f43f43f43trgrfgfbdfrvt4ggg"

#define ALL_REVIEW @"%@/api/customer/getAllReviews"

@end
