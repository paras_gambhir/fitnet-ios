//
//  CategoryVC.h
//  FitNetApp
//
//  Created by anish on 07/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UITableView *categoryTable;




@end
