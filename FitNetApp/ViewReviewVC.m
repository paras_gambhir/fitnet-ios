//
//  ViewReviewVC.m
//  FitNetApp
//
//  Created by anish on 07/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "ViewReviewVC.h"

@interface ViewReviewVC ()

@end

@implementation ViewReviewVC

- (void)viewDidLoad {
    [super viewDidLoad];

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if(_strOtherUserID.length != 0){
        [self hitAPIOtherUser];
    }
    else{
        [self hitAPI];
    }
}
- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - HIT API

-(void)hitAPI{
    [super showLoader];
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"] forKey:@"accessToken"];
    
    
    [API PostViewReview:[dict mutableCopy] completionHandler:^(NSDictionary *result,NSError *error){
        NSLog(@"%@",result);
        _arrTableData = [NSMutableArray new];
        _arrTableData = [[ReviewModel parseDataToArray:[result valueForKeyPath:@"data.reviews"]] mutableCopy];
        if([_arrTableData count] == 0){
            [_tableView setHidden:YES];
            _labelAlert.text = @"No reviews added yet!";
        }
        else{
            [_tableView setHidden:NO];
        }
        [_tableView reloadData];
    }];
}

-(void)hitAPIOtherUser{
    [super showLoader];
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"] forKey:@"accessToken"];
    [dict setObject:_strOtherUserID forKey:@"userId"];
    
    [API viewReviewOfOtherUser:[dict mutableCopy] completionHandler:^(NSDictionary *result,NSError *error){
        NSLog(@"%@",result);
        _arrTableData = [NSMutableArray new];
        _arrTableData = [[ReviewModel parseDataToArray:[result valueForKeyPath:@"data.reviews"]] mutableCopy];
        if([_arrTableData count] == 0){
            [_tableView setHidden:YES];
            _labelAlert.text = @"No reviews added yet!";
        }
        else{
            [_tableView setHidden:NO];
        }
        [_tableView reloadData];
    }];
}



-(void)handleSuccess:(NSDictionary *)dict{
}

#pragma mark - TABLEVIEW DELEGATE AND DATASOURCE

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrTableData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    ViewReviewTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ViewReviewTableCell"];
    [cell configureCell:[_arrTableData objectAtIndex:indexPath.row]];
    return  cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ViewCommentsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewCommentsVC"];
    [vc setReviewModel:[_arrTableData objectAtIndex:indexPath.row]];
    [vc setArrTableData:[[CommentModel parseDataToArray:[[_arrTableData objectAtIndex:indexPath.row] arrComments]] mutableCopy]];
    [vc setStrReviewID:[[_arrTableData objectAtIndex:indexPath.row] strID]];
    [self.navigationController pushViewController:vc animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 77;
}


#pragma mark :- ACTION BUTTONS

- (IBAction)actionBtnback:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
