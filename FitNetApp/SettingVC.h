//
//  SettingVC.h
//  FitNetApp
//
//  Created by anish on 20/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseControllerVC.h"
#import "BlockUnBlockUserVC.h"
#import <MessageUI/MessageUI.h>
#import "TermsAndConditionVC.h"
#import "UserInfo.h"

@interface SettingVC : BaseControllerVC <UIActionSheetDelegate,MFMailComposeViewControllerDelegate,UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIButton *TermsBtn;
@property (weak, nonatomic) IBOutlet UIButton *termsandconditionBtn;
@property (weak, nonatomic) IBOutlet UIButton *reportBtn;
@property (weak, nonatomic) IBOutlet UIButton *blockBtn;
@property (weak, nonatomic) IBOutlet UIButton *logoutBtn;
@property (weak, nonatomic) IBOutlet UISlider *sliderBar;
@property (weak, nonatomic) IBOutlet UILabel *sliderTxtLabel;
@property (weak, nonatomic) IBOutlet UIButton *homeLoctionBtn;
@property (weak, nonatomic) IBOutlet UIButton *workLocationBtn;
- (IBAction)sliderValueDidChange:(id)sender;

- (IBAction)sliderDidEnd:(id)sender;

@property NSString *strHomeLat;
@property NSString *strHomeLng;
@property NSString *strHomeAddress;

@property NSString *strWorkLat;
@property NSString *strWorkLng;
@property NSString *strWorkAddress;


@end
