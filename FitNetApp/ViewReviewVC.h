//
//  ViewReviewVC.h
//  FitNetApp
//
//  Created by anish on 07/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewReviewTableCell.h"
#import "APIManager.h"
#import "Macros.h"
#import "BaseControllerVC.h"
#import "ReviewModel.h"
#import "ViewCommentsVC.h"

@interface ViewReviewVC : BaseControllerVC<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewTop;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *labelAlert;
- (IBAction)actionBtnback:(id)sender;
@property (weak, nonatomic) IBOutlet TPFloatRatingView *viewRating;

@property NSMutableArray *arrTableData;

@property NSString *strOtherUserID;

@end
