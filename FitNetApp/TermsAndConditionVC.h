//
//  TermsAndConditionVC.h
//  FitNetApp
//
//  Created by anish on 24/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsAndConditionVC : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
- (IBAction)actionBtnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnBAck;



@end
