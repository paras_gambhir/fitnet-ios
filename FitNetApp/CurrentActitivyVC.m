//
//  CurrentActitivyVC.m
//  FitNetApp
//
//  Created by anish on 18/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "CurrentActitivyVC.h"
#import "AsyncImageView.h"
@interface CurrentActitivyVC ()
{
    @private
    
    NSDictionary *postDic;
    NSDictionary *tempDic;
    
    NSString *jobId;
    int RatingVal;
}

@end

@implementation CurrentActitivyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _segmentControl.selectedSegmentIndex = 1;

    UIImageView *_topView =  [self createUIImageView:CGRectMake(0.0, 0, self.view.frame.size.width, 64) backgroundColor:[UIColor colorWithRed:57.0/255.0 green:160.0/255.0 blue:217.0/255.0 alpha:1.0] image:nil isLogo:YES];
    
    
    _topView.userInteractionEnabled=YES;
    
    UILabel *_lblIAM = [self createLable:CGRectMake(0.0, 25, self.view.frame.size.width, 30.0) bckgroundColor:[UIColor clearColor] title:@"CURRENT ACTIVITY" font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    _lblIAM.textAlignment = NSTextAlignmentCenter;
    _lblIAM.textColor = [UIColor whiteColor];
    [_topView addSubview:_lblIAM];
    
    _labelAlert.hidden = YES;
    [self.view addSubview:_topView];
    
    
    _currentActivityTable.layer.borderWidth = 1.0;
    _currentActivityTable.layer.borderColor = [UIColor colorWithRed:126.0/255.0 green:161.0/255.0 blue:177.0/255.0 alpha:1.0].CGColor;
    
    [_currentActivityTable setDelegate:self];
    _currentActivityTable.dataSource=self;
    [_currentActivityTable reloadData];
    
    
    [[_commentTextView layer] setBorderWidth:0.8f];
    [[_commentTextView layer] setBorderColor:[UIColor blackColor].CGColor];
    
    
    
    self.ratingView.delegate = self;
    self.ratingView.emptySelectedImage = [UIImage imageNamed:@"star_inactive"];
    self.ratingView.fullSelectedImage = [UIImage imageNamed:@"star_active"];
    self.ratingView.contentMode = UIViewContentModeScaleAspectFill;
    self.ratingView.maxRating = 5;
    self.ratingView.minRating = 0;
    self.ratingView.rating = 0;
    self.ratingView.editable = YES;
    self.ratingView.halfRatings = NO;
    self.ratingView.floatRatings = NO;
    
    self.ratingLabel.text = [NSString stringWithFormat:@"%.2f", self.ratingView.rating];
    self.liveLabel.text = [NSString stringWithFormat:@"%.2f", self.ratingView.rating];
    UIButton *_btnSetting = [self createButton:CGRectMake(self.view.frame.size.width-45, 20, 40, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"nav_settings1"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    [_btnSetting setImage:[UIImage imageNamed:@"nav_settings1"] forState:UIControlStateNormal];
    [_btnSetting addTarget:self action:@selector(func__btnSetting) forControlEvents:UIControlEventTouchUpInside];
    //[_btnPlus setTitle:@"Plus" forState:UIControlStateNormal];
    _btnSetting.backgroundColor = [UIColor clearColor];
    [_topView addSubview:_btnSetting];
    
    
    [self.view addSubview:_topView];
    
    
    postDic = [NSDictionary alloc];
    
    
    //_postToFeedTableView.backgroundColor = [UIColor yellowColor];
    
    // Do any additional setup after loading the view.
}

-(void)func__btnSetting
{
    SettingVC *SettingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingVC"];
    [self.navigationController pushViewController:SettingVC animated:YES];
}
-(void)refreshApi
{
    
    _segmentControl.selectedSegmentIndex = 1;
    [_segmentControl setSelectedSegmentIndex:1];
    [appDelegate() CheckInternetConnection];
    if([appDelegate() internetWorking] ==0)
    {
        NSDictionary *initdataInfo = @{
                                       @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"]
                                       };
        NSLog(@"Category%@",initdataInfo);
        [API PostCurrentActivityFeedWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
         {
             NSLog(@"json=%@",json);
             postDic =json;
             
             tempDic = [[postDic valueForKey:@"data"] valueForKey:@"onGoing"];
             
             [_currentActivityTable setDelegate:self];
             _currentActivityTable.dataSource=self;
             [_currentActivityTable reloadData];
             
             if(tempDic.count ==0)
             {
                 _currentActivityTable.hidden =YES;
                 _labelAlert.hidden = NO;
                 _labelAlert.text = (_segmentControl.selectedSegmentIndex == 0) ? @"No My Activities Yet.!" : @"No Ongoing Activities Yet.!"  ;
             }
             else
             {
                 _currentActivityTable.hidden =NO;
             }
             
             
             [[NSUserDefaults standardUserDefaults] setObject:json  forKey:kCurrentActivity];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             
             
         }];
    }
    else
    {
        [appDelegate() dismissActivityAlert];
        [appDelegate() errorMessages:kAlertInternetConnection];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rateuser:) name:@"rate" object:nil];
    
    [self refreshApi];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)rateuser:(NSNotification *)noti{
    NSLog(@"%@",noti);
    [self hitAPI:[noti.userInfo mutableCopy]];
}
-(void)hitAPI:(NSMutableDictionary *)dict{
    [dict setObject:[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"] forKey:@"accessToken"];
    NSLog(@"%@",dict);
    
    [API PostEndJobWithInfo:dict completionHandler:^(NSDictionary *json,NSError *error)
     {
         NSLog(@"json=%@",json);
         [self refreshApi];
         
     }];
    
}
#pragma -mark
#pragma -mark <TableView Delegate>
#pragma -mark
/************************* TableViewMethods **********************************/
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//-------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tempDic.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CurrentActivityCell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSLog(@"value=%@",[[[[tempDic valueForKey:@"customer"] valueForKey:@"profilePicURL"] valueForKey:@"thumbnail"] objectAtIndex:indexPath.row]);
        
        UIImageView *imaegeViewProfilePic = (UIImageView *)[cell.contentView viewWithTag:1200];
//        _thumbImageView.backgroundColor = [UIColor clearColor];
//        _thumbImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        _thumbImageView.layer.borderWidth=1.f;
//        _thumbImageView.imageURL = [NSURL URLWithString:[[[[tempDic valueForKey:@"customer"] valueForKey:@"profilePicURL"] valueForKey:@"thumbnail"] objectAtIndex:indexPath.row]];
    
    [imaegeViewProfilePic sd_setImageWithURL:[NSURL URLWithString:[[tempDic valueForKeyPath:@"customer.profilePicURL.thumbnail"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"profile_icon-1"]];
    
        UILabel *label=(UILabel *)[cell.contentView viewWithTag:1201];//
        label.text = [[[[tempDic valueForKey:@"customer"] valueForKey:@"name"] objectAtIndex:indexPath.row] capitalizedString];
        
        UITextView *declabel=(UITextView *)[cell.contentView viewWithTag:1205];
        declabel.text = [[tempDic valueForKey:@"description"] objectAtIndex:indexPath.row];
        declabel.textContainerInset = UIEdgeInsetsZero;
        declabel.textContainer.lineFragmentPadding = 0;
        //declabel.backgroundColor = [UIColor yellowColor];
        declabel.textAlignment = NSTextAlignmentLeft;

        
    UILabel *labelLoction=(UILabel *)[cell.contentView viewWithTag:1203];
    labelLoction.text = [NSString stringWithFormat:@"Location: %@",[[[tempDic valueForKey:@"locationAddress"] objectAtIndex:indexPath.row] capitalizedString]];
    
    UILabel *labelTime=(UILabel *)[cell.contentView viewWithTag:1202];
    labelTime.text = [NSString stringWithFormat:@"Posted: %@",[[[tempDic valueForKey:@"creationDate"] objectAtIndex:indexPath.row] capitalizedString]];
    
    if (_segmentControl.selectedSegmentIndex == 0)
    {
        UIButton *buttonEnd=(UIButton *)[cell.contentView viewWithTag:1204];
        buttonEnd.hidden =YES;
    }
    
    if (_segmentControl.selectedSegmentIndex == 1){
        
    UIButton *buttonEnd=(UIButton *)[cell.contentView viewWithTag:1204];
         buttonEnd.hidden =NO;
    [buttonEnd setTitle:[NSString stringWithFormat:@"%@",[[tempDic valueForKey:@"_id"] objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        [buttonEnd setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [buttonEnd addTarget:self action:@selector(buttonEndAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    UIButton *btnChat=(UIButton *)[cell.contentView viewWithTag:5005];
    [btnChat setTitle:@"Chat" forState:UIControlStateNormal];
    [btnChat setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnChat addTarget:self action:@selector(btnChatAction:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
    
}
-(NSIndexPath *) getButtonIndexPath:(UIButton *) button{
    CGRect buttonFrame = [button convertRect:button.bounds toView:_currentActivityTable];
    return [_currentActivityTable indexPathForRowAtPoint:buttonFrame.origin];
}

-(IBAction)btnChatAction:(UIButton*)sender{

    NSIndexPath *indexPath = [self getButtonIndexPath:sender];

    NSString *strPostID;

    if (_segmentControl.selectedSegmentIndex == 0){
        strPostID = [[postDic valueForKeyPath:@"data.myActivity._id"] objectAtIndex:indexPath.row];
    }
    else{
        strPostID = [[postDic valueForKeyPath:@"data.onGoing._id"] objectAtIndex:indexPath.row];
    }
    
    
    ChatVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatVC"];
    [vc setStrPostID:strPostID];
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)buttonEndAction:(UIButton*)sender{
    
    NSIndexPath *indexPath = [self getButtonIndexPath:sender];
    [super showRatingScreen:[_arrTableData objectAtIndex:indexPath.row]];

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIImaheView UI

-(UIImageView*)createUIImageView:(CGRect)frame backgroundColor:(UIColor*)backgroundColor image:(UIImage*)image isLogo:(BOOL)isLogo{
    
    UIImageView *_imageView = [[UIImageView alloc]initWithFrame:frame];
    _imageView.image = image;
    _imageView.backgroundColor = backgroundColor;
    return _imageView;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UILable UI

-(UILabel*)createLable:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UILabel *_lable = [[UILabel alloc] init];
    _lable.backgroundColor = backgroundColor;
    _lable.frame = frame;
    _lable.text = title;
    _lable.textColor = titleColor;
    _lable.font = font;
    
    return _lable;
}
- (IBAction)segmentControlAction:(id)sender
{
    if (_segmentControl.selectedSegmentIndex == 0)
    {
        tempDic=[[postDic valueForKey:@"data"] valueForKey:@"myActivity"];
        [_currentActivityTable setDelegate:self];
        _currentActivityTable.dataSource=self;
        [_currentActivityTable reloadData];
        
        if(tempDic.count ==0)
        {
            _currentActivityTable.hidden =YES;
            _labelAlert.text =  @"No My Activities Yet.!"  ;

        }
        else
        {
              _currentActivityTable.hidden =NO;
        }
        
    
    } else if(_segmentControl.selectedSegmentIndex == 1)
    {
        tempDic =[[postDic valueForKey:@"data"] valueForKey:@"onGoing"];
        
        [_currentActivityTable setDelegate:self];
        _currentActivityTable.dataSource=self;
        [_currentActivityTable reloadData];
        
        if(tempDic.count ==0)
        {
            _currentActivityTable.hidden =YES;
            _labelAlert.hidden = NO;
            _labelAlert.text =  @"No Ongoing Activities Yet.!"  ;

        }
        else
        {
            _arrTableData = [[CurrentActivityModel parseDataToArray:[tempDic mutableCopy]] mutableCopy];
            _currentActivityTable.hidden =NO;
        }
    }
}

- (IBAction)endActionJob:(id)sender {
    
  
    NSString *textDesc=  [_commentTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (textDesc.length==0)
    {
        [appDelegate() errorMessages:kAlertDis];
    }
    else
    {
        [appDelegate() CheckInternetConnection];
        if([appDelegate() internetWorking] ==0)
        {
            
            NSDictionary *initdataInfo = @{
                                            @"accessToken":[[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData] valueForKey:@"accessToken"],
                                           @"jobId":jobId,
                                           @"rating":[NSString stringWithFormat:@"%d",RatingVal],
                                           @"comments":textDesc,
                                           };
            NSLog(@"Category%@",initdataInfo);
            [API PostEndJobWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
             {
                 NSLog(@"json=%@",json);
                 
                 UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:kAppName message:[NSString stringWithFormat:@"%@",[json valueForKey:@"message"]] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
                 [alertview show];
                 _backBg.hidden = YES;
                 _endPopup.hidden =YES;
                 [self.view endEditing:YES];
                 [self refreshApi];
                 
             }];
        }
        else
        {
            [appDelegate() dismissActivityAlert];
            [appDelegate() errorMessages:kAlertInternetConnection];
        }
    }
    
}
- (IBAction)endCrossPopupAction:(id)sender {
    
    _backBg.hidden = YES;
    _endPopup.hidden =YES;
    [self.view endEditing:YES];
}
-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
    _button.backgroundColor = backgroundColor;
    _button.frame = frame;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitleColor:titleColor forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5] forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5Hov] forState:UIControlStateHighlighted];
    
    _button.titleLabel.font = font;
    
    return _button;
}
#pragma mark - TPFloatRatingViewDelegate

- (void)floatRatingView:(TPFloatRatingView *)ratingView ratingDidChange:(CGFloat)rating
{
    NSLog(@"rating=%.2f",rating);
    
    RatingVal = rating;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
