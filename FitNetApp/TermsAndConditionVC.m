//
//  TermsAndConditionVC.m
//  FitNetApp
//
//  Created by anish on 24/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "TermsAndConditionVC.h"

@interface TermsAndConditionVC ()

@end

@implementation TermsAndConditionVC

- (void)viewDidLoad {
    [super viewDidLoad];

    _spinner.color = [UIColor blackColor];
    _webView.delegate = self;
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://54.187.56.3:8001/api/customer/termsAndConditions"] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:15.0f]];
}

#pragma mark - WEB VIEW DELEGATES

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [_spinner startAnimating];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [_spinner stopAnimating];
    [_spinner setHidesWhenStopped:YES];
}

- (IBAction)actionBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end

