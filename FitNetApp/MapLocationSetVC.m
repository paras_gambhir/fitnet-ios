//
//  MapLocationSetVC.m
//  FitNetApp
//
//  Created by anish on 09/08/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "MapLocationSetVC.h"
#import <MapKit/MapKit.h>
#import "DDAnnotation.h"
@interface MapLocationSetVC ()
{
    @private
    CLLocationCoordinate2D theCoordinate;
    NSString * latStr;
    NSString * longStr;
    CLLocationManager *locationManager;
    NSString *searchedLocationString;
    NSMutableDictionary *dic_adress;
    NSMutableArray *arr_address;
    NSString *str_Address;
    UILabel* addressTextLable;
    NSArray *Arr_latilong;
    NSDictionary *dic_loc;
    DDAnnotation *pin_annotation;
    
}

@end

@implementation MapLocationSetVC

- (void)viewDidLoad {
    
    UIImageView *_topView =   [self createUIImageView:CGRectMake(0.0, 0, self.view.frame.size.width, 64) backgroundColor:[UIColor colorWithRed:57.0/255.0 green:160.0/255.0 blue:217.0/255.0 alpha:1.0] image:nil isLogo:YES];
    
    _topView.userInteractionEnabled=YES;
    
    UILabel *_lblIAM = [self createLable:CGRectMake(0.0, 25, self.view.frame.size.width, 30.0) bckgroundColor:[UIColor clearColor] title:@"Location" font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    _lblIAM.textAlignment = NSTextAlignmentCenter;
    _lblIAM.textColor = [UIColor whiteColor];
    [_topView addSubview:_lblIAM];
    
    
    UIButton *_btnBackButton = [self createButton:CGRectMake(0, 20, 40, 40) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"nav_back1"] title:nil font:[UIFont fontWithName:@"OpenSans-Light" size:18.0] titleColor:[UIColor blackColor]];
    [_btnBackButton setImage:[UIImage imageNamed:@"nav_back1"] forState:UIControlStateNormal];
    [_btnBackButton addTarget:self action:@selector(func__btnBack) forControlEvents:UIControlEventTouchUpInside];
    [_btnBackButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_topView addSubview:_btnBackButton];
    
    
    [self.view addSubview:_topView];
    
    _searchTableView.layer.borderWidth= 1;
    _searchTableView.layer.borderColor = [UIColor blueColor].CGColor;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    [locationManager startUpdatingLocation];
    [locationManager requestWhenInUseAuthorization];
    
    
    latStr=[NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
    longStr=[NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
    
    NSLog(@"latStr=%@",latStr);
    NSLog(@"longStr=%@",longStr);
    
    NSString *strLat = [[NSUserDefaults standardUserDefaults] valueForKey:kLAT];
    NSString *strLng = [[NSUserDefaults standardUserDefaults] valueForKey:kLNG];
    
    
    GMSCameraPosition* camera = [GMSCameraPosition cameraWithLatitude:[latStr doubleValue]
                                                            longitude:[longStr doubleValue]
                                                                 zoom:16];
    if(_strHomeWorkAddress.length  > 0){
        _mapView.myLocationEnabled = NO;

    }
    else{
        _mapView.myLocationEnabled = YES;
    }
    
    
    NSLog(@"%f",_mapView.myLocation.coordinate.latitude);
    NSLog(@"%f",_mapView.myLocation.coordinate.longitude);
    _mapView.settings.compassButton = YES;
    [_mapView setCamera:camera];
    
//    [_mapGet setMapType:MKMapTypeStandard];
//    [_mapGet setZoomEnabled:YES];
//    [_mapGet setScrollEnabled:YES];
//    _mapGet.showsUserLocation=YES;
//    _mapGet.showsBuildings=YES;
//    _mapGet.backgroundColor=[UIColor colorWithRed:223.0/255.0 green:223.0/255.0 blue:223.0/255.0 alpha:1.0];
//    
//    MKCoordinateRegion region = { {0.0, 0.0 },{ 0.0, 0.0 }};
//    region.center.latitude = theCoordinate.latitude ;
//    region.center.longitude = theCoordinate.longitude;
//    region.span.longitudeDelta = 0.01f;
//    region.span.latitudeDelta = 0.01f;
//    [_mapGet setRegion:region animated:YES];
//    
//    MKCoordinateSpan span;
//    span.latitudeDelta = 0.005;
//    span.longitudeDelta = 0.005;
//    
//    CLLocationCoordinate2D coordinate;
//    coordinate.latitude = [latStr floatValue];
//    coordinate.longitude = [longStr floatValue];
//    
//    
//    pin_annotation = [[DDAnnotation alloc] initWithCoordinate:theCoordinate addressDictionary:nil];
//    pin_annotation.title = @"Current Location";
//    [_mapGet addAnnotation:pin_annotation];
    
    
    _textFieldMap.clearButtonMode = UITextFieldViewModeWhileEditing;
}

//#pragma mark -MapView
//- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
//{
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
//    [_mapGet setRegion:[_mapGet regionThatFits:region] animated:YES];
//    
//    // Add an annotation
//    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//    point.coordinate = userLocation.coordinate;
//    point.title = @"Where am I?";
//    point.subtitle = @"I'm here!!!";
//    
//    [_mapGet addAnnotation:point];
//}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField.text.length != 0)
    {
        NSString *result;
        
        result = [[NSString alloc] init];
        result = [NSString stringWithFormat:@"%@",textField.text];
        
        searchedLocationString = [[NSString alloc] init];
        searchedLocationString = [result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        NSString *newString = [searchedLocationString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        [self CallFunctionToGetLatLong : newString];
    }
    //        else
    //        {
    //
    //            [UIView animateWithDuration:0.3f animations:^{
    //
    //                _searchTableView.frame=CGRectMake(_searchTableView.frame.origin.x, _searchTableView.frame.origin.y, _searchTableView.frame.size.width, 0);
    //            }
    //                             completion:^(BOOL finished)
    //             {
    //             }];
    //        }
    
    
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    textField.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f];
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    NSLog(@"textFieldShouldClear:");
    textField.text = @"";
    [textField resignFirstResponder];
    
    _backImage.hidden = YES;
    _searchTableView.hidden = YES;
    
    return NO;
}
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    NSLog(@"textFieldShouldReturn:");
    [theTextField resignFirstResponder];
    
   // _backImage.hidden = YES;
   // _searchTableView.hidden = YES;
    return YES;
}
//---------------------- Daily Limit Over ---------------------------------------------------------------

//NSURL *urli=[NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/textsearch/json?query=%@&key=%@",searchedLocationString,@"AIzaSyAtdKFeeYQMQDUPztvhJnCB71-higwNf9I"]];

//---------------------------------------------------------------------------

//NSURL *urli=[NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?key=%@&address=%@&sensor=false",@"AIzaSyBUtx3F7KRfzJLPL8pBR821aTji1lG34B0",searchedLocationString]];

//--------- Free -------

//NSURL *urli=[NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?key=%@&address=%@&sensor=false",@"AIzaSyBUtx3F7KRfzJLPL8pBR821aTji1lG34B0",searchedLocationString]];

// NSURL *urli=[NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/textsearch/json?query=%@&key=%@",searchedLocationString,@"AIzaSyAtdKFeeYQMQDUPztvhJnCB71-higwNf9I"]];

//AIzaSyAtdKFeeYQMQDUPztvhJnCB71-higwNf9I
-(void)CallFunctionToGetLatLong :(NSString *)strValue
{
    
    NSURL *urli=[NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&sensor=%@&key=%@", [searchedLocationString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
                                      @"true", @"AIzaSyCDi2dklT-95tEHqYoE7Tklwzn3eJP-MtM"]];
    
    NSLog(@"urli=%@",urli);
    
    NSString *str = [NSString stringWithFormat:@""];
    NSData *postData = [str dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:urli];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    dic_adress=[[NSMutableDictionary alloc]init];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:
     ^(NSURLResponse *response, NSData *data, NSError* connectionError)
     {
         if(data)
         {
             
             dic_adress = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
             
             NSLog(@"dic_adress=%@",dic_adress);
             [arr_address removeAllObjects];
             
             arr_address= [[NSMutableArray alloc]initWithArray:[[dic_adress valueForKey:@"predictions"]valueForKey:@"description"]];
             
             _backImage.hidden=NO;
             _backImage.backgroundColor=[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.4];
             
             
             _searchTableView.hidden=NO;
             [_searchTableView setSeparatorInset:UIEdgeInsetsZero];
             _searchTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
             _searchTableView.backgroundColor=[UIColor whiteColor];
             _searchTableView.showsVerticalScrollIndicator = YES;
             _searchTableView.alwaysBounceVertical = YES;
             
             
             [_searchTableView setDelegate:self];
             _searchTableView.dataSource=self;
             [_searchTableView reloadData];
             
             _searchTableView.hidden=NO;
         }
     }];
    //    if (_searchTableView.frame.size.height==0)
    //    {
    //        [UIView animateWithDuration:0.3f animations:^{
    //
    //            _searchTableView.frame=CGRectMake(_searchTableView.frame.origin.x, _searchTableView.frame.origin.y, _searchTableView.frame.size.width, [arr_address count]*30);
    //
    //        }
    //                         completion:^(BOOL finished)
    //         {
    //
    //         }];
    //
    //    }
    //    else  if (_searchTableView.frame.size.height==  [arr_address count]*30)
    //    {
    //        [UIView animateWithDuration:0.3f animations:^{
    //            _searchTableView.frame=CGRectMake(_searchTableView.frame.origin.x, _searchTableView.frame.origin.y, _searchTableView.frame.size.width, 0);
    //        }
    //                         completion:^(BOOL finished)
    //         {
    //
    //         }];
    //    }
}
//------------------------------------------------------------------------------------------------------------------------------------
#pragma mark -UITableView Datasource and Delegate methods
/************************* TableViewMethods **********************************/
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
//-------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [arr_address count];
    
    
}
//-------------------------------------------------------------------------------------------------------------------------------------------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 40;
    
}
//-------------------------------------------------------------------------------------------------------------------------------------------
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier =@"cell";
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.backgroundView=[[UIView alloc] initWithFrame:cell.bounds];
        cell.backgroundColor=[UIColor clearColor];
    }
    if(indexPath.row%2==0)
    {
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }
    else
    {
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    
    addressTextLable = [cell.contentView viewWithTag: 100];
    addressTextLable.backgroundColor=[UIColor  clearColor];
    addressTextLable.text = [arr_address objectAtIndex:indexPath.row];
    addressTextLable.textColor=[UIColor blackColor];
    addressTextLable.numberOfLines=0;
    
    UIButton* locationNameButton = [cell.contentView viewWithTag: 200];
    locationNameButton.backgroundColor=[UIColor clearColor];
    [locationNameButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    locationNameButton.exclusiveTouch=YES;
    [locationNameButton setTitle:[NSString stringWithFormat:@"%ld",(long)indexPath.row] forState:UIControlStateNormal];
    [locationNameButton addTarget:self action:@selector(locationButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    locationNameButton.userInteractionEnabled=true;
    
    
    
    
    return cell;
}

-(IBAction)locationButtonAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    if(arr_address.count>0)
    {
        
        int intValue = [sender.titleLabel.text intValue];
        str_Address=[arr_address objectAtIndex:intValue];
        NSLog(@"str_Address=%@",str_Address);
        
       
        
        NSString *esc_addr =  [str_Address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"esc_addr=%@",esc_addr);
        _textFieldMap.text=str_Address;
        
        NSURL *urli = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=true",esc_addr]];
        
        NSString *str = [NSString stringWithFormat:@""];
        NSData *postData = [str dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:urli];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];

        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:
         ^(NSURLResponse *response, NSData *data, NSError* connectionError){
             if(data){
                 Arr_latilong = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                 
                 if ([Arr_latilong count] != 0){
                     latStr = [NSString stringWithFormat:@"%@",[[[[[Arr_latilong valueForKey:@"results"] valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lat"] objectAtIndex:0]];
                     
                     
                     longStr = [NSString stringWithFormat:@"%@",[[[[[Arr_latilong valueForKey:@"results"] valueForKey:@"geometry"] valueForKey:@"location"] valueForKey:@"lng"] objectAtIndex:0]];
                     
                     [[NSUserDefaults standardUserDefaults] setObject:str_Address  forKey:kLocation];
                      [[NSUserDefaults standardUserDefaults] setObject:str_Address  forKey:kHomeLocation];
                     
                      [[NSUserDefaults standardUserDefaults] setObject:str_Address  forKey:kWorkLocation];
                     
                    [[NSUserDefaults standardUserDefaults] setObject:latStr  forKey:kLocationLat];
                    [[NSUserDefaults standardUserDefaults] setObject:longStr  forKey:kLocationLong];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                     
                
                     dic_loc=[[NSDictionary alloc]initWithObjectsAndKeys:latStr,@"lat",longStr,@"long",str_Address,@"Address",nil];
                    
                 }
                    [self.navigationController popViewControllerAnimated:YES];
             }
         }
         ];

        _backImage.hidden = YES;
        _searchTableView.hidden = YES;
    }
    
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch* touch =[touches anyObject];
    if([touch view] == (UIView *)[self.view viewWithTag:741258])
    {
        _backImage.hidden = YES;
        _searchTableView.hidden = YES;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)mapAction:(id)sender {
    
    
}
-(void)func__btnBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIImaheView UI

-(UIImageView*)createUIImageView:(CGRect)frame backgroundColor:(UIColor*)backgroundColor image:(UIImage*)image isLogo:(BOOL)isLogo{
    
    UIImageView *_imageView = [[UIImageView alloc]initWithFrame:frame];
    _imageView.image = image;
    _imageView.backgroundColor = backgroundColor;
    return _imageView;
}
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UILable UI

-(UILabel*)createLable:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UILabel *_lable = [[UILabel alloc] init];
    _lable.backgroundColor = backgroundColor;
    _lable.frame = frame;
    _lable.text = title;
    _lable.textColor = titleColor;
    _lable.font = font;
    
    return _lable;
}
//------------------------------------- bckgroundColor ----------------------------------------------------------------------------------------------
#pragma mark- ---------------------------------------------------------------------------------
#pragma mark- Method to Create UIButton UI
-(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor{
    
    UIButton *_button = [UIButton buttonWithType: UIButtonTypeCustom];
    _button.backgroundColor = backgroundColor;
    _button.frame = frame;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitleColor:titleColor forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5] forState:UIControlStateNormal];
    // [_botton setImage:[UIImage imageNamed:kimageFBFriends5Hov] forState:UIControlStateHighlighted];
    
    _button.titleLabel.font = font;
    
    return _button;
}


@end
