//
//  ViewController.m
//  FitNetApp
//
//  Created by anish on 14/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import "ViewController.h"
#import "HomeVC.h"
#import "TabBarVC.h"
#import <FacebookSDK/FacebookSDK.h>
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "Constant.h"
#import "TermsAndConditionVC.h"

@interface ViewController ()
{
    NSDictionary *resultDic;
}

@property (nonatomic, strong) AppDelegate *appDelegate;
-(void)handleFBSessionStateChangeWithNotification:(NSNotification *)notification;

@end

@implementation ViewController
//--------------------------------------------------------
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // Observe for the custom notification regarding the session state change.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleFBSessionStateChangeWithNotification:)
                                                 name:@"SessionStateChangeNotification"
                                               object:nil];
    
    // Initialize the appDelegate property.
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    

    //[self facebookBtnAction];
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)facebookLoginAction:(id)sender
{
    if (_clickTermsBtn.selected == true)
    {
        if ([FBSession activeSession].state != FBSessionStateOpen &&
            [FBSession activeSession].state != FBSessionStateOpenTokenExtended)
        {
            
            [self.appDelegate openActiveSessionWithPermissions:@[@"public_profile", @"email"] allowLoginUI:YES];
            
        }
        else
        {
            // Close an existing session.
            [[FBSession activeSession] closeAndClearTokenInformation];
            
            // Update the UI.
        }
    }
    else
    {
        
         [appDelegate() errorMessages:kAlertTerms];

    }
    
   }
#pragma mark - Private method implementation

-(void)handleFBSessionStateChangeWithNotification:(NSNotification *)notification{
    // Get the session, state and error values from the notification's userInfo dictionary.
    NSDictionary *userInfo = [notification userInfo];
    
    FBSessionState sessionState = [[userInfo objectForKey:@"state"] integerValue];
    NSError *error = [userInfo objectForKey:@"error"];
    
    // Handle the session state.
    // Usually, the only interesting states are the opened session, the closed session and the failed login.
    if (!error) {
        // In case that there's not any error, then check if the session opened or closed.
        if (sessionState == FBSessionStateOpen)
        {
            [FBRequestConnection startWithGraphPath:@"me"
                                         parameters:@{@"fields": @"first_name, last_name, picture.type(normal), email"}
                                         HTTPMethod:@"GET"
                                  completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                                      if (!error) {
                                          // Set the use full name.
                                          resultDic = result ;
                                          NSLog(@"resultDic=%@",resultDic);
                                         [self getFacebookApiCall];
                                          
                                      }
                                      else{
                                          NSLog(@"%@", [error localizedDescription]);
                                      }
                                  }];
        
            
        }
        else if (sessionState == FBSessionStateClosed || sessionState == FBSessionStateClosedLoginFailed){
            // A session was closed or the login was failed. Update the UI accordingly.
        
        }
    }
    else{
        // In case an error has occurred, then just log the error and update the UI accordingly.
        NSLog(@"Error: %@", [error localizedDescription]);
    
    }
}
-(void)getFacebookApiCall
{
    [appDelegate() CheckInternetConnection];
    if([appDelegate() internetWorking] ==0)
    {
        NSString *_strDeviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:kUserDeviceToken];
        
        NSLog(@"_strDeviceToken=%@",_strDeviceToken);
        
        if (_strDeviceToken.length==0)
        {
            _strDeviceToken = @"12345678";
        }
        
        NSString *lat = [[NSUserDefaults standardUserDefaults] valueForKey:kLAT];
        NSString *lon = [[NSUserDefaults standardUserDefaults] valueForKey:kLNG];
        
        if (lat.length==0)
        {
            lat = @"1234";
        }
        if (lon.length==0)
        {
            lon = @"1234";
        }
        
        NSLog(@"deviceToken=%@",_strDeviceToken);
        NSLog(@"resultDic=%@",[[[resultDic valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"]);
        NSString *url;
//        if ([[[[resultDic valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"] length]==0)
//        {
////            url = @"http://www.indianfunpic.com/wp-content/uploads/2016/06/Funny-Kids-5.jpg";
            url = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large",[resultDic valueForKey:@"id"]];
//        }
        NSDictionary *initdataInfo = @{
                                       @"name":[resultDic valueForKey:@"first_name"],
                                       @"imageUrl":url,
                                       @"facebookId":[resultDic valueForKey:@"id"],
                                       @"email":[resultDic valueForKey:@"email"],
                                       @"latitude":lat,
                                       @"longitude":lon,
                                       @"deviceType":@"IOS",
                                       @"deviceToken":_strDeviceToken,
                                       @"appVersion":@"1",
                                       };
        NSLog(@"initdataInfo%@",initdataInfo);
        [API PostInitdataUserWithInfo:[initdataInfo mutableCopy] completionHandler:^(NSDictionary *json,NSError *error)
         {
             
             NSLog(@"initdataInfo=%@",json);

             
             [[NSUserDefaults standardUserDefaults] setObject:[json valueForKey:@"data"]  forKey:kLoginData];
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             NSLog(@"kLoginData=%@",[[NSUserDefaults standardUserDefaults] valueForKey:kLoginData]);
             TabBarVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarVCPush"];
             [self.navigationController pushViewController:newView animated:YES];

             [appDelegate() dismissActivityAlert];
         }];
    }
    else
    {
        [appDelegate() dismissActivityAlert];
        [appDelegate() errorMessages:kAlertInternetConnection];
    }

    
}

//-----------------------------
-(void)facebookBtnAction
{
   
}
-(void)viewDidAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}
#pragma mark - FBLoginView Delegate method implementation
-(void)loginViewShowingLoggedInUser:(FBLoginView *)loginView{
    
}

-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user{
    NSLog(@"%@", user);
    }


-(void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView{
}


-(void)loginView:(FBLoginView *)loginView handleError:(NSError *)error{
    NSLog(@"%@", [error localizedDescription]);
}
- (IBAction)termsAction:(id)sender {
    
    TermsAndConditionVC *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndConditionVC"];
    [self.navigationController pushViewController:newView animated:YES];
}
- (IBAction)clickBtnAction:(UIButton *)sender {
    
    if (sender.selected == false)
    {
        sender.selected = true;
        [_clickTermsBtn setBackgroundImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
    }
    else
    {
     [_clickTermsBtn setBackgroundImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
     sender.selected = false;
    }
}

//-----------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)unwindSegue:(UIStoryboardSegue *)segue{
    NSLog(@"it worked buddy");
}

@end
