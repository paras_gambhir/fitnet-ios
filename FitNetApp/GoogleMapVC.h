//
//  GoogleMapVC.h
//  FitNetApp
//
//  Created by anish on 18/07/16.
//  Copyright © 2016 Anish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "APIManager.h"
#import "Macros.h"
#import "BaseControllerVC.h"
#import "MapModel.h"
#import <GoogleMaps/GoogleMaps.h>
#import "SettingVC.h"

@interface GoogleMapVC : BaseControllerVC <GMSMapViewDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControlHomeWork;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@property NSString *strLatHome;
@property NSString *strLngHome;
@property NSString *strLatWork;
@property NSString *strLngWork;

- (IBAction)actionSegmentControl:(id)sender;
- (IBAction)actionBtnSettings:(id)sender;

@property NSMutableArray *arrHomeData;
@property NSMutableArray *arrWorkData;


@end
